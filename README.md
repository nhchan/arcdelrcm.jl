# ArcDelRCM.jl

This is an Arctic-delta model that can reproduce the 2-m ramp feature ubiquitous to Arctic deltas. This is achieved by making a series of modifications and refinements concerning permafrost erosion/deposition and the effects of bed-fast ice to a base model. 

The base model, upon which the modifications were made, is the Arctic Delta Reduced-Complexity Model (DeltaRCM-Arctic) based on DeltaRCM by Liang et al. (2015)[^1]<sup>,</sup>[^2] and the advancements to include Arctic settings by Lauzon et al. (2019)[^3]. All the modifications leading to this model, ArcDelRCM.jl, are detailed in the research article by Chan et al. (2022)[^4]. 

This code is written purely in the Julia language.

To run the code on your computer, first install Julia version >= 1.5, and execute the run script (a template of which is provided as `Run_script_template.jl`). All the tunable parameters are included in the run script. 

The comments in the source code serve as documentations for the time being.

## Citation
The source codes can be cited as follows:


> Chan, Ngai-Ham (2023): ArcDelRCM.jl - an Arctic-delta reduced-complexity model that can reproduce the 2-m ramp feature ubiquitous to Arctic deltas. V. 0.3.0. GFZ Data Services. https://doi.org/10.5880/GFZ.4.7.2023.001

## References

[^1]: Liang, M., Voller, V.R., and Paola, C. 2015, *Earth Surface Dynamics*, 3, 67-86. https://www.doi.org/10.5194/esurf-3-67-2015

[^2]: Liang, M., Geleynse, N., Edmonds, D. A., and Passalacqua, P. 2015, *Earth Surface Dynamics*, 3, 87-104. https://www.doi.org/10.5194/esurf-3-87-2015

[^3]: Lauzon, R., Piliouras, A., and Rowland, J. C. 2019, *Geophysical Research Letters*, 46, 6574–6582. https://doi.org/10.1029/2019GL082792

[^4]: Chan, N.-H., Langer, M., Juhls, B., Rettelbach, T., Overduin, P., Huppert, K., and Braun, J. 2022, *Earth Surface Dynamics*, accepted. https://doi.org/10.5194/esurf-2022-25
