#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    config-and-execute script file: Run.jl

Script file to execute an ArcDelRCM simulation. This file contains all the user-defined simulation parameters. Some
secondary (i.e., derived) parameters are defined in the `setparams` function in "src/init.jl". Most of them are
dependent on or tightly linked with the parameters defined herein; therefore, be extra careful when modifying anything
there. This file can be executed in REPL using an `include` statement, or in the terminal by running `julia Run.jl`.

A note on the types specifications:
  * ::Int is the system-default integer type, which is most likely to be Int64 these days.
  * ::𝔽   is defined in "ArcDelConst.jl", and points to the system-default floating-point type,
          which is most likely to be Float64 these days.
These are sometimes redundant, but are included so that users are explicitly informed of the types of the parameters.
=#

# [================================================ Table of Contents =================================================]
# Nx, Ny, δc, δz, N₀, Nwall, #.................................................................................. [- 1 -]
# Δt, lasttime, nw, ns, lastiterw, #............................................................................ [- 2 -]
# iceextent, hᵢmax, 𝐸, å, #..................................................................................... [- 3 -]
# sedfraction, f_sand, f_mud, #................................................................................. [-4.1-]
# S₀, h₀, hB, hdry, hΔ, u₀, umax, u_muddep, u_mudero, u_sandero, uΔ #........................................... [-4.2-]
# Qw₀, Qs₀, #................................................................................................... [-4.3-]
# α, β, γ, ϵ, θ, ϖ, ω, #........................................................................................ [- 5 -]
# backwater_method!, manningscoef², 𝐶𝔣, #....................................................................... [- 6 -]

# Load the ArcDelRCM package; display useful error message if not found.
if ~isdefined(Main, :ArcDelRCM)
	try
		using ArcDelRCM
	catch
		@error "module ArcDelRCM not found.\n" *
		"Please ensure that its path is in your global `LOAD_PATH` variable,\n" *
		"or run `include(<path-to-source-file>); using .ArcDelRCM` to load it."
	end
end

# Set the same random seed for model-testing purposes. Disable for “production” runs.
using Random; Random.seed!(20200616230157);

## let: create a local scope to keep things tidy ##
let



#================================================ SETTINGS STARTS HERE ================================================#

# Is this a “dry run” to only print out all parameters settings (without running the simulation)?
dry_run::Bool = true

# Would you like to enable this simulation's record to be inherited and continued in the future?
to_be_continued::Bool = false

# Is this an Arctic simulation? Arctic => true : non-Arctic => false
arctic_truefalse::Bool = true

#------- Domain Dimensions -------#                                                                              [- 1 -]

# Domain length (⟂ to the inlet wall), L, and width (∥ to the inlet wall), W; typically, W = 2 × L.
Nx::Int, Ny::Int = 60, 120
# NB: due to boundary conditions on the ocean sides, it is not useful to have Nx > Ny.

# Dimension of square cells [m].
δc::𝔽 = 50.

# Thickness of a stratigraphic layer [m].
δz::𝔽 = 0.05

# Number of inlet cells.
N₀::Int = 5

# Thickness of the inlet wall.
Nwall::Int = 3

#------- Time-, Iteration- and Routing- Related Parameters -------#                                              [- 2 -]

# Size of each time step [s].
# There is an internal check (with notificaation) to ensure numerical stability.
Δt::𝔽 = 25000.

# Number of timesteps to run.
lasttime::Int = 5005

# Number of water packets to split up the upstream water discharge, Qw₀, over each time step.
nw::Int = 2000

# Number of sediment packets to split up the sediment input volume, ΔVs, over each time step.
ns::Int = 2000

# Number of iterations to perform in the releasing of all `nw` water packets, in order to
# stablise the flow field from the random-walk nature of the packet routing.
lastiterw::Int = 5

#------- Arctic Parameters -------#                                                                              [- 3 -]

# Initial upstream ice extent as fraction of delta length [fraction of 1.0].
iceextent::𝔽 = 0.4

# Maximum ice thickness [m].
hᵢmax::𝔽 = 3.

# Permafrost erodability relative to non-permafrost materials [fraction of 1].
𝐸::𝔽 = 0.95

# Atmospheric melting contribution coefficient [fraction of 1].
# (Full contribution means that all ice will melt during the melting period due to atmospheric heat alone.)
å::𝔽 = 1.

#------- Flow and Basin Parameters -------#                                                                      [- 4 -]

#———— Sediment Fractions ————#                                                                                   [-4.1-]

# Sediment fraction by water volume.
sedfraction::𝔽 = 0.001

# Fraction of sand, which is treated as bed load.
f_sand::𝔽 = 0.5

# Fraction of mud, which is treated as suspended load.
f_mud::𝔽 = 1 - f_sand

# ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑
# Explicitly declaring fraction of each sediment type allows for future
# addition of others without too much modifications to existing codes.

#———— Slope, Depths, and Speeds ————#                                                                            [-4.2-]

# Reference slope during minimum inlet discharge.
# This can be inferred from the water surface elevation difference between the inlet and the ocean of the fully
# formed (i.e., well-developed) delta. If no fully formed deltas can be used as a guidance for these values,
# the values given by Liang et al. (2015):
# 	3 × 10⁻⁴ for a purely sandy delta, 1 × 10⁻⁴ for a purely muddy one,
# can also be used as average slope values to infer the (minimum) slope during low inlet discharge. Due to the
# reduced complexity nature of the model, these values are necessarily approximate.
# (For constant inlet discharge, this is simply the constant reference slope.)
Sₘᵢₙ::𝔽 = 3e-4 * f_sand + 1e-4 * (1 - f_sand)

# Characteristic flow depth, which is also the inlet channel depth, during the lowest inlet discharge [m].
# The flow depth at higher inlet discharges will be determined by the scaling based on Manning's formula.
# (For constant inlet discharge, this is simply the constant inlet flow depth.)
hₘᵢₙ::𝔽 = 5.

# Depth of the sea basin.
hB::𝔽 = 5.

# Minimum water depth before being considered dry land [m].
hdry::𝔽 = min(0.1, 0.1hₘᵢₙ)

# Water depth at or shallower than which the cell will be considered to be on the delta [m].
hΔ::𝔽 = 0.1hₘᵢₙ
# Adopted h_ref = h₀ in the terminology used in Liang et al. (2015)

# Characteristic flow speed, which is also the inlet channel velocity, at the lowest inlet discharge [m/s].
# The velocity at higher inlet discharges will be determined by the scaling based on Manning's formula.
# (For constant inlet discharge, this is simply the constant inlet flow speed.)
uₘᵢₙ::𝔽 = 1.

# Maximum limit of flow speed [m/s].
# This can be set such that all flows are safely subcritical (defined as: Froude Number < 1 => u < √(𝒈h)).
# In the original Liang et al. (2015) setup, this was set to 2 × u₀.
uₘₐₓ::𝔽 = 2uₘᵢₙ

# Coefficient to obtain flow speed limit, below which mud is deposited [m/s], given in the form:
# 	u_muddep = u_muddep_coef * uₘₑₐₙ ,
# where uₘₑₐₙ is the average inlet flow speed, u₀. If u₀ is constant, uₘₑₐₙ = uₘᵢₙ.
# (Finer grain size ⟹ Slower flow limit for deposition.)
u_muddep_coef::𝔽 = 0.3

# Coefficient to obtain flow speed limit, above which mud is eroded [m/s], given in the form:
# 	u_mudero = u_mudero_coef * uₘₑₐₙ ,
# where uₘₑₐₙ is the average inlet flow speed, u₀. If u₀ is constant, uₘₑₐₙ = uₘᵢₙ.
u_mudero_coef::𝔽 = 1.5

# FlCoefficient to obtain fw velocity limit, above which sand is eroded [m/s], given in the form:
# 	u_sandero = u_sandero_coef * uₘₑₐₙ ,
# where uₘₑₐₙ is the average inlet flow speed, u₀. If u₀ is constant, uₘₑₐₙ = uₘᵢₙ.
u_sandero_coef::𝔽 = 1.05

# Flow speed at or above which the cell will be considered to be on the delta [m/s].
uΔ::𝔽 = 0.5uₘᵢₙ
# Adopted u_ref = u₀ in the terminology used in Liang et al. (2015)

#———— Discharges and Volumes ————#                                                                               [-4.3-]

# Constant water discharge at inlet (volume velocity) [m³/s or 1000 L/s].
Qw₀::Array{𝔽,1} = [uₘᵢₙ * hₘᵢₙ * N₀ * δc]

# Constant sediment flux at inlet (volume velocity) [m³/s].
Qs₀::Array{𝔽,1} = Qw₀ * sedfraction

#------- Weights and Scaling Coefficients -------#                                                               [- 5 -]

# Scaling coefficient for the bed-elevation diffusive filtering.
α::𝔽 = 0.1

# Exponent of the Meyer-Peter & Müller (1948) formula to calculate sediment transport capacity:
#   (Local Transport Flux Capacity) / (Reference Flux) = (Local Flow Velocity)ᵝ / (Reference Flow Velocity)ᵝ,
# where β is the exponent raising the velocity quantities.
β::𝔽 = 3

# Partitioning coefficient for routing directions, γ, is the fraction of water-surface gradient
# to take into account, with the rest being the “inertia” (i.e., downstream) direction.
# The form that γ took in the original MATLAB version of this model is:
# 	ℊ * S₀ * δc / (u₀ ^ 2)
# which probably came roughly from taking the ratio of the pressure-gradient term over the inertia terms
# (without local acceleration) in the shallow water equations. For very large δc, however, the characteristic
# slope, S₀, and the typical variation of flow velocity, u (in the δu/δx term in the 1D shallow water equation),
# will have to be reassessed. For the model to produce something sensible, γ has to be roughly between 0.03 and
# 0.15, based on the combined range given by the pair of Liang et al. (2015) papers.
γ::𝔽 = ℊ * S₀ * δc / (u₀ ^ 2)

# Diffusiion coefficient of the surface-elevation smoothing process, such that
#   H_{smooth} = (1 - ϵ) × H + ϵ H_{nb},
# where H is the original surface elevation field, and H_{nb} is the cell-by-cell average of all
# neighbouring cells in H.
ϵ::𝔽 = 0.1

# Exponent of depth-dependence in routing probability weights.
θ = (water = 1., sand = 2., mud = 1.)

# Underrelaxation coefficient for damping sharp changes to the surface elevation between time steps, such that
#   H_{new} = (1 - ϖ) × H_{previous} + ϖ H_{smooth},
# where H_{smooth} is the latest calculated surface elevation smoothed by a diffusion filter.
ϖ::𝔽 = 0.1

# Underrelaxation coefficient for damping sharp changes in the unit-discharge field between iterations and
# between time steps, such that
#   q_{new} = (1 - ω) × q_{existing} + ω × q_{latest},
# where q_{existing} is the field, q, in the previous time step or the previous iteration, and q_{latest} is
# the same field resulting from the current iteration.
# The two sub-values, `iterfirst` and `itersubsq`, are for the first and subsequent iterations, respectively.
ω = (iterfirst = 0.9, itersubsq = 2 / lastiterw)

#------- Method to Use for Backwater Calculation -------#                                                        [- 6 -]

# Alias of the backwater-calculation method to be used.
# Choose from the following (don't forget the colon and the exclamation mark):
# :backwater_constslope! || :backwater_gvfmanning! || :backwater_gvfflowrcm!
backwater_method = :backwater_constslope!

# Manning's 𝑛 value (∼ coefficient of friction).
# See
#   [Wikipedia: Manning formula]
#    (https://en.wikipedia.org/wiki/Manning_formula)
# and
#   [Reference tables for Manning's n values]
#    (http://www.fsl.orst.edu/geowater/FX3/help/FX3_Help.html#8_Hydraulic_Reference/Mannings_n_Tables.htm)
# for details.
manningscoef²::𝔽 = abs2(0.04)
# Applicable only if backwater_method ≡ :backwater_gvfmanning!

# “Coefficient of friction” defined in [Liang et al. (2015, Part II)](https://doi.org/10.5194/esurf-3-87-2015).
𝐶𝔣::𝔽 = 0.01
# Applicable only if backwater_method ≡ :backwater_gvfflowrcm!

#================================================= SETTINGS ENDS HERE =================================================#



#------- Generate the Parameters Variable and Execute the Simulation -------#

# 1. Generate the struct of Composite Type `Params` to be passed into the main simulation function, `run_*()`.
settings = setparams(
                     arctic_truefalse;
                      Nx, Ny, δc, δz, N₀, Nwall, Δt,
                      lasttime, nw, ns, lastiterw,
                      iceextent, hᵢmax, 𝐸, å,
                      sedfraction, f_sand, f_mud,
                      Sₘᵢₙ, hₘᵢₙ, hB, hdry, hΔ, uₘᵢₙ, uₘₐₓ, u_muddep_coef, u_mudero_coef, u_sandero_coef, uΔ,
                      Qw₀, Qs₀,
                      α, β, γ, ϵ, θ, ϖ, ω,
                      backwater_method,
                      manningscoef²,
                      𝐶𝔣
                     )
# 💡 function setparams(arctic_or_not::Bool; <all variables declared above as keyword arguments>) is in "src/init.jl".

# 2a. If this is a “dry run”, print all values of `settings` to screen:
if dry_run
	printstruct(settings)
	return
end

# 2b. Execute the simulation.
if arctic_truefalse # IF this is an Arctic simulation, call `run_withice`:

    run_withice(settings; to_be_continued)
    # Other keyword arguments include:
    #   ramp_up::Int = 300
    #   days_per_year::Int = 10
    #   days_to_melt_ice::Int = days_per_year
	# 	days_before_melt_start::Real = 0.0
	# 	melt_time_profile::Symbol = :linear
	# 	seabedtilt_init::Vector = []
	# 	island_block::Vector = []
    #   output_filename_stem::String = "default"
    #   output_parent_dir::String = pwd()
    #   output_time_as_years::Bool = false
    #   output_include_flags::Bool = false
	# 	output_include_strata::Bool = true
	# 	steps_per_output::Int = 1
	# 	to_be_continued::Bool = false
	# 	inheritance::String = "none"

else # OTHERWISE, run the non-Arctic version, `run_icefree`:

    run_icefree(settings; to_be_continued)
    # Other keyword arguments include:
	# 	seabedtilt_init::Vector = []
	# 	island_block::Vector = []
    #   output_filename_stem::String = "default"
    #   output_parent_dir::String = pwd()
    #   output_time_as_years::Bool = false
    #   output_include_flags::Bool = false
	# 	output_include_strata::Bool = true
	# 	steps_per_output::Int = 1
	# 	to_be_continued::Bool = false
	# 	inheritance::String = "none"

end

## end let ##
end



##======= REMARKS =======##
#=
Any specific comments about the simulation set up above.
This can be usefule when sharing this execution script,
or for record-keeping purposes (especially when running
a large number of simulations for analyses).
=#



#------------------------------------------------------#
# Ways to type the unicode symbols in Julia's Juno IDE:
# (symbol : code)
# ₀ : \_0
# ½ : \1/2
# ² : \^2
# å : \aa
# 𝐶 : \itC
# δ : \delta
# Δ : \Delta
# 𝐸 : \itE
# 𝔣 : \frakf
# ℊ : \scrg
# ᵢ : \_i
# α : \alpha
# β : \beta
# γ : \gamma
# ϵ : \epsilon
# θ : \theta
# ϖ : \varpi
# ω : \omega
