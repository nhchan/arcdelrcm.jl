#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: tasks.jl

Define all the functions that constitute the main tasks (procedural groups) in an ArcDelRCM
(Arctic Delta RCM) simulation. The tasks are groups of procedures that are executed one
after another (often in loops) to form procedural units that are more complicated.

```
==========================
|    TABLE of CONTENTS   |
|   (look-up along the   |
|     right-hand edge    |
|      of this file)     |
--------------------------
## ==== flow_iteration! ##
## ==== round_of_water! ##
## ===== round_of_sand! ##
## ====== round_of_mud! ##

=#

include("procedures.jl") # Contains the functions that form the procedural groups of the simulation

## ================================================================================================== flow_iteration! ##
# Perform Iterations of Water-Packet Release and Guidance to Acquire a (Hopefully) Stable Flow Field
"""
    flow_iteration!(
                    next_step_function::Function,
                    is_first_timestep::Bool,
                    tᵢ::Int,
                    params::Params,
                    waters::Waters,
                    water_tracking::WaterTracking,
                    sea_surface_height::𝔽,
                    boundaries::Boundaries
                    )

Perform iterations of water-packet spawning and guidance, (water) surface-elevation
calculation, shoreline/bank migration, and flow-field determination, with the goal of
achieving a stabilised water flow field.

`next_step_function` is the function that handles the weighted-random walk of the packets.
All input arguments _except_ `is_first_time`, `sea_surface_height`, and the `wall_flag` and
`ocean_flag` fields inside the struct `boundaries` are modified in-place.
"""
function flow_iteration!(
                         next_step_function::Function,
                         is_first_timestep::Bool,
                         tᵢ::Int,
                         params::Params,
                         waters::Waters,
                         water_tracking::WaterTracking,
                         sea_surface_height::𝔽,
                         boundaries::Boundaries
                         )

    for iterw::Int in 1:params.lastiterw

        # Spawn and determine the paths of `nw` number of water packets, calculating the water-surface
        # elevation along each path.
        round_of_water!(
                        next_step_function,
                        tᵢ,
                        params,
                        waters,
                        water_tracking,
                        sea_surface_height,
                        boundaries
                        )
        # By the end of each packet-loop inside `round_of_water!`, both `path_history` and `step_history` in the struct
        # `water_tracking` are reset to zero.

        # Calculate the current water-surface elevation from all the water-packet paths.
        full_surface!(is_first_timestep, params, waters, water_tracking, sea_surface_height, boundaries)

        #= By this point, `water_tracking.visit_count` and `water_tracking.cumul_surf_elev` had been reset to zero. =#

        # Adjust shorelines or banks, so that there are no walls of water taller than the adjacent dry land.
        migrate_shore_bank!(params, waters)

        # Update the water-depth, unit-discharge, and flow-speed fields.
        update_after_water!(is_first_timestep, iterw, params, waters)

        #= By this point, all the “next” unit-discharge fields (i.e., `*_nxt`) had been reset to zero for the =#
        #= next iteration or time step.                                                                       =#

        # Re-impose inlet boundary condition for discharge
        reimpose_boundary_conds_dischg!(tᵢ, params, waters)

    end # for iterw::Int in 1:lastiterw

end

## ================================================================================================== round_of_water! ##
# Release and Determine the Paths of All the `nw` (Number of) Water Packets, for One Iteration within One Time Step
"""
    round_of_water!(
                    next_step_function::Function,
                    tᵢ::Int,
                    params::Params,
                    waters::Waters,
                    water_tracking::WaterTracking,
                    sea_surface_height::𝔽,
                    boundaries::Boundaries
                    )

Spawn and determine the paths of each of the `nw` (number) of water packets and calculating
their individual along-path surface slope. This task group includes checking for packets
trapped in a loop and ensuring that every water packet ends in the ocean before being
stopped by a boundary or step-number limit. This function is run once per iteration (of
determing the water flow and surface) inside each time step. Input `next_step_function` is
meant to be either [`nextstep_icefree`](@ref) or [`nextstep_withice`](@ref).

At the end of each packet loop, the `path_history` and `step_history` fields of input struct
`water_tracking` are reset to zero.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `px`, `py`, the fields `unit_dischg_x_nxt`, `unit_dischg_y_nxt`, and
`unit_dischg_nxt` in the struct `waters`, and the fields `visit_count`, and `cumul_surf_elev`
in the struct `water_tracking` are modified in-place.
"""
function round_of_water!(
                         next_step_function::Function,
                         tᵢ::Int,
                         params::Params,
                         waters::Waters,
                         water_tracking::WaterTracking,
                         sea_surface_height::𝔽,
                         boundaries::Boundaries
                         )

    for packetnumber in 1:params.nw

        # Create a water packet at a randomly chosen inlet cell; px, py mark the position.
        px::Int, py::Int = spawn_water!(tᵢ, params, waters, water_tracking)

        # Continue the onwards journey of the water packet, until it reaches a boundary or `maxstep` number of steps.
        px, py, laststep::Int, looped::Bool = stepn_water!(
                                                           next_step_function,
                                                           tᵢ,
                                                           packetnumber,
                                                           px, py,
                                                           params,
                                                           waters,
                                                           water_tracking,
                                                           boundaries
                                                           )

        # Ensuring that the water packet reached the ocean boundary, and that its path did not loop back on itself, we
        # now retrace the path and build the appropriate slope along the closest flow line.
        boundaries.ocean_flag[px, py] && !looped &&
            params.backwater_method!(tᵢ, laststep, params, waters, water_tracking, sea_surface_height)

        # Clear the path and direction history for the next packet.
        fill!(view(water_tracking.path_history, 1:laststep), CartesianIndex(0, 0))
        fill!(view(water_tracking.step_history, 1:(laststep - 1)), 0)
        #= Note:
        Unless the length of the above arrays are ≳ O(10³), and that the length to (re-)fill is usually approximately
        half of the total array length, the `fill!(view(...), ...)` combination is faster. Otherwise, use `fill!`
        without the `view` slicing. The worst would be to use a `for` loop or re-define the array variable.
        =#
    end # for packetnumber in 1:nw

end

## =================================================================================================== round_of_sand! ##
# Release and Determine the Paths of All the `ns * f_sand` (Number of) Sand Packets within the Scope of One Time Step
"""
    round_of_sand!(
                   next_step_function::Function,
                   tᵢ::Int,
                   params::Params,
                   waters::Waters,
                   seabed::Array{SedimentColumn{𝔽𝕤},2},
                   sediment_transport::SedimentTransport,
                   boundaries::Boundaries
                   )

Spawn and determine the paths of each of the `ns * f_sand` (number) of sand packets, and
deposit or erode along the way if conditions fit. This function is run once per time step.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`
in the struct `waters`; and `sand_flux` and `sand_deposit_volume` in the struct
`sediment_transport` are modified in-place.
"""
function round_of_sand!(
                        next_step_function::Function,
                        tᵢ::Int,
                        params::Params,
                        waters::Waters,
                        seabed::Array{SedimentColumn{𝔽𝕤},2},
                        sediment_transport::SedimentTransport,
                        boundaries::Boundaries
                        )

    for nothing in 1:round(Int, params.ns * params.f_sand)

        # Create a sand packet at a randomly chosen inlet cell; px, py mark its position, `Vresp` its “residual” volume.
        # At this starting point, `Vresp` is equal to `Vsp`, which is the sediment volume apportioned to each packet.
        px::Int, py::Int, Vresp::𝔽 = spawn_sand!(tᵢ, params, sediment_transport)

        # Continue the onwards journey of the sand packet, until it reaches a boundary or `maxstep` number of steps.
        px, py, Vresp = stepn_sand!(
                                    next_step_function,
                                    tᵢ,
                                    px, py,
                                    params,
                                    waters,
                                    seabed,
                                    sediment_transport,
                                    Vresp,
                                    boundaries
                                    )

        # One can print out the value of `Vresp` for a brief sanity check of the code.
        # @debug "Sand packet residual volume (end of packet):" Vresp

    end # for nothing in 1:round(Int, ns * f_sand)

end

## ==================================================================================================== round_of_mud! ##
# Release and Determine the Paths of All the `ns * f_mud` (Number of) Mud Packets within the Scope of One Time Step
"""
    round_of_mud!(
                  next_step_function::Function,
                  tᵢ::Int,
                  params::Params,
                  waters::Waters,
                  seabed::Array{SedimentColumn{𝔽𝕤},2},
                  sediment_transport::SedimentTransport,
                  boundaries::Boundaries
                  )

Spawn and determine the paths of each of the `ns * f_mud` (number) of mud packets, and
deposit or erode along the way if conditions fit. This function is run once per time step.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`
in the struct `waters`; and `mud_deposit_volume` in the struct `sediment_transport` are
modified in-place.
"""
function round_of_mud!(
                       next_step_function::Function,
                       tᵢ::Int,
                       params::Params,
                       waters::Waters,
                       seabed::Array{SedimentColumn{𝔽𝕤},2},
                       sediment_transport::SedimentTransport,
                       boundaries::Boundaries
                       )

    for nothing in 1:round(Int, params.ns * params.f_mud)

        # Create a sand packet at a randomly chosen inlet cell; px, py mark its position, `Vresp` its “residual” volume.
        # At this starting point, `Vresp` is equal to `Vsp`, which is the sediment volume apportioned to each packet.
        px::Int, py::Int, Vresp::𝔽 = spawn_mud(tᵢ, params)

        # Continue the onwards journey of the sand packet, until it reaches a boundary or `maxstep` number of steps.
        px, py, Vresp = stepn_mud!(
                                   next_step_function,
                                   tᵢ,
                                   px, py,
                                   params,
                                   waters,
                                   seabed,
                                   sediment_transport,
                                   Vresp,
                                   boundaries
                                   )

        # One can print out the value of `Vresp` for a brief sanity check of the code.
        # @debug "Mud packet residual volume (end of packet):" Vresp

    end # for nothing in 1:round(Int, ns * f_mud)

end
