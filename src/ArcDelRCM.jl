#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
"""
    module ArcDelRCM

Provide two main functions to simulate Arctic and non-Arctic deltas, [`run_withice`](@ref)
and [`run_icefree`](@ref), respectively. The Arctic version (based on Lauzon et al., 2019)
is built upon the non-Arctic base (DeltaRCM; see Liang et al., 2015) with minimal internal
code duplications.

Seven source files in the `src/` directory contain all of the:

  * functions ("ArcDelRCM.jl", "init.jl", "tasks.jl", "procedures.jl", and
               "support.jl"),
  * global-constant parameters ("constants.jl"), and
  * definitions of Composite Types ("types.jl"),

and all exporting are done through ArcDelRCM.jl. In most scenarios, users need only to
modify the parameters in the "Run.jl" script file in the package's root directory (a
template is provided as "Run_scripttemplate.jl"). There, they can also specify any options
provided by the two main functions.

The source files are only “included” once amongst themselves. The following tree structure
shows which files (branches) are included by which (trunk). For example, "ArcDelRCM.jl"
contains `include` statements to include both "init.jl" and "tasks.jl". An additional
"Run.jl" script file for users to input the settings and execute the simulation resides in
the package's root directory.
```
╔ ArcDelRCM.jl
╠━┳━ init.jl
║ ┖─┬─ constants.jl
║   ╰─── types.jl
╚━┳━ tasks.jl
  ┖─┬─ procedures.jl
    ╰─── support.jl
```

!!! compat "Julia 1.5"
    This module requires Julia >1.5 due to the syntax used for keyword arguments in the
    function calls. Specifically, for keyword arguments whose name and the name of the
    variable assigned to it are the same (e.g. `start_time = start_time`), one of the
    pair is omitted (i.e., writing only `start_time`). One can add back the `<keyword> =`
    part to bring this module back to compatibility with Julia 1.4 or possibly earlier.

---
REFERENCES:

  - Lauzon, R., Piliouras, A., and Rowland, J.C. 2019, Geophysical Research Letters, 46,
        6574-6582. doi:10.1029/2019GL082792
  - Liang, M., Voller, V.R., and Paola, C. 2015, Earth Surface Dynamics, 3, 67-86.
        doi:10.5194/esurf-3-67-2015
"""
module ArcDelRCM

# Tell Julia to load this module with optimisation level 3, if this experimental feature is available.
if isdefined(Base, :Experimental) && isdefined(Base.Experimental, Symbol("@optlevel"))
    @eval Base.Experimental.@optlevel 3
end

using UnPack, ProgressBars # 💡: ProgressMeter and its @showprogress macro is another option

include("init.jl")    # Contains code to allocate and initialise all the variables; "src/constants.jl" is included.
include("tasks.jl")   # Contains the “Tasks” functions of the simulation; "src/support.jl" is included.

# Export universal constants
export ℊ, sqrt2, sqrt½, sperday, speryear

# Export the parameter- setting and printing function.
export setparams, printstruct

# Export the main functions for executing the simulation.
export run_icefree, run_withice

# Export the Composite prototypes defined in "src/types.jl".
export Waters, WaterTracking, SedimentTransport, Boundaries, AllGriddedVars,
       Sediment, SedimentColumn,
       Params, DomainParams

# Export the floating-point types applied in the module (defined in "constants.jl").
export 𝔽, 𝔽𝕤

#=                                      ╔════════════════════════════════════╗                                        =#
#=                                      ║              ⇃ MAIN ⇂              ║                                        =#
#=                                      ╚════════════════════════════════════╝                                        =#

## ====================================================================================================== run_withice ##
# Simulation Under Arctic Conditions
"""
    run_withice(params::Params;
                 ramp_up::Int = 300, days_per_year::Int = 10, days_to_melt_ice::Int = days_per_year,
                 days_before_melt_start::Real = 0.0, melt_time_profile::Symbol = :linear,
                 seabedtilt_init::Vector = [], island_block::Vector = [],
                 output_filename_stem::String = "default", output_parent_dir::String = pwd(),
                 output_time_as_years::Bool = false, output_include_flags::Bool = false,
                 output_include_strata::Bool = true, steps_per_output::Int = 1,
                 to_be_continued::Bool = false, inheritance::String = "none"
                )

Run an Arctic DeltaRCM simulation.

*About the optional keyword arguments:*

To build up an existing channel network from an empty ocean basin, a ramp-up phase can be
run for `ramp_up` number of time steps (default = 300) under ice-free conditions (Lauzon et
al., 2019); i.e., by calling [`run_icefree`](@ref). This phase is executed with negative
time-step numbers until and including 0, such that the Arctic phase can start with time step
1. Since the focus of the model is on the Spring melting season and the short summer, each
year is represented by `days_per_year` number of days (optional; default = 10). Maximal
“winter” ice, both in thickness and extent, is imposed at the start of each year, but can
be delayed by `days_before_melt_start` number of days. Subsequent melting progresses for
`days_to_melt_ice` number of days (default = `days_per_year`), with the time profile (i.e.,
the melting progression's shape along the time axis) given by `melt_time_profile`, which
defaults to `:linear`, but can be any symbols (:) representing any 1-D mathematical
functions available in Julia's base module (e.g. `:tanh`). By definition,
the maximum thickness of ice will melt due to atmospheric heat alone in `days_to_melt_ice`
number of days.

The output files will be saved to an "OUTPUT" directory under the path `output_parent_dir`
(default is the output of `pwd()`) with the filename stem of `output_filename_stem` (default
is "default") to which suffixes "_statevars.nc" and "_strata.jld2" will be appended. Options
to save the time as years, to include flags (e.g. `dry_flag`), and to save strata in the
output files are given by `output_time_as_years` (default is false), `output_include_flags`
(default is false), and `output_include_strata` (default is true), respectively. To conserve
disk space, an option to write output at time-step intervals of `steps_per_output` (default
is 1; i.e., output at every step) is provided.

If this simulation were to be continued in the future (i.e., to enable the ability for
another simulation to inherit the ending state of the current one), set `to_be_continued` to
true. A saved-state file with the same filename stem and suffix "_savedstate.jld2" will be
saved along with the other output files.

If this simulation were to inherit the ending state of another one, provide the keyword
argument, `inheritance`, with the full path to the "*_savedstate.jld2" file, from which the
starting state of this simulation will be read. The code will also look for a corresponding
strata record file (with the suffix "_strata.jld2") that has the same path and filename stem
(i.e., before the "_savedstate.jld2" suffix). **Both files must be present with the same
filename stem.**
"""
function run_withice(params::Params;
                      ramp_up::Int = 300, days_per_year::Int = 10, days_to_melt_ice::Int = days_per_year,
                      days_before_melt_start::Real = 0.0, melt_time_profile::Symbol = :linear,
                      seabedtilt_init::Vector = [], island_block::Vector = [],
                      output_filename_stem::String = "default", output_parent_dir::String = pwd(),
                      output_time_as_years::Bool = false, output_include_flags::Bool = false,
                      output_include_strata::Bool = true, steps_per_output::Int = 1,
                      to_be_continued::Bool = false, inheritance::String = "none"
                     )

    # Unpack the parameters used below
    @unpack Δt, daysperΔt, yearsperΔt, å, hᵢmax, lasttime = params

    # De facto starting time-step.
    start_time = -ramp_up + 1

    # Time steps simulated in each year (rounded to the nearest integer).
    Δtsperyear = round(Int, days_per_year / daysperΔt)
    @assert Δtsperyear > 1 "There are not enough time steps in a year. At least 2 time steps per year is required."

    # Run the ice-free simulation for `ramp_up` number of steps in order to establish an initial channel network, and
    # inherit output files' information from the ice-free simulation.
    all_gridded_vars, sea_surface_height,
    output_fullpath_nc, output_fullpath_jld, output_fullpath_ss, write_flags_to_output =
        run_icefree(params;
                     start_time, end_time = 0, main_mode = false, time_steps_per_year = Δtsperyear,
                     seabedtilt_init, island_block,
                     output_filename_stem, output_parent_dir,
                     output_time_as_years, output_include_flags, output_include_strata,
                     steps_per_output, inheritance
                    )

    # Add some information to the output NetCDF file as global attributes.
    ncputatt(output_fullpath_nc, "", Dict(
                                          "time-step size [s]" => Δt,
                                          "ramp-up steps" => ramp_up,
                                          "time-steps per year" => Δtsperyear,
                                          "days per model-year" => days_per_year
                                          ))
    # 💡: These are visible using `ncinfo()` and retrivable using `ncgetatt()`.

    # Unpack the fields of all gridded variables so that their call-names are available individually.
    @unpack waters, water_tracking, seabed, sediment_transport, boundaries = all_gridded_vars

    # Atmospheric melting per time step, Δt:
    # First, check if the `days_before_melt_start` translates to less than 3 time steps for ice melting.
    Δtsbeforemelt = round(Int, days_before_melt_start / daysperΔt)
    @assert Δtsperyear - Δtsbeforemelt > 2 "There are not enough time steps to numerically stably melt ice after " *
        "`days_before_melt_start`.\n3 time steps are needed in each model year for ice-melting to proceed fully."
    # Second, acquire the shape of the melting time profile.
    if melt_time_profile ≡ :linear
        meltfunc = x -> 1
    else
        meltfunc = getfield(@__MODULE__, melt_time_profile)
    end
    # Third, how many time steps does `days_to_melt_ice` translate to?
    Δtstomelt = round(Int, days_to_melt_ice / daysperΔt)
    # Fourth, get the actual atmospheric melting as an iterator (repeating every model year).
    meltiterator = Iterators.cycle(quadrature_weights(meltfunc, Δtsperyear;
                                                      forepadzeros = Δtsbeforemelt,
                                                      aftpadzeros = Δtsperyear - Δtsbeforemelt - Δtstomelt))

    # Initialise the postive-degree-day index, for determining thaw depths.
    # The spatially varying nature is due to bed-fast ice preventing thaw.
    𝕀⁺° = zeros(Float64, params.Nx, params.Ny)

    # Completed model year (excluding the ramp-up period), starting from 0.
    yearsgone::Int = 0

    # Set the iterator for the time loop, in order to have a progress bar, and
    # set its description to distincguish it from the ramp-up phase.
    timeiterator = ProgressBar(1:lasttime)
    set_description(timeiterator, "Main phase (with ice):")

    for (timestep::Int, meltfrac) in zip(timeiterator, meltiterator)

        # Are we at the beginning of another melting season?
        # If yes, impose the maximum ice thickness and extent for subsequent melting.
        # If not, update the ice thickness due to discharge-induced and atmospheric melting.
        # Update also the positive-degree-day index (to be used for sediment thawing later).
        if timestep % Δtsperyear ≡ 1
            impose_winter_ice!(𝕀⁺°, params, waters; I⁺°₀ = 10.0)
        else
            melt_ice!(𝕀⁺°, params, waters, å * hᵢmax * meltfrac)
        end

        # Define an time-step index that is used to index into the time series of discharges (e.g. Qw₀)
        tᵢ = length(params.Qw₀) ≡ 1 ? 1 : timestep + ramp_up

        # Set the thaw depth for the current time step.
        thaw_stefans!(𝕀⁺°, seabed)

        # Re-impose inlet boundary condition for discharge. (This is here to handle time-varying discharges.)
        reimpose_boundary_conds_dischg!(tᵢ, params, waters)

        # Perform iterations of water-packet random walk and the associated surface-elevation calculations to
        # determine the flow field, which, in turn, influences the water's flow path and surface elevation, until
        # all are (hopefully) stabilised.
        flow_iteration!(
                        nextstep_withice,
                        timestep ≡ start_time,
                        tᵢ,
                        params, waters, water_tracking, sea_surface_height, boundaries
                        )

        # Spawn and determine the paths of `ns * f_sand` number of sand packets, depositing or eroding along the way,
        # if the conditions are right.
        round_of_sand!(nextstep_withice, tᵢ, params, waters, seabed, sediment_transport, boundaries)

        # Spawn and determine the paths of `ns * (1 - f_sand)` number of mud packets, depositing or eroding along the
        # way, if the conditions are right.
        round_of_mud!(nextstep_withice, tᵢ, params, waters, seabed, sediment_transport, boundaries)

        # Topographic diffusion of the bed.
        bed_diffusion!(params, waters, seabed, sediment_transport, boundaries)

        # Adjust shorelines or banks, so that there are no walls of taller water than the adjacent dry land.
        migrate_shore_bank!(params, waters)

        # Reimpose boundary conditions concerning water depth, h₀; surface elevation is unchanged.
        reimpose_boundary_conds_depth!(tᵢ, params, waters)

        # Register the mass-exchanges determined above into the sedimentary records.
        sedimentary_records_withice!(
                                     muladd(timestep, yearsperΔt, yearsgone),
                                     params,
                                     waters,
                                     seabed,
                                     sediment_transport,
                                     timestep % Δtsperyear ≡ 0 ? yearsgone += 1 : typemin(yearsgone)
                                     )
        #= A note on `timestep % Δtsperyear ≡ 0 ? yearsgone += 1 : typemin(yearsgone)`:
        The above step includes a check and update for a model-year's end.
        If the time-step number is at an integer multiple of the number of time-steps in a model year (also rounded),
        then increment `yearsgone` by 1 and pass it as an argument into sedimentary_records_withice!; otherwise, pass
        the minimum representable value of the type of yearsgone (which must be an integer type).
        =#

        # Write the state-variables arrays at this time step into the output file.
        (lasttime - timestep) % steps_per_output ≡ 0 &&
            write_statevars(output_fullpath_nc, (timestep - start_time) ÷ steps_per_output + 1, params, waters)

        # Optionally(*), the status flags can also be recorded (e.g. for debugging), enabled by `write_flags_to_output`.
        write_flags_to_output && (lasttime - timestep) % steps_per_output ≡ 0 &&
            write_stateflags(output_fullpath_nc, (timestep - start_time) ÷ steps_per_output + 1, params, waters, boundaries)
        # *Note: The function `init_outputfile()` in the source file "init.jl" has keyword arguments that
        #        include the option to record status flags; see its documentation for details.

    end # for timestep::Int in timeiterator

    # Write the sediment-strata data into a Julia Data File (JLD2).
    output_include_strata && write_strata(output_fullpath_jld, params, seabed)

    # If this simulation is to be inherited and continued, save its ending state.
    to_be_continued && save_state(output_fullpath_ss, params, all_gridded_vars, sea_surface_height)

    return nothing

end

## ====================================================================================================== run_icefree ##
# Simulation Under Non-Arctic Conditions
"""
    run_icefree(params::Params;
                 start_time::Int = 1, end_time::Int = lasttime,
                 main_mode::Bool = (end_time ≡ lasttime),
                 time_steps_per_year::Int = round(Int, inv(yearsperΔt)),
                 seabedtilt_init::Vector = [], island_block::Vector = [],
                 output_filename_stem::String = "default", output_parent_dir::String = pwd(),
                 output_time_as_years::Bool = false, output_include_flags::Bool = false,
                 output_include_strata::Bool = true, steps_per_output::Int = 1,
                 to_be_continued::Bool = false, inheritance::String = "none"
                )

Run an ice-free DeltaRCM simulation.

*About the optional keyword arguments:*

The simulation will run from time-step number `start_time` (default = 1) until time-step
number `end_time` (default = `lasttime`, which is a global constant) inclusive. When run
with `main_mode = true` (default as long as `end_time ≡ lasttime`), variables and output
files corresponding to an ice-free case will be allocated/created and initialised, and the
simulation will end with completed output files. Explicitly setting `main_mode` to false or
an input to `end_time` such that `end_time ≢ lasttime` will signify that the simulation is
called as the “ramp-up” phase of an Arctic DeltaRCM simulation. In this case, variables and
output files will be setup with Arctic-related quantities, and the simulation will not end
with completed output files. Instead, it will return the variables for the calling function
to continue the simulation under Arctic conditions. To that end, `time_steps_per_year` is
provided as an optional input so that the Arctic output file is created with the correct
time dimension (default is simply the number of whole time steps that can fit in one year).

The output files will be saved to an "OUTPUT" directory under the path `output_parent_dir`
(default is the output of `pwd()`) with the filename stem of `output_filename_stem` (default
is "default") to which suffixes "_statevars.nc" and "_strata.jld2" will be appended. Options
to save the time as years, to include flags (e.g. `dry_flag`), and to save strata in the
output files are given by `output_time_as_years` (default is false), `output_include_flags`
(default is false), and `output_include_strata` (default is true), respectively. To conserve
disk space, an option to write output at time-step intervals of `steps_per_output` (default
is 1; i.e., output at every step) is provided.

If this simulation were to be continued in the future (i.e., to enable the ability for
another simulation to inherit the ending state of the current one), set `to_be_continued` to
true. A saved-state file with the same filename stem and suffix "_savedstate.jld2" will be
saved along with the other output files.

If this simulation were to inherit the ending state of another one, provide the keyword
argument, `inheritance`, with the full path to the "*_savedstate.jld2" file, from which the
starting state of this simulation will be read. The code will also look for a corresponding
strata record file (with the suffix "_strata.jld2") that has the same path and filename stem
(i.e., before the "_savedstate.jld2" suffix). **Both files must be present with the same
filename stem.**
"""
function run_icefree(params::Params;
                      start_time::Int = 1, end_time::Int = params.lasttime,
                      main_mode::Bool = (end_time ≡ params.lasttime),
                      time_steps_per_year::Int = round(Int, inv(params.yearsperΔt)),
                      seabedtilt_init::Vector = [], island_block::Vector = [],
                      output_filename_stem::String = "default", output_parent_dir::String = pwd(),
                      output_time_as_years::Bool = false, output_include_flags::Bool = false,
                      output_include_strata::Bool = true, steps_per_output::Int = 1,
                      to_be_continued::Bool = false, inheritance::String = "none"
                     )

    # Unpack the parameters used below
    @unpack Nx, Ny, lasttime, yearsperΔt, backwater_method! = params

    # Check that the predominate floating-point type matches the system's default floating-point type.
    @assert 𝔽 ≡ typeof(0.0) "This code is not designed to run with 𝔽 being different from system's default\n" *
    "float type. The source code's use of 𝔽 instead of Float64 or Float32 for type-\n" *
    "assert is to accomodate 64- & 32-bit systems. Ensure that 𝔽 is equal to \n" *
    "typeof(0.0) in “src/constants.jl”.";

    # Ensure the method set for backwater calculation is an appropriate one; stop the programme if that's not the case.
    backwater_check(backwater_method!) || return

    # Information print to Standard Out.
    @info "Running on a simulation domain with the size:" Nx, Ny;

    # Preallocate the required (non-global-constant) variables, mostly as Composite Types (i.e., `structs`).
    all_gridded_vars, sea_surface_height = preallocate(params)

    # Unpack the fields of all gridded variables so that their call-names are available individually.
    @unpack waters, water_tracking, seabed, sediment_transport, boundaries = all_gridded_vars

    # If not inheriting a previously saved state as the starting state:
    if inheritance ≡ "none"

        # Initialise the starting state of the simulation.
        populate_initial_state!(params, all_gridded_vars, sea_surface_height)

        # Impose a tilt on the initial bed elevation, if specified.
        seabedtilt_sculpter_!(seabedtilt_init, params, waters, seabed)

        # Create island block where no flows can go, if specified.
        island_builder_!(island_block, waters, boundaries)

    else # Otherwise:

        # Inherit variables into the starting state of the simulation.
        inherit_previous_state!(inheritance, params, all_gridded_vars, sea_surface_height)

    end # if inheritance ≡ "none"

    # Output Specifications:
    # Create and initialise the output NetCDF file for the state variables.
    # Optionally(*), the status flags can also be recorded (e.g. for debugging); this option will be passed on to
    # the write-out commands later in the code.
    # *Note: The function `init_outputfile()` has keyword arguments; see documentation for details.
    output_fullpath_nc, output_fullpath_jld, output_fullpath_ss, write_flags_to_output =
        init_outputfile(
                        start_time, time_steps_per_year, output_filename_stem, output_parent_dir, params;
                         steps_per_output = steps_per_output, ice_free = main_mode,
                         give_time_in_years = output_time_as_years, include_flags = output_include_flags
                        )

    # Set the iterator for the time loop, in order to have a progress bar.
    # (This would be a simple `start_time:end_time` of type `UnitRange{Int}` if `start_time` ≤ `end_time`.)
    timeiterator = end_time - start_time > 0 ? ProgressBar(start_time:end_time) : start_time:end_time

    # Either this simulation is fully ice-free, OR it is the ramp-up phase of another simulation.
    # In the latter case, give a descriptor to the progress bar for clarity (if it is indeed a `ProgressBar`).
    main_mode || (typeof(timeiterator) <: ProgressBar && set_description(timeiterator, "Ramp-up phase (ice-free):"))

    for timestep::Int in timeiterator

        # Define an time-step index that is used to index into the time series of discharges (e.g. Qw₀)
        tᵢ = length(params.Qw₀) ≡ 1 ? 1 : timestep

        # Re-impose inlet boundary condition for discharge. (This is here to handle time-varying discharges.)
        reimpose_boundary_conds_dischg!(tᵢ, params, waters)

        # Perform iterations of water-packet random walk and the associated surface-elevation calculations to
        # determine the flow field, which, in turn, influences the water's flow path and surface elevation, until
        # all are (hopefully) stabilised.
        flow_iteration!(
                        nextstep_icefree,
                        timestep ≡ start_time,
                        tᵢ,
                        params, waters, water_tracking, sea_surface_height, boundaries
                        )

        # Spawn and determine the paths of `ns * f_sand` number of sand packets, depositing or eroding along the way,
        # if the conditions are right.
        round_of_sand!(nextstep_icefree, tᵢ, params, waters, seabed, sediment_transport, boundaries)

        # Spawn and determine the paths of `ns * (1 - f_sand)` number of mud packets, depositing or eroding along the
        # way, if the conditions are right.
        round_of_mud!(nextstep_icefree, tᵢ, params, waters, seabed, sediment_transport, boundaries)

        # Topographic diffusion of the bed.
        bed_diffusion!(params, waters, seabed, sediment_transport, boundaries)

        # Adjust shorelines or banks, so that there are no walls of taller water than the adjacent dry land.
        migrate_shore_bank!(params, waters)

        # Reimpose boundary conditions concerning water depth, h₀; surface elevation is unchanged.
        reimpose_boundary_conds_depth!(tᵢ, params, waters)

        # Register the mass-exchanges determined above into the sedimentary records.
        sedimentary_records_icefree!(timestep * yearsperΔt, params, waters, seabed, sediment_transport)

        # Write the state-variables arrays at this time step into the output file; at set intervals only!
        (lasttime - timestep) % steps_per_output ≡ 0 &&
            write_statevars(output_fullpath_nc, (timestep - start_time) ÷ steps_per_output + 1, params, waters)
        #= NB:
        `timestep`, which can be negative, is offset by `-start_time` then `+1` after dividing by `steps_per_output`
        so that the first time step to be written corresponds to element 1 in the corresponding arrays in the NetCDF
        file; negative indexing is not supported. The actual start time is recorded in the `time` variable saved in the
        NetCDF file. (Same for below.)
        =#

        # Optionally(*), the status flags can also be recorded (e.g. for debugging), enabled by `write_flags_to_output`.
        write_flags_to_output && (lasttime - timestep) % steps_per_output ≡ 0 &&
            write_stateflags(output_fullpath_nc, (timestep - start_time) ÷ steps_per_output + 1, params, waters, boundaries)
        # *Note: The function `init_outputfile()` in the source file "init.jl" has keyword arguments that
        #        include the option to record status flags; see its documentation for details.

    end # for timestep::Int in ProgressBar(start_time:end_time)

    # If this is a stand-alone, ice-free simulation:
    if main_mode

        # Write the sediment-strata data into a Julia Data File (JLD2).
        output_include_strata && write_strata(output_fullpath_jld, params, seabed)

        # If this simulation is to be inherited and continued, save its ending state.
        to_be_continued && save_state(output_fullpath_ss, params, all_gridded_vars, sea_surface_height)

        return nothing

    else # Otherwise, this is the ice-free portion of another simulation:

        # Return the output files' information.
        return all_gridded_vars, sea_surface_height,
               output_fullpath_nc, output_fullpath_jld, output_fullpath_ss, write_flags_to_output

    end # if main_mode

end

end # End of `module ArcDelRCM`
