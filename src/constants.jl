#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: const.jl

Define all the constant (global) parameters and (custom) composite variable types necessary
for an ArcDelRCM (Arctic Delta RCM) simulation.
=#

include("types.jl")

# [================================================ Table of Contents =================================================]
# ℊ, sqrt2, sqrt½, sperday, speryear, #........................................................................ [- 1 -]
# kernel_addneighbours, kernel_diffneighbours, #................................................................ [- 2 -]
# 𝐝ᵢ, Δᵢ, onestepx, onestepy, #................................................................................. [- 3 -]
# 𝔽, 𝔽𝕤, 𝟘 #..................................................................................................... [- 4 -]

#------- Universal and Physical Constants -------#                                                               [- 1 -]
"ℊ, the gravitational acceleration on the Earth's surface."
const ℊ = 9.80065

"Density ratio between ice and water, ρᵢ / ρₕ₂ₒ."
const ρiρw = 0.917

"Frequently used square-root values."
const sqrt2, sqrt½ = sqrt(2), sqrt(0.5)

"Seconds per day."
const sperday = 86400

"Seconds per 365.25 days of exactly 86400 seconds (Julian astronomical year)."
const speryear = 31557600

#------- Kernels for Diffusive Filtering -------#                                                                [- 2 -]
"When convolved with this kernel, each cell will contain the sum of all its 8 neighbouring
cells."
const kernel_addneighbours = [1 1 1; 1 0 1; 1 1 1]

"When convolved with this kernel, each cell will contain the sum of all its 8 neighbouring
cells minus 8 times itself."
const kernel_diffneighbours = [1 1 1; 1 -8 1; 1 1 1]

#------- Directions and Distances -------#                                                                       [- 3 -]
# It may help to visualise the grid as follows:
#
#        ⬇  Inlet is from the top ⬇
#    ____________________________________
#   |  1, 1 |  1, 2 |  1, 3 | ⋯ |  1, Ny |
#   |  2, 1 |  2, 2 |  2, 3 | ⋯ |  2, Ny |
#   |  3, 1 |  3, 2 |  3, 3 | ⋯ |  3, Ny |
#       ⋮        ⋮       ⋮     ⋱     ⋮
#   | Nx, 1 | Nx, 2 | Nx, 3 | ⋯ | Nx, Ny |
#    ————————————————————————————————————
#
# This way, the indexing of any distance- or directional matrices (3-by-3) and
# vectors (9-element) related to a single step are as follows:
#
#        ⬇  Inlet is from the top ⬇
#                     ⋮
#             ___________
#            | 1 | 4 | 7 |
#   ⋯        | 2 | 5 | 8 |        ⋯
#            | 3 | 6 | 9 |
#             ———————————
#                     ⋮
# where 5 represents the current/original location.
# # # # # # # # # # # # # # # # # # # # # #

"Cellular direction (unit) vector, 𝐝ᵢ, from the central cell towards each of the 8
neighbouring cells. Each element corresponds to a 3-by-3 grid, with column-major ordering:
increasing x going down, and increasing y going right, origin in the centre."
const 𝐝ᵢ = (
            (-sqrt½, -sqrt½), (0., -1.), (sqrt½, -sqrt½), (-1., 0.),
            (0., 0.),
            (1., 0.), (-sqrt½, sqrt½), (0., 1.), (sqrt½, sqrt½)
            )

"Cellular distance vector, Δᵢ, between each of the adjacent cells.
Each element corresponds to a 3-by-3 grid, with column-major ordering:
increasing x going down, and increasing y going right, origin in the centre."
const Δᵢ = (√2, 1., √2, 1., 0., 1., √2, 1., √2)

const onestepx = (-1,  0,  1, -1, 0, 1, -1, 0, 1)
const onestepy = (-1, -1, -1,  0, 0, 0,  1, 1, 1)
"The x and y component-pair representing one step (to a neighbouring cell) in each
direction. Each element pair, (onestepx[i], onestepy[i]) where i ∈ 1:9, corresponds to a
3-by-3 grid, with column-major ordering: increasing x going down, and increasing y going
right, origin in the centre."
onestepx, onestepy

#------- Computational Parameters -------#                                                                       [- 4 -]
"System's default floating-point precision (𝔽 for floating-point)."
const 𝔽 = typeof(0.0) # NB: MUST be the system-default floating-point type!

"Precision at which to store sediment strata (𝔽𝕤 for floating-point for strata)."
const 𝔽𝕤 = Float32 # For the system default, use: `typeof(0.0)`.

const 𝟘⁺ = eps(𝔽(0)) * 2^(precision(𝔽) - 1)
const 𝟘⁻ = eps(𝔽(0)) * 2^(precision(𝔽) - 1) - eps(𝔽(0))
"Subnormal/Denormal number limits. (See https://en.wikipedia.org/wiki/Denormal_number for
more information.)"
𝟘⁺, 𝟘⁻
#=
Under IEEE 754 binary format:
double-precision (Float64) numbers have 52 bits in its significand field;
single-precision (Float32) numbers have 23 bits in its significand field.

The smallest representable number around zero for type 𝔽 is

    eps(𝔽(0)) ,

and the number of bits in the significand in type 𝔽 is

    precision(𝔽) .

However, `precision(𝔽)` gives the number of bits including the hidden bit; the actual
number of bits is 1 less.

To calculate the smallest number that still contains the full precision of the floating-
point type, take the smallest representable number and “shift” it up the number of bits
available in the significant (i.e., by multiplying 2 to the power of number of bits):

    Smallest “normal” number = eps(𝔽(0)) * 2^(precision(𝔽) - 1) .

One can verify this limiting value by running the following commands:
```
issubnormal(eps(𝔽(0)) * 2^(precision(𝔽) - 1))             # returns false
issubnormal(eps(𝔽(0)) * 2^(precision(𝔽) - 1) - eps(𝔽(0))) # returns true
```
NB: This global constant is not needed if `set_zero_subnormals(true)` is executed; however,
there are occasions when setting `set_zero_subnormals(true)` leads to a point or two with
unphysical bed elevations (e.g. a spike or hole of ~ 70 m one or two pixels across) late
in the simulation. These may be the result of the delicate balance between the elevations,
depths, ice thickness, and flow speed: Even if some of them are not equal, their differences
may be treated as subnormal zero (e.g. during the weighted random walk calculation), which
may build up to inconsistencies that have latent, sporadic (and undesirable) effects. Given
that benchmark tests between `set_zero_subnormals(true)` and `set_zero_subnormals(false)`
have the same performance, it is safer to refrain from setting `set_zero_subnormals(true)`
globally. Instead, use the 𝟘⁺, 𝟘⁻ constants in the few cases where we actually want a
subnormal number to be treated as zero (e.g. when deciding whether or not the random-walk
weights sum to zero).

Note that the instability was only observed in the Arctic runs. The ice-free simulations do
not appear to display any instability after many tests.
=#

#------------------------------------------------------#
# Ways to type the unicode symbols in Julia's Juno IDE:
# (symbol : code)
# 𝟘  : \bbzero
# ½  : \1/2
# Δ  : \Delta
# 𝐝  : \bfd
# 𝔽  : \bbF
# ℊ : \scrg
# ᵢ  : \_i
# 𝕤  : \bbs
# ⁺  : \^+
# ⁻  : \^-
