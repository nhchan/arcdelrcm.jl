#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: support.jl

Define all the functions that are called by the main procedures in an ArcDelRCM (Arctic
Delta RCM) simulation. These are mostly support functions that performs some kind of
detection, direction picking or relocation, and frequently used arithmetic. Unlike the main
procedures in "src/procedures.jl", the functions herein take the variables (with concrete
types) directly, rather than taking Composite-Typed struct of variables.


=========================================
|           TABLE of CONTENTS           |
|          (look-up along the           |
|            right-hand edge            |
|             of this file)             |
-----------------------------------------
#      ⟦ Area and Classification ⟧      #
## ======================== delta_area ##
## ======================= is_on_delta ##
## ====================== locate_shore ##
#     ⟦ Flow Handling and Updates ⟧     #
## ===================== loop_disposal ##
## ======================= sculpt_ice! ##
## ==== HOF: update_flow_velocity_HOF! ##
## ------ Alias: update_flow_velocity! ##
## ====== update_water_depth_dry_flag! ##
#      ⟦ Sediment Mass Exchanges ⟧      #
## ================== deposit_stratum! ##
## ==================== erode_stratum! ##
## =================== freeze_or_thaw! ##
## ==================== topo_depo_ero! ##
## ===(currently unused) active_layer! ##
## ===(currently unused) frosty_layer! ##
#     ⟦ Arithmetic & Random Helpers ⟧   #
## ================== nothingmeanszero ##
## ============================ nextup ##
## ================ quadrature_weights ##
## ========================== roulette ##
## ======================== underrelax ##
## ================== HOF: wheretonext ##
## -------------- Helper: rand_helper_ ##
## ----------- Alias: nextstep_icefree ##
## ----------- Alias: nextstep_withice ##
## ------------ Helper: w_inertia_H2O_ ##
## ------------ Helper: w_inertia_ICE_ ##
## ---------------- Helper: w_surface_ ##

=#

import LinearAlgebra: normalize!

#------- Directions and Distances -------#
# It may help to visualise the grid as
# follows:
#
#        ⬇  Inlet is from the top ⬇
#    ____________________________________
#   |  1, 1 |  1, 2 |  1, 3 | ⋯ |  1, Ny |
#   |  2, 1 |  2, 2 |  2, 3 | ⋯ |  2, Ny |
#   |  3, 1 |  3, 2 |  3, 3 | ⋯ |  3, Ny |
#       ⋮        ⋮       ⋮     ⋱     ⋮
#   | Nx, 1 | Nx, 2 | Nx, 3 | ⋯ | Nx, Ny |
#    ————————————————————————————————————
#
# This way, the indexing of any distance-
# or directional matrices (3-by-3) and
# vectors (9-element) related to a single
# step are as follows:
#
#        ⬇  Inlet is from the top ⬇
#                     ⋮
#             ___________
#            | 1 | 4 | 7 |
#   ⋯        | 2 | 5 | 8 |        ⋯
#            | 3 | 6 | 9 |
#             ———————————
#                     ⋮
# where 5 represents the current/original
# location.
# # # # # # # # # # # # # # # # # # # # # #
## ======================================================================================================= delta_area ##
"""
    delta_area(hΔ, uΔ, water_depth, flow_speed)

Calculate the area of the delta in terms of number of domain grid cells, based on the same
criteria as those used by [`is_on_delta`](@ref). Return the integer area.

The arguments `water_depth` and `flow_speed` must be numeric arrays.
"""
function delta_area(hΔ, uΔ, water_depth, flow_speed)

    area::𝔽 = 0

    @inbounds @simd for lineari in eachindex(water_depth, flow_speed)

        area += water_depth[lineari] ≤ hΔ || flow_speed[lineari] ≥ uΔ
        #= Note:
        The expression to the right of `+=` in the line above is intentionally hard-coded rather than calling
        `is_on_delta`, which would have yielded identical results. This goes against the mantra of “not copy-
        and-pasting code”. However, possibly due to more flexibility of the compiler in handling the explicit code
        rather than passing the variable pointers to a function call (even if inlined), the explicit expression gives
        twice the speed. Since the code is simple enough, unlikely to require modifications, and in close proximity to
        the definition of `is_on_delta`, a decision was made to write out the expression explicity.
        =#

    end

    return area

end

## ====================================================================================================== is_on_delta ##
"""
    is_on_delta(indices, water_depth, flow_speed, hΔ, uΔ)

Return whether a cell is on the delta or not, based on the cell's (i.e., local) water
depth and flow speed. If neither conditions are satisfied, the cell is not on the delta,
but in the ocean.

These are the two conditions that define an ocean when both are met:

1. ``η < η_{shore}``, where ``η`` is the local bed elevation; and
   ``η_{shore} ≡ H_{sealevel} - 0.1 × h_{ref}``, where ``H_{sealevel}`` is the sea-surface
   elevation and ``h_{ref}`` is some reference flow depth. Assuming that the first cell to
   satisfy ``η = η_{shore}`` has the surface elevation of ``H_{sealevel}``, we can write

``
        η = H_{sealevel} - h ,
``

   where ``h`` is the local flow depth. This means this condition can be re-written as

``
        H_{sealevel} - h < H_{sealevel} - 0.1 × h_{ref} ,
``

   or, ``h > 0.1 × h_{ref}``.

   We name the variable holding this `h` value, `hΔ`. Typically, `hΔ` is about 0.5 metres.

2. ``|𝐮| < u_{shore}``, where ``|𝐮|`` is the local flow speed, and
   ``u_{shore} ≡ ½ u_{ref}``, where ``u_{ref}`` is some reference speed.

   We name the variable holding this ``u_{shore}`` value, `uΔ`.
"""
@inline function is_on_delta(indices, water_depth, flow_speed, hΔ, uΔ)

    return water_depth[indices...] ≤ hΔ || flow_speed[indices...] ≥ uΔ

end

## ===================================================================================================== locate_shore ##
"""
    locate_shore(last_step, path_history, flow_depth, flow_speed, hΔ, uΔ)

Trace the path history of a water packet from the end backwards, and return the step number
when the packet reach the “shoreline” (landward side). The determination of whether or not
a cell is on the delta (vs. the ocean) is carried out by [`is_on_delta`](@ref). The step
numbers are defined as ascending integers from 1 at the inlet cell.

!!! warning
    This assumes that the water packet's path ended in the ocean. Check that the last step
    of the water's path is indeed in the ocean. There are no built-in checks!

# Example
```jldoctest
julia> hΔ = 4.5; uΔ = 0.5;

julia> using Random; Random.seed!(1234);

julia> path_hist = CartesianIndices((1:5, 1:10))[rand(1:50, 10)]
10-element Array{CartesianIndex{2},1}:
 CartesianIndex(5, 9)
 CartesianIndex(4, 5)
 CartesianIndex(2, 3)
 CartesianIndex(3, 5)
 CartesianIndex(4, 6)
 CartesianIndex(5, 4)
 CartesianIndex(3, 1)
 CartesianIndex(1, 4)
 CartesianIndex(5, 2)
 CartesianIndex(4, 1)

julia> flowdepth = fill(200.0, (5, 10)); flowspeed = fill(0.0, (5, 10));

julia> flowdepth[4, 6] = 0.0; flowspeed[5, 4] = 𝔽(u₀);

julia> locate_shore(10, path_hist, hΔ, uΔ, flowdepth, flowspeed)
6
```
"""
function locate_shore(last_step, path_history, water_depth, flow_speed, hΔ, uΔ)

    # Is this cell on the delta or is this the originating cell? If yes, return the corresponding step number.
    # (If the first condition evaluates to false, the return command will not be evaluated.)
    (is_on_delta(Tuple(path_history[last_step]), water_depth, flow_speed, hΔ, uΔ) || last_step ≡ 1) && return last_step

    # Recursion: do exactly the same thing for the next (backwards) step along the packet's path.
    locate_shore(last_step - 1, path_history, water_depth, flow_speed, hΔ, uΔ)

    #=
    Although the step returned is a step immediately landwards from the theoretical, infinitesimally thin shoreline, it
    is treated as a shoreline cell, and will be assigned the sea-surface height.
    =#

end

## ==================================================================================================== loop_disposal ##
# “Teleport” a Packet to Five Unit-vector-length Away from the “Origin” (x = 1, y = Ny½)
"""
    px, py = loop_disposal(px, py, Nx, Ny, Nwall, Ny½)

Relocate a packet from the input coordinate to a new coordinate five unit vectors away, in
the direction away from the “origin”, which is at the centre of the inlet channel, against
the inlet boundary. The operation is subject to discretisation, of course. Return the new
location's x, y coordinates as integers.

# Example
```jldoctest
julia> Nx, Ny = 60, 120; Ny½ = Ny ÷ 2; Nwall = 3;

julia> loop_disposal(4, 30, Nx, Ny, Nwall, Ny½)
(4, 25)
```
"""
@inline function loop_disposal(px, py, Nx, Ny, Nwall, Ny½)

    # Compute the distance from the location where the water packet intercepted its own path
    rx::Int = px - 1
    ry::Int = py - Ny½
    r = √(abs2(rx) + abs2(ry))

    # Teleport the water packet roughly 5 times the distance between it and the “origin” of the inlet, in the direction
    # away from that “origin”, but limit its new location (i.e., clamp it down) to within the valid simulation domain.
    px = clamp(px + 5 * round(Int, rx / r), Nwall + 1, Nx - 1)
    py = clamp(py + 5 * round(Int, ry / r), 2        , Ny - 1)

    return px, py

end

## ====================================================================================================== sculpt_ice! ##
# Lay Down in Each Grid Cell the Appropriate Ice Thickness Representing the Maximum Winter Ice Cover
"""
    sculpt_ice!(ice_free_radius, ice_full_radius, Nwall, Ny½, hᵢmax, I⁺°₀, 𝕀⁺°, water_depth, ice_thickness)

Determine the exact thickness of ice at each grid cell when ice is at its maximum thickness
and extent. This includes the ten-grid-cell tapering from full thickness on the ocean-ward
area to exactly zero at just beyond the ice-extent boundary. Also assign positive-degree-
days index according to the ice thickness and water depth at each grid cell: If the ice-
thickness is such that the ice will not float, 0.0 is assigned; otherwise, the offset value
𝕀₀ is assigned (see the documentation for [`impost_winter_ice!`](@ref) in `procedures.jl`
for details).

The arguments `𝕀⁺°`, `water_depth` and `ice_thickness` must be numeric arrays.

Input arguments `ice_thickness` and `𝕀⁺°` are modified in-place.

---
Requires the following global constant:
  * ρiρw    Density ratio between ice and water, ρᵢ / ρₕ₂ₒ.
"""
function sculpt_ice!(ice_free_radius, ice_full_radius, Nwall, Ny½, hᵢmax, I⁺°₀, 𝕀⁺°, water_depth, ice_thickness)

    # Go through each cell of the grid (the portion excluding the inlet wall and channel, via `@view` to avoid copying)
    # using Cartesian indexing.
    @inbounds @simd for cartesiani in @view CartesianIndices(ice_thickness)[(Nwall + 1):end, 1:end]

        # The distance from the cell to the mid-point at the inlet-channel exit.
        r = √( abs2(cartesiani[1] - Nwall) + abs2(cartesiani[2] - Ny½) )

        # If the cell is ocean-wards from the radius of maximum ice thickness:
        if r > ice_full_radius

            # Assign maximum thickness, up to 99.99% of the local water depth.
            ice_thickness[cartesiani] = min(hᵢmax, 0.9999 * water_depth[cartesiani])

        elseif r ≤ ice_free_radius # Otherwise, if the cell is inlet-wards from the ice-free radius:

            # Assign zero ice thickness.
            ice_thickness[cartesiani] = 0.0

        else # Otherwise:

            # Assign the tapering part of the ice thickness, up to 99.99% of the local water depth.
            ice_thickness[cartesiani] = min(0.1 * hᵢmax * (r - ice_free_radius), 0.9999 * water_depth[cartesiani])

        end # if r > ice_full_radius

        # Assign the 0.0 plus any positive-degree-day offset, 𝕀₀, if the ice is not "bed-fast";
        # otherwise, simply assign 0.0.
        𝕀⁺°[cartesiani] = ifelse(ice_thickness[cartesiani] < 0.9999 * water_depth[cartesiani], I⁺°₀, 0.0)

    end

end

## ======================================================================================== update_flow_velocity_HOF! ##
# Implement the Changes to Flow Fields after Deposition or Erosion
"""
    update_flow_velocity_HOF!(
                              flow_speed_anonymous::Function,
                              indices,
                              uₘₐₓ,
                              water_depth,
                              flow_speed_x, flow_speed_y,
                              flow_speed,
                              unit_dischg_x, unit_dischg_y,
                              unit_dischg,
                              args...
                              )

Update the flow velocity after the water flow field has converged, or after deposition or
erosion. Since this is a higher-order function set up specifically for the alias functions
to utilise multiple dispatch by taking over type assertions, it does not itself contain any
type assertions (e.g., `::Array{𝔽,2}`), unlike most of the other functions in this
package.

`indices` can either be a linear index or a tuple of x, y indices.

Input arguments `flow_speed_x`, `flow_speed_y`, and `flow_speed` are modified in-place.
"""
Base.@propagate_inbounds @inline function update_flow_velocity_HOF!(
                                           flow_speed_anonymous::Function,
                                           indices,
                                           uₘₐₓ,
                                           water_depth,
                                           flow_speed_x, flow_speed_y,
                                           flow_speed,
                                           unit_dischg_x, unit_dischg_y,
                                           unit_dischg,
                                           args...
                                           )

    # Update the flow-speed magnitude, up to a maximum of `uₘₐₓ`.
    flow_speed_anonymous(indices, uₘₐₓ, water_depth, flow_speed, unit_dischg, args...)

    # Decompose flow speed into x- and y- components according to flow direction indicated by the discharge.
    flow_speed_x[indices...] = flow_speed[indices...] * unit_dischg_x[indices...] / unit_dischg[indices...]
    flow_speed_y[indices...] = flow_speed[indices...] * unit_dischg_y[indices...] / unit_dischg[indices...]

end

## ------------------------------------------------------------------------------------- Alias: update_flow_velocity! ##
# (There are two alias methods, differing by the type of the `ice_thickness`/`nothingness` input argument.)
"""
    update_flow_velocity!(
                          indices,
                          uₘₐₓ::𝔽,
                          nothingness::Array{Nothing,2},
                          water_depth::Array{𝔽,2},
                          flow_speed_x::Array{𝔽,2}, flow_speed_y::Array{𝔽,2},
                          flow_speed::Array{𝔽,2},
                          unit_dischg_x::Array{𝔽,2}, unit_dischg_y::Array{𝔽,2},
                          unit_dischg::Array{𝔽,2}
                          )
    update_flow_velocity!(
                          indices,
                          uₘₐₓ::𝔽,
                          ice_thickness::Array{𝔽,2},
                          water_depth::Array{𝔽,2},
                          flow_speed_x::Array{𝔽,2}, flow_speed_y::Array{𝔽,2},
                          flow_speed::Array{𝔽,2},
                          unit_dischg_x::Array{𝔽,2}, unit_dischg_y::Array{𝔽,2},
                          unit_dischg::Array{𝔽,2}
                          )

Alias of [`update_flow_velocity_HOF!`](@ref). The variant with `nothingness` as the third
input argument assumes an ice-free condition. The variant with `ice_thickness::Array{𝔽,2}`
in third positional argument ice thickness into account.

When calling the ice-free variant, the `ice_thickness` (i.e, third-positional) argument
should be an array of [`nothing`](@ref) (and is disregarded); this saves some memory and
mathematical operations involving what would be all zeros under ice-free condition.
"""
update_flow_velocity!(
                      indices,
                      uₘₐₓ::𝔽,
                      nothingness::Array{Nothing,2},
                      water_depth::Array{𝔽,2},
                      flow_speed_x::Array{𝔽,2}, flow_speed_y::Array{𝔽,2},
                      flow_speed::Array{𝔽,2},
                      unit_dischg_x::Array{𝔽,2}, unit_dischg_y::Array{𝔽,2},
                      unit_dischg::Array{𝔽,2}
                      ) = update_flow_velocity_HOF!(
                                                    indices,
                                                    uₘₐₓ,
                                                    water_depth,
                                                    flow_speed_x, flow_speed_y,
                                                    flow_speed,
                                                    unit_dischg_x, unit_dischg_y,
                                                    unit_dischg
                                                    ) do indices, uₘₐₓ, water_depth, flow_speed, unit_dischg

    # Update the flow-speed magnitude, up to a maximum of `uₘₐₓ`.
    flow_speed[indices...] = min(uₘₐₓ, unit_dischg[indices...] / water_depth[indices...])

end

update_flow_velocity!(
                      indices,
                      uₘₐₓ::𝔽,
                      ice_thickness::Array{𝔽,2},
                      water_depth::Array{𝔽,2},
                      flow_speed_x::Array{𝔽,2}, flow_speed_y::Array{𝔽,2},
                      flow_speed::Array{𝔽,2},
                      unit_dischg_x::Array{𝔽,2}, unit_dischg_y::Array{𝔽,2},
                      unit_dischg::Array{𝔽,2}
                      ) = update_flow_velocity_HOF!(
                                                    indices,
                                                    uₘₐₓ,
                                                    water_depth,
                                                    flow_speed_x, flow_speed_y,
                                                    flow_speed,
                                                    unit_dischg_x, unit_dischg_y,
                                                    unit_dischg,
                                                    ice_thickness
                                                    ) do indices,
                                                         uₘₐₓ, water_depth, flow_speed, unit_dischg, ice_thickness

    # Update the flow-speed magnitude, up to a maximum of `uₘₐₓ`.
    flow_speed[indices...] = clamp(
                                   unit_dischg[indices...] / (water_depth[indices...] - ice_thickness[indices...]),
                                   0, uₘₐₓ
                                   )
    # Note: The lower bound of clamping ensures that flow speed does not become negative even if water depth drops
    #       below ice thickness during flow iterations. This will not happen when guiding sediment packets, but the
    #       performance difference between `min` and `clamp` is negligible.

end

## ===================================================================================== update_water_depth_dry_flag! ##
"""
    update_water_depth_dry_flag!(index, hdry, surf_elev, bed_elev, water_depth, dry_flag)

Update the water depth according to the surface and bed elevations, then update the dry
flag accordingly. If the bed elevation is higher than the surface elevation, water depth
is limited to zero.

The third through fifth arguments must be numeric arrays, and the last argument must be a
boolean or bit array.
"""
Base.@propagate_inbounds @inline function update_water_depth_dry_flag!(
    index, hdry, surf_elev, bed_elev, water_depth, dry_flag
    )

    # Recalculate water depth at the coordinate where this helper function is invoked.
    water_depth[index] = max(0.0, surf_elev[index] - bed_elev[index])

    # Is the water is deep enough? Yes:No —— the cell should be considered as wet:dry.
    dry_flag[index] = water_depth[index] < hdry

end

## ================================================================================================= deposit_stratum! ##
# Perform the Act of Deposition on a Sediment Column
"""
    deposit_stratum!(sediment_column, sand_fraction, current_time, layers_to_add)

Add to the sediment column (`::Array{Sediment{𝔽𝕤},1}`), one stratum at a time through a
loop for `layers_to_add` times. `current_time` is recorded “as is”.

Input argument `sediment_column` is modified in-place.

See the documentations of composite types [`Sediment`](@ref)  and [`SedimentColumn`](@ref)
for additional information.
"""
function deposit_stratum!(sediment_column, sand_fraction, current_time, layers_to_add)

    # Repeat for each layers to deposit; this loop will not execute if layers_to_add < 1.
    for nothing in 1:layers_to_add
        # Add elements to the end of the `sediment_column` vector.
        push!(sediment_column, Sediment{𝔽𝕤}(sand_fraction, current_time, [NaN]))
    end
    return layers_to_add # To inform the coller how many (discretised) layers were deposited

end

## =================================================================================================== erode_stratum! ##
# Perform the Act of Erosion on a Sediment Column
"""
    erode_stratum!(sediment_column, layers_to_zap)

Remove from the sediment column (`::Array{Sediment{𝔽𝕤},1}`), one stratum at a time
through a loop for `layers_to_zap` times.

Input argument `sediment_column` is modified in-place.

See the documentations of composite types [`Sediment`](@ref)  and [`SedimentColumn`](@ref)
for additional information.
"""
function erode_stratum!(sediment_column, layers_to_zap)

    # Repeat for each layers to erode; this loop will not execute if layers_to_zap < 1.
    for nothing in 1:layers_to_zap
        # Remove the last element in the `sediment_column` vector.
        pop!(sediment_column)
    end
    return layers_to_zap # To inform the caller how many (discretised) layers were eroded

end

## ================================================================================================== freeze_or_thaw! ##
# Assign/Erase the Frozen-since Dates of Each of the Layers in a Sediment Column
"""
    freeze_or_thaw!(sediment_column, δz, frozen_layers, year_frozen)

Step through each layer in the sediment column (`::SedimentColumn{𝔽𝕤}`) and assign the
date-of-freezing to the newly frozen ones, and erase the date-of-freezing from the newly
thawed ones.
"""
function freeze_or_thaw!(sediment_column, δz, frozen_layers, year_frozen)

    for layeri in eachindex(sediment_column)

        # If the layer is frozen and has been so for at least 2 (model) years
        if layeri ≤ frozen_layers && year_frozen - sediment_column[layeri].frostysince[1] ≥ 2

            # Set the thickness of the permafrost to the top of this layer.
            sediment_column.permaz[1] = layeri * δz

        # OR: if the layer is frozen and has not yet been assigned a date-of-freezing:
        elseif layeri ≤ frozen_layers && isnan(sediment_column[layeri].frostysince[1])

            # Assign the date-of-freezing as the current model year.
            sediment_column[layeri].frostysince[1] = year_frozen

        # OR: if the layer is above the thaw depth but has been frozen before:
        elseif layeri > frozen_layers && !isnan(sediment_column[layeri].frostysince[1])

            # Erase the date-of-freezing.
            sediment_column[layeri].frostysince[1] = NaN

        end # if

    end # for layeri in eachindex(sediment_column)

    #=
    The following line with a nested call to recusive functions `frosty_layer!` and `active_layer!` does exactly the
    same thing as the loop above (as of 8ᵗʰ June, 2020), but benchmark testing showed that the line below is roughly
    50 nanoseconds slower than the loop above. The line is preserved below, in case there would be compiler changes or
    new ideas in the future that could “turn the table”.
    =#
    # sediment_column.permaz[1] = δz * frosty_layer!(
    #                                                active_layer!(
    #                                                              length(sediment_column),
    #                                                              sediment_column,
    #                                                              frozen_layers
    #                                                              ),
    #                                                sediment_column,
    #                                                year_frozen
    #                                                )

end

## =================================================================================================== topo_depo_ero! ##
# Implement the Effect of Deposition or Erosion
"""
    topo_depo_ero!(px, py, bed_elev, water_depth, Δη)

Adjust bed elevation and water depth at a specific cell (x = px, y = py) arising from the
deposition/erosion specified:
``
    Δη = -ΔVp / δc²,
``
where Δη is the change in bed elevation due to deposition (+) or erosion (-), ΔVp is the
change in sediment volume carried by the sediment packet currently at the specified cell,
and δc² is the cell's area.

Input arguments `bed_elev` and `water_depth` must be numeric arrays and are modified
in-place.
"""
function topo_depo_ero!(px, py, bed_elev, water_depth, Δη)

    # Rise of the bed, fall of the depth; or vice versa.
    bed_elev[px, py] += Δη
    water_depth[px, py] -= Δη

end

## ==================================================================================================== active_layer! ##
# (Currently unused, but included in unit testing. Designed to be called in `freeze_or_thaw!`.)
"""
    layer_№ = active_layer!(layeri, sediment_column, frozen_layers)

Unfreeze any previously frozen sediment cells in the active layer, and return the layer
number of the top-most frozen layer.
"""
function active_layer!(layeri, sediment_column, frozen_layers)

    # Either this layer is previously unfrozen (with NaN in its `frostysince` field),
    # OR “unfreeze” it by assining NaN to its `frostysince` field.
    isnan(sediment_column[layeri].frostysince[1]) || (sediment_column[layeri].frostysince[1] = NaN)

    # If this is the layer just above the frozen layer, return the layer number of the frozen layer.
    layeri - 1 ≡ frozen_layers && return layeri - 1

    # Recursively call this function on the layer below the current one.
    active_layer!(layeri - 1, sediment_column, frozen_layers)

end

## ==================================================================================================== frosty_layer! ##
# (Currently unused, but included in unit testing. Designed to be called in `freeze_or_thaw!`.)
"""
    layer_№ = frosty_layer!(layeri, sediment_column, year_frozen)

Freeze any previously unfrozen sediment cells below the active layer, and return the layer
number of the top-most permafrost layer, which is defined as any sediment that has been
frozen for two or more years.
"""
function frosty_layer!(layeri, sediment_column, year_frozen)

    # If the layer was not frozen before:
    if isnan(sediment_column[layeri].frostysince[1])

        # “Freeze” it by assining the current year, `year_frozen`, to its `frostysince` field.
        sediment_column[layeri].frostysince[1] = year_frozen

    # Otherwise, if the current layer has been frozen for two or more years:
    elseif year_frozen - sediment_column[layeri].frostysince[1] ≥ 2

        # Return the layer number of the current layer.
        return layeri

    end

    # Recursively call this function on the layer below the current one.
    frosty_layer!(layeri - 1, sediment_column, year_frozen)

end

## ================================================================================================= nothingmeanszero ##
"""
    nothingmeanszero([T=Float64,] value::Nothing)
    nothingmeanszero(value::Number)

Return zero for an input of `nothing`; otherwise, return the value unmodified. For an input
of `nothing`, the Type of the zero value can be optionally specified.
"""
@inline function nothingmeanszero(::Type{T}, ::Nothing) where {T}
    if @generated
        return :($T(0))
    else
        return T(0)
    end
end
@inline nothingmeanszero(nothingness::Nothing) = nothingmeanszero(Float64, nothingness)
@inline nothingmeanszero(anything::Number) = anything

## =========================================================================================================== nextup ##
"""
    nextup(x, y)

Return the next value in the positive direction from x that is wholly divisible by y; if x
is already wholly divisible by y, return x itself.
"""
nextup(x, y) = x % y |> m -> x + (m ≢ 0) * y - m
# Calculate remainder of `x / y`, then pass the value to the argument `m` of the anonymous function.

## =============================================================================================== quadrature_weights ##
"""
    quadrature_weights(func::Function, a, b, yshift, xshift, multiplier, qweightslength::Int;
                        forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false)
    quadrature_weights(func::Function, a, b, yshift, xshift, qweightslength::Int;
                        forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false)
    quadrature_weights(func::Function, qweightslength;
                       forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false)
    quadrature_weights(qweightslength; forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false)

Return a single-dimensional array of weights that can be used in summations. The magnitude
of the weights follow the shape of the function `func`, such that given sampling points
``xᵢ`` that are between `a` and `b` and are uniformly ``Δx`` apart, the iᵗʰ weight is

``
    wᵢ = [m × f(xᵢ + x′) + y′] × Δx ,
``

where ``m`` is the `multiplier`, ``f`` is the function `func`, ``x′`` is the `xshift`,
``y′`` is the `yshift`.

If a `multiplier` is not supplied, the sum of weights will be normalised to 1.

If, in addition, `a`, `b`, `yshift`, and `xshift` are not supplied, the interval is assumed
to be from 0 to 1 with no x- or y-shifts, and the sum of weights will be 1.

If, furthermore, no `func`tions are supplied, the weights will be uniform and will sum to 1.

Optionally, `forepadzeros` number of zeros can be placed at the start of the weights array,
whilst keeping the overall length, `qweightslength`, unchanged. Similar goes for padding
`aftpadzeros` number of zeros at the end of the array. The `verbose = true` mode prints
out the sum of the weights for verification.

# Example
```jldoctest
julia> quadrature_weights(10; verbose = true)
1.0
10-element Array{Float64,1}:
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002
 0.10000000000000002

 julia> quadrature_weights(tanh, 0, 1, 0, 0, 9; forepadzeros = 3, aftpadzeros = 2, verbose = true)
 1.0
 9-element Vector{Float64}:
  0.0
  0.0
  0.0
  0.0
  0.1929975980623166
  0.34983282313274516
  0.45716957880493825
  0.0
  0.0

julia> sum(quadrature_weights(sin, 0, pi/2, 0, 0, 1, 20000000)) ≈ 1
true
```
"""
function quadrature_weights(
                            func::Function, a, b, yshift, xshift, multiplier, qweightslength::Int;
                             forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false
                            )

    # Check if zero-padding is ≥ than the qweights's length
    @assert qweightslength > forepadzeros + aftpadzeros "there cannot be more padding zeros ($(forepadzeros + aftpadzeros)) than the length of the weights vector ($qweightslength).";

    # How long should the non-zero-padded part of the array be?
    alen = ifelse(forepadzeros + aftpadzeros ≡ 0, qweightslength, qweightslength - forepadzeros - aftpadzeros)

    # Get the sampling points of the function `func` between a and b, and the stride, Δx.
    xpoints = range(a, b; length = alen)
    Δx = (b - a) / (alen + forepadzeros)

    # Get the array of weights based on the function supplied by `func`, between a and b.
    qweights = [(yshift + multiplier * func(xi + xshift)) * Δx for xi in xpoints]

    # Either there are no zero-paddings, or add the zero padding.
    forepadzeros ≡ 0 || (qweights = vcat(zeros(forepadzeros), qweights))
    aftpadzeros ≡ 0 || (qweights = vcat(qweights, zeros(aftpadzeros)))

    # In verbose mode, print out the sum of the weights.
    verbose && println(sum(qweights))

    return qweights

end

# Gives weights in the shape of the function `func` between `a` and `b`, and that integrates to 1.
function quadrature_weights(
                            func::Function, a, b, yshift, xshift, qweightslength::Int;
                             forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false
                            )

    # Use quadrature_weights to get the unnormalised weights.
    qweights = quadrature_weights(func, a, b, yshift, xshift, 1, qweightslength;
                                  forepadzeros, aftpadzeros, verbose = false)

    # Get the sum (so that qweights doesn't mutate whilst we normalise it by element!).
    sumqweights = sum(qweights)

    # Normalise qweights by element.
    @. qweights /= sumqweights

    # In verbose mode, print out the sum of the weights.
    verbose && println(sum(qweights))

    return qweights

end

# Gives weights in the shape of the function `func` between 0 and 1, and that integrates to 1.
quadrature_weights(func::Function, qweightslength; forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false) =
    quadrature_weights(func, 0, 1, 0, 0, qweightslength; forepadzeros, aftpadzeros, verbose)

# Gives equal weights that sums to 1.
quadrature_weights(qweightslength; forepadzeros::Int = 0, aftpadzeros::Int = 0, verbose::Bool = false) =
    quadrature_weights(x -> 1, qweightslength; forepadzeros, aftpadzeros, verbose)

## ========================================================================================================= roulette ##
"""
    roulette(weights::Array{T}, sum_weights::T = sum(weights)) where {T <: Number}

Pick a random index (e.g. representing a random-walk direction) based on the array of
probability weights supplied. Returns an integer index (::Int). The algorithm used herein
is commonly called “Roulette Selection”, in which the “fitness” is given by the `weights`
array in the argument.

# Examples
```jldoctest
julia> using Random; Random.seed!(2468);

julia> roulette(rand(𝔽, 9))
9

julia> roulette(rand(𝔽, 9))
4
```
"""
function roulette(weights::Array{T}, sum_weights::T = sum(weights)) where {T <: Number}

    random_number::𝔽 = sum_weights * rand(T)

    @inbounds for i in eachindex(weights)

        # If `random_number` becomes less than the current weight, return the current index.
        random_number < weights[i] && return i

        random_number -= weights[i]

    end # for i in eachindex(weights)

    # error("The roulette picked nothing. Something might be broken! (Tip: Check the values of the argument.)\n",
    #       "weights: \t\t", weights,
    #       "\nsum(weights): \t", sum(weights),
    #       "\nrandom_number: \t", random_number)

    return -1 # If something is wrong, such as when some weights are NaNs, trigger an error.

end

## ======================================================================================================= underrelax ##
"""
    underrelax(new_value::Number, old_value::Number, coefficient::Number)
    underrelax(new_value::Array{<:Number}, old_value::Array{<:Number}, coefficient::Number)

Perform the arithmetic to slow down the change of a variable as in the underrelaxation
technique. Specifically, given a variable, ``w``, with an existing value (either in the
previous iteration or the previous time step), ``w′``, and the freshly calculated value,
``w″``, the underrelaxation is performed as follows:

``
        w = ζ w″ + (1 - ζ) w′ ,
``

where ``ζ`` (zeta) is the “update-fraction” coefficient ∈[0, 1], which can be understood
as the fraction of the freshly calculated value to include in the update (the rest being
contributed by the existing value).

# Examples
```jldoctest
julia> underrelax(3.0, 5.0, 0.4)
4.2
```
"""
@inline function underrelax(new_value::Number, old_value::Number, coefficient::Number)

    # Optional optimisation; to be decided by the compiler:
    if @generated

        return :(coefficient * new_value + (1 - coefficient) * old_value)

    else

        return (coefficient * new_value + (1 - coefficient) * old_value)

    end

end

@inline function underrelax(new_value::Array{<:Number}, old_value::Array{<:Number}, coefficient::Number)

    # Optional optimisation; to be decided by the compiler:
    if @generated

        return :(@. coefficient * new_value + (1 - coefficient) * old_value)

    else

        return (@. coefficient * new_value + (1 - coefficient) * old_value)

    end

end

## ====================================================================================================== wheretonext ##
# (There are two methods, differing only by the input types before `;`. Add more with the same name if necessary.)
"""
    wheretonext(
                weights_anonymous::Function,
                surf_elev_slice::AbstractArray{<:Number,2},
                water_depth_slice::AbstractArray{<:Number,2},
                flow_dir_local_x::Number, flow_dir_local_y::Number,
                gamma::Number,
                theta::Number,
                mask_slice::AbstractArray{Bool,2},
                exclusion_slice::AbstractArray{Bool,2},
                args...
                )
    wheretonext(
                weights_anonymous::Function,
                water_depth_slice::AbstractArray{<:Number,2},
                flow_dir_local_x::Number, flow_dir_local_y::Number,
                theta::Number,
                mask_slice::AbstractArray{Bool,2},
                exclusion_slice::AbstractArray{Bool,2},
                args...
                )

Pick the next step for the packet in question. This is a higher-order function (HOF) that
wraps around an anonymous core, the argument `weights_anonymous`, which can have several
variants depending on whether or not ice is present. This anonymous core computes weights,
for the purpose of performing a random walk, for each of the eight directions towards a
neighbouring cell. For efficiency, the current cell itself is included in the passed
`*_slice` arguments, but its probability will always be zero. Return an integer between 1
and 9 (excluding 5, which is the current location of the packet), with 1 being a step
moving by -1 in both x and y directions, and 9 being a step moving by +1 in both x and y
directions.

All `*_slice` arguments **must** have the size of (3, 3).

The version without the input argument `surf_elev_slice` is for guiding sediment packets.

# Example
```jldoctest
julia> γ = 0.05; Δᵢ = (√2, 1., √2, 1., 0., 1., √2, 1., √2); θ = 1;

julia> 𝐝ᵢ =
        ((-sqrt½, -sqrt½), (0., -1.), (sqrt½, -sqrt½), (-1., 0.), (0., 0.),
         (1., 0.), (-sqrt½, sqrt½), (0., 1.), (sqrt½, sqrt½));

julia> flowx = 0.2; flowy = 0.0;

julia> using Random; Random.seed!(1234); mask = rand(5, 10) .> rand(5, 10)
5×10 BitArray{2}:
 0  1  1  1  1  0  1  1  0  1
 1  0  0  1  1  0  1  1  1  0
 1  0  0  1  1  0  0  1  1  0
 0  0  1  0  1  1  0  1  0  0
 1  1  1  1  1  0  0  1  0  1

julia> Random.seed!(4321); surfe = rand(5, 10); wdepth = rand(5, 10)
5×10 Array{Float64,2}:
 0.501823  0.173653  0.549716   0.750671  …  0.537212  0.599836  0.208997   0.676071
 0.899959  0.58117   0.0426898  0.261337     0.919141  0.743117  0.586271   0.190799
 0.202069  0.652935  0.620252   0.833268     0.930161  0.403762  0.0915354  0.0544763
 0.96831   0.121834  0.82616    0.549755     0.905996  0.656007  0.656594   0.975171
 0.32969   0.630226  0.546046   0.973709     0.289867  0.276817  0.912042   0.799606

julia> wheretonext(
                   view(surfe, 2:4, 3:5),
                   view(wdepth, 2:4, 3:5),
                   flowx,
                   flowy,
                   γ,
                   θ,
                   view(mask, 2:4, 3:5),
                   falses(3, 3)
                   ) do neighbour, w_inertia, w_surface, theta, flow_dir_x, flow_dir_y,
       surf_elev, water_depth
           w_inertia[neighbour] = water_depth[neighbour] ^ theta *
                                  max(
                                      0.0,
                                      flow_dir_x * 𝐝ᵢ[neighbour][1] +
                                      flow_dir_y * 𝐝ᵢ[neighbour][2]
                                      ) / Δᵢ[neighbour]
           w_surface[neighbour] = max(0.0, surf_elev[5] - surf_elev[neighbour]) /
                                  Δᵢ[neighbour]
       end
6
```
The example above uses the [`view`](@ref) function, which is the preferred way to pass
array-type arguments into this function.
"""
function wheretonext(
                     weights_anonymous::Function,
                     surf_elev_slice::AbstractArray{<:Number,2},
                     water_depth_slice::AbstractArray{<:Number,2},
                     flow_dir_local_x::Number, flow_dir_local_y::Number,
                     gamma::Number,
                     theta::Number,
                     mask_slice::AbstractArray{Bool,2},
                     exclusion_slice::AbstractArray{Bool,2},
                     args...
                     )

    # Dimensions check: must be 3 by 3 for the code to work. (Can be disabled to speed up code after testing.)
    # @assert size(mask_slice) ≡ size(surf_elev_slice) ≡ size(water_depth_slice) ≡
    #         (3, 3) "“Slice” input variables must have size (3, 3)."

    w_inertia = zeros(9)                    # Element 5 should always be ignored, so must not be NaN or Inf.
    w_surface = zeros(9)                    # Element 5 should always be ignored, so must not be NaN or Inf.

    # Go over all the elements except the central cell, in no particular order.
    @inbounds @simd for neighbour in 1:9 # NB: Numbering is from top to bottom, by column from left to right.
        # The part after the boolean OR operator, ||, will be evaluated only if the part(s) before evaluates to false.
        mask_slice[neighbour] ||
        exclusion_slice[neighbour] ||
        neighbour ≡ 5 ||
        weights_anonymous(
                          neighbour,
                          w_inertia, w_surface,
                          theta,
                          flow_dir_local_x, flow_dir_local_y,
                          surf_elev_slice,
                          water_depth_slice,
                          args...
                          )
    end

    # Normalise the weights, but only if the sum of weights is non-zero (to within full precision).
    sum(w_inertia) < 𝟘⁺ || normalize!(w_inertia)
    sum(w_surface) < 𝟘⁺ || normalize!(w_surface)

    # Partition the contribution of each kind of weights, according to the partition coefficient, γ.
    w = underrelax(w_surface, w_inertia, gamma)

    # If the weights are not all zeros within full floating-point precision:
    if sum(w) > 𝟘⁻

        # Perform a weighted random walk.
        return roulette(w)

    else # Otherwise:

        # Inner function whose sole purpose is to rule on whether or not a direction is forbidden,
        # based on `mask_slice` and `exclusion_slice`.
        # (Passed to a local scope via `let` for optimal performance.)
        isallowed = let mask_slice = mask_slice, exclusion_slice = exclusion_slice
            x -> ~mask_slice[x] && ~exclusion_slice[x] && x ≢ 5 # self (i.e., not moving) is disallowed
        end

        # Randomly choose a direction that is not blocked via the `isallowed` inner function.
        return rand_helper_(filter!(isallowed, Vector(1:9)), exclusion_slice)

    end

    #= A note on the return lines above:
    The intention is to avoid picking a direction that will lead to a prohibited zone (i.e., a boundary or a dry cell).
    For the weighted random-walk case using `roulette`, the assumption is that the `mask_slice` and `exclusion_slice`
    will lead to zero weights, inhibiting the direction(s) from being picked. However, the plain random-walk case is
    still free to select "prohibited" directions. Therefore, the argument of the rand() function call is structured so
    that these "prohibited" choices are removed: `filter!(...)` gives a linear (i.e., one-dimensional array) of
    indices not equal to 5 (i.e., remaining stationary) and where the directional-exclusions are false. This filtered
    vector is then passed to `rand` via `rand_helper_`; if the vector is empty, which is very rare but still possible,
    `rand_helper_` will keep only the `exclusion_slice` (most likely wall-boundary cells in practice) directions
    forbidden, but allow the `mask_slice` directions (most likely the dry cells in practice). Since the latter, very
    rare case most likely arise due to packet-path loop handling via `loop_disposal`, which is itself rare and do not
    contribute to backwater-shaping, it is an acceptable compromise.
    =#

end

function wheretonext(
                     weights_anonymous::Function,
                     water_depth_slice::AbstractArray{<:Number,2},
                     flow_dir_local_x::Number, flow_dir_local_y::Number,
                     theta::Number,
                     mask_slice::AbstractArray{Bool,2},
                     exclusion_slice::AbstractArray{Bool,2},
                     args...
                     )

    # Dimensions check: must be 3 by 3 for the code to work. (Can be disabled to speed up code after testing.)
    # @assert size(mask_slice) ≡ size(water_depth_slice) ≡ (3, 3) "“Slice” input variables must have size (3, 3)."

    w = zeros(9) # Element 5 should always be ignored, so must not be NaN or Inf.

    # Go over all the elements except the central cell, in no particular order.
    @inbounds @simd for neighbour in 1:9 # NB: Numbering from top to bottom, column by column from left to right.
        # The part after the boolean OR operator, ||, will be evaluated only if the part(s) before evaluates to false.
        mask_slice[neighbour] ||
        exclusion_slice[neighbour] ||
        neighbour ≡ 5 ||
        weights_anonymous(
                          neighbour,
                          w,
                          theta,
                          flow_dir_local_x, flow_dir_local_y,
                          water_depth_slice,
                          args...
                          )
    end

    # If the weights are not all zeros within full floating-point precision:
    if sum(w) > 𝟘⁻

        # Perform a weighted random walk.
        return roulette(w)
        #= A note on `roulette`: It does not require the weights to be normalised. =#

    else # Otherwise:

        # Inner function whose sole purpose is to rule on whether or not a direction is forbidden,
        # based on `mask_slice` and `exclusion_slice`.
        # (Passed to a local scope via `let` for optimal performance.)
        isallowed = let mask_slice = mask_slice, exclusion_slice = exclusion_slice
            x -> ~mask_slice[x] && ~exclusion_slice[x] && x ≢ 5 # self (i.e., not moving) is disallowed
        end

        # Randomly choose a direction that is not blocked via the `isallowed` inner function.
        return rand_helper_(filter!(isallowed, Vector(1:9)), exclusion_slice)

    end

    #= A note on the return lines above:
    The intention is to avoid picking a direction that will lead to a prohibited zone (i.e., a boundary or a dry cell).
    For the weighted random-walk case using `roulette`, the assumption is that the `mask_slice` and `exclusion_slice`
    will lead to zero weights, inhibiting the direction(s) from being picked. However, the plain random-walk case is
    still free to select "prohibited" directions. Therefore, the argument of the rand() function call is structured so
    that these "prohibited" choices are removed: `filter!(...)` gives a linear (i.e., one-dimensional array) of
    indices not equal to 5 (i.e., remaining stationary) and where the directional-exclusions are false. This filtered
    vector is then passed to `rand` via `rand_helper_`; if the vector is empty, which is very rare but still possible,
    `rand_helper_` will keep only the `exclusion_slice` (most likely wall-boundary cells in practice) directions
    forbidden, but allow the `mask_slice` directions (most likely the dry cells in practice). Since the latter, very
    rare case most likely arise due to packet-path loop handling via `loop_disposal`, which is itself rare and do not
    contribute to backwater-shaping, it is an acceptable compromise.
    =#

end

## --------------------------------------------------------------------------------------------- Helper: rand_helper_ ##
"""
    rand_helper_(choices, exclusion_slice)

Helper function to handle the random walk when the combination of `mask_slice` and
`exclusion_slice` (in [`wheretonext`](@ref)) forbid all possible directions (i.e., when the
`choices` array is empty). “Compromise” by forbidding directions by `exclusion_slice` only.
Return directly `rand(choices)` otherwise.
"""
@inline function rand_helper_(choices, exclusion_slice)

    # If the array `choises` is not empty:
    if ~isempty(choices)

        # Simply return a random choice from `choices`.
        return rand(choices)

    else # Otherwise, make compromise:

        # Inner function whose sole purpose is to rule on whether or a direction is forbidden,
        # based only on `exclusion_slice`.
        # (Passed to a local scope via `let` for optimal performance.)
        isallowed = let exclusion_slice = exclusion_slice
            x -> ~exclusion_slice[x] && x ≢ 5
        end

        # Randomly choose a direction that is not blocked via the `isallowed` inner function.
        return rand(filter!(isallowed, Vector(1:9)))

    end # if ~isempty(choices)

end

## ------------------------------------------------------------------------------------------ Alias: nextstep_icefree ##
"""
    nextstep_icefree(surf_elev, water_depth, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion)
    nextstep_icefree(water_depth, flow_dir_x, flow_dir_y, theta, mask, exclusion)

Alias of the variant of [`wheretonext`](@ref) that does not consider ice thickness.

---
Requires the following global constants:
  * `Δᵢ`    Cellular distance vector between each of the adjacent cells.
  * `𝐝ᵢ`    Cellular direction (unit) vector from the central cell towards each neighbour.
"""
nextstep_icefree(surf_elev, water_depth, nothing, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion) =
wheretonext(surf_elev, water_depth, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion) do neighbour,
    w_inertia, w_surface, theta, flow_dir_x, flow_dir_y, surf_elev, water_depth

    # Weights based on inertia, determined by the flow direction downstream.
    @inbounds w_inertia_H2O_(neighbour, w_inertia, water_depth, theta, flow_dir_x, flow_dir_y)

    # Weights based on gravity, determined by surface elevation.
    @inbounds w_surface_(neighbour, w_surface, surf_elev)

end

nextstep_icefree(water_depth, nothing, flow_dir_x, flow_dir_y, theta, mask, exclusion) =
wheretonext(water_depth, flow_dir_x, flow_dir_y, theta, mask, exclusion) do neighbour,
    w, theta, flow_dir_x, flow_dir_y, water_depth

    # Weights based on inertia, determined by the flow direction downstream.
    @inbounds w_inertia_H2O_(neighbour, w, water_depth, theta, flow_dir_x, flow_dir_y)

end

## ------------------------------------------------------------------------------------------ Alias: nextstep_withice ##
"""
    nextstep_withice(surf_elev, water_depth, ice_thickness, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion)
    nextstep_withice(water_depth, ice_thickness, flow_dir_x, flow_dir_y, theta, mask, exclusion)

Alias of the variant of [`wheretonext`](@ref) that takes ice thickness into account.

---
Requires the following global constants:
  * `Δᵢ`    Cellular distance vector between each of the adjacent cells.
  * `𝐝ᵢ`    Cellular direction (unit) vector from the central cell towards each neighbour.
"""
nextstep_withice(surf_elev, water_depth, ice_thickness, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion) =
wheretonext(
            surf_elev, water_depth, flow_dir_x, flow_dir_y, gamma, theta, mask, exclusion,
            ice_thickness::typeof(water_depth)
            ) do neighbour, w_inertia, w_surface, theta, flow_dir_x, flow_dir_y, surf_elev, water_depth, ice_thickness

    # Weights based on inertia, determined by the flow direction downstream.
    @inbounds w_inertia_ICE_(neighbour, w_inertia, water_depth, ice_thickness, theta, flow_dir_x, flow_dir_y)

    # Weights based on gravity, determined by surface elevation.
    @inbounds w_surface_(neighbour, w_surface, surf_elev)

end

nextstep_withice(water_depth, ice_thickness, flow_dir_x, flow_dir_y, theta, mask, exclusion) =
wheretonext(
            water_depth, flow_dir_x, flow_dir_y, theta, mask, exclusion,
            ice_thickness::typeof(water_depth)
            ) do neighbour, w, theta, flow_dir_x, flow_dir_y, water_depth, ice_thickness

    # Weights based on inertia, determined by the flow direction downstream.
    @inbounds w_inertia_ICE_(neighbour, w, water_depth, ice_thickness, theta, flow_dir_x, flow_dir_y)

end

## ------------------------------------------------------------------------------------------- Helper: w_inertia_H2O_ ##
"""
    w_inertia_H2O_(neighbour, w_inertia, water_depth, theta, flow_dir_x, flow_dir_y)

Helper function to calculate directional weights based on inertia towards the `neighbour`,
without the presence of ice.
"""
@inline function w_inertia_H2O_(neighbour, w_inertia, water_depth, theta, flow_dir_x, flow_dir_y)

    w_inertia[neighbour] = water_depth[neighbour] ^ theta *
                           max(0.0, flow_dir_x * 𝐝ᵢ[neighbour][1] + flow_dir_y * 𝐝ᵢ[neighbour][2]) /
                           Δᵢ[neighbour]

end

## ------------------------------------------------------------------------------------------- Helper: w_inertia_ICE_ ##
"""
    w_inertia_ICE_(neighbour, w_inertia, water_depth, ice_thickness, theta, flow_dir_x, flow_dir_y)

Helper function to calculate directional weights based on inertia towards the `neighbour`,
with the presence of ice.
"""
@inline function w_inertia_ICE_(neighbour, w_inertia, water_depth, ice_thickness, theta, flow_dir_x, flow_dir_y)

    w_inertia[neighbour] = (
                            (water_depth[neighbour] - ice_thickness[neighbour]) *   # These two lines
                            (1 - ice_thickness[neighbour] / water_depth[neighbour]) # give ``h_{eff}``
                           ) ^ theta *
                           max(0.0, flow_dir_x * 𝐝ᵢ[neighbour][1] + flow_dir_y * 𝐝ᵢ[neighbour][2]) /
                           Δᵢ[neighbour]

end

## ----------------------------------------------------------------------------------------------- Helper: w_surface_ ##
"""
    w_surface_(neighbour, w_surface, surf_elev)

Helper function to calculate directional weights based on gravitational differences towards
the `neighbour`.
"""
@inline function w_surface_(neighbour, w_surface, surf_elev)

    w_surface[neighbour] = max(0.0, surf_elev[5] - surf_elev[neighbour]) / Δᵢ[neighbour]

end
