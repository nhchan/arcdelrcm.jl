#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: types.jl

Declare all the Composite Types and the augmentations to some of the methods in Julia
Base to enable coverage of these types.
=#

#------- State- and Flag- Variable Types Declarations -------#
"""
    Waters{T,V}

Composite of all gridded state-variables of water flow. The fields are, in order:
`surf_elev`, `bed_elev`, `ice_thickness`, `water_depth`, `dry_flag`, `flow_speed_x`,
`flow_speed_y`, `flow_speed`, `unit_dischg_x`, `unit_dischg_y`, `unit_dischg`,
`unit_dischg_x_nxt`, `unit_dischg_y_nxt`, and `unit_dischg_nxt`. They are all of the type
`Array{T,2}`, except `dry_flag` (`BitArray`) and `ice_thickness` (`Array{V,2}`).

The parametric type, `T`, is intended to be some floating-point type. `ice_thickness` has
its own parametric type, `V`, to allow for the possibility that it takes on the [`Nothing`]
(@ref) type rather than a numeric type.
"""
Base.@kwdef struct Waters{T,V}

    surf_elev::Array{T,2}            # Water-surface elevation, H, in metres.
    bed_elev::Array{T,2}             # Bed elevation, η, in metres.
    ice_thickness::Array{V,2}        # No ice thickness; dummy value. Needed to trigger ice-free variants of functions.
    water_depth::Array{T,2}          # Water depth, h, in metres; inclusive of ice thickness.

    dry_flag::BitArray{2}   # Cells with water depth smaller than h_dry is flagged as true.

    # Flow velocity, 𝐮, in metres per second.
    flow_speed_x::Array{T,2}    # x component
    flow_speed_y::Array{T,2}    # y component
    flow_speed::Array{T,2}      # magnitude

    # Unit water discharge, 𝐪 (flow rate per unit width of cross section, like a slit), in metres squared per second.
    # NB: The direction of 𝐪 is also used as flow direction, denoted 𝐅 in Liang et al. (2015).
    unit_dischg_x::Array{T,2}   # x component
    unit_dischg_y::Array{T,2}   # y component
    unit_dischg::Array{T,2}     # magnitude

    # Unit water discharge, 𝐪 (flow rate per unit width of cross section, like a slit), in metres squared per second.
    unit_dischg_x_nxt::Array{T,2}   # x component
    unit_dischg_y_nxt::Array{T,2}   # y component
    unit_dischg_nxt::Array{T,2}     # magnitude
    #= Note:
    The “_next” variables are places to hold the values from the latest round of calculations; this enables the use of
    previous values (either from the previous time step or iteration) to damp/underrelax the change.
    =#

end

"""
    WaterTracking{K,T}

Composite of all place-holder variables with the purpose of tracking water-packet paths and
whilst building surface elevations. The fields are, in order:
`path_history::Array{CartesianIndex{2},1}`, `step_history::Array{K,1}`,
`visit_count::Array{K,2}`, and `cumul_surf_elev::Array{T,2}`.

The parametric type, `K`, is intended to be an integer type, whilst type `T` is intended to
be a floating-point type. `path_history` is a one-dimensional array of two-dimensional
CartesianIndex values.
"""
Base.@kwdef struct WaterTracking{K,T}

    # Path history of each packet, in terms of Cartesian coordinates
    path_history::Array{CartesianIndex{2},1}

    #=
    Step-direction history of each packet, in terms of the linear index of a 3 by 3 matrix, with the central cell being
    the origin. For example, if the current location is (2, 2), and the next step is (1, 1), the value 1 is assigned.
    Similarly, if the next step is (2, 3), the value 8 is assigned. If there are no movements, the value is 5.
    =#
    step_history::Array{K,1}

    # Array to log the number of visits by the water packets over the current time step.
    visit_count::Array{K,2}

    # Array to log the non-averaged, cumulated water surface elevation resulting from all water packets in the current
    # time step.
    cumul_surf_elev::Array{T,2}

end

"""
    SedimentTransport{T}

Composite of all gridded fields that track sediment-transport states and volumes (in the
current time step). The fields are, in order: `erodibility`, `sand_flux`,
`sand_deposit_volume`, and `mud_deposit_volume`. They are all of the type `Array{T,2}`.

The parametric type, `T`, is intended to be some floating-point type.
"""
Base.@kwdef struct SedimentTransport{T}

    # Erodibility scaling factor, E, as fraction of 1, which is full erodibility when no permafrost is present.
    erodibility::Array{T,2}

    # Sand flux (volume per unit time per unit width of cross section), in metres squared per second.
    sand_flux::Array{T,2} # Determined by all sediment parcels in each time step, and zeroed after each time step

    # Volume of sediment (sand or mud) deposited in a unit of time.
    sand_deposit_volume::Array{T,2} # Determined by all sediment parcels in each time step; zeroed after each time step
    mud_deposit_volume::Array{T,2}  # Determined by all sediment parcels in each time step; zeroed after each time step

end

"""
    Boundaries

Composite of BitArrays that are on-off flags of simulation-domain boundaries. The fields
are `wall_flag` and `ocean_flag`, both of the type `BitArray{2}`.
"""
Base.@kwdef struct Boundaries

    wall_flag::BitArray{2}      # Inlet-wall boundary
    ocean_flag::BitArray{2}     # Ocean-limit boundary

end

#------- Composite Type of All Gridded-Variables' Composite Types -------#
# For easy passing of all (or nearly all) variable fields.
"""
    AllGriddedVars{T,V,K,S}

Composite of all the gridded variable types. The fields are, in order: `waters::Waters{T,V}`
`water_tracking::WaterTracking{K,T}`, `seabed::Array{S,2}`,
`sediment_transport::SedimentTransport{T}`, and `boundaries::Boundaries`.

The parametric type `T` is intended to be some floating-point type, `K` is intended to be
some integer type, `S` is intended to be the `SedimentColumn{T}` type, and `V` is intended
to be either the [`Nothing`](@ref) type or some floating-point type.
"""
Base.@kwdef struct AllGriddedVars{T,V,K,S}

    waters::Waters{T,V}
    water_tracking::WaterTracking{K,T}
    seabed::Array{S,2} # Intended to be of type Array{SedimentColumn{T},2}, where SedimentColumn{T} is defined below
    sediment_transport::SedimentTransport{T}
    boundaries::Boundaries

end

#------- Sediment Storage Types Declarations -------#                                                            [- 9 -]
# Element of deposited sediment.
"""
    Sediment{T}

Deposited-sediment type. Contains fields (of parametric type, T) that are properties
associated with this element of deposited sediment: sand fraction `sandfrac`, time of
deposition `timeofdep`, and time since frozen `frostysince`.
"""
struct Sediment{T}

    sandfrac::T             # Sand fraction
    timeofdep::T            # Time when deposited, in years since the start of simulation
    frostysince::Vector{T}  # Time when it became part of the frozen layer, in model years
    # Note: `frostysince` is a vector so that its value remains mutable inside an immutable struct.

    # Inner constructor method to ensure that `frostysince` contains only 1 element.
    Sediment{T}(sandfrac, timeofdep, frostysince) where T = length(frostysince) ≢ 1 ?
        error("the field `frostysince` of the Sediment type must have a length of 1.") :
        new{T}(sandfrac, timeofdep, frostysince)

end
# Some outer constructor(s) for the type Sediment{T}
Sediment(sandfrac::T, timeofdep::T, frostysince::Vector{T}) where T =
    Sediment{T}(sandfrac, timeofdep, frostysince)
Sediment(sandfrac::T, timeofdep::T) where T = Sediment{T}(sandfrac, timeofdep, T.([NaN]))

# Column of deposited sediments.
"""
    SedimentColumn{T}

Sediment-column type. Contains fields (of parametric type, T) that are properties
associated with this particular column: the elevations of the sea bottom and the underlying
bedrock `z[1:2]`, the thickness of permafrost `permaz[1]` and the depth of the active layer
`permaz[2]`, and the stratigraphic (vector) column of sediments `strata`.
"""
struct SedimentColumn{T}

    # NB: The Vector construct of any scalar attributes is so that the value is mutable,
    #     even though the size is not.

    # Two-element vector:
    #    [1] Bed-rock elevation; i.e., the bottom of the sediment column, in metres
    #    [2] Bed-surface elevation; i.e., the top of the sediment column, in metres
    z::Vector{T}

    # Two-element vector:
    #    [1] Thickness of permafrost layers, in metres from the bottom (z[1])
    #    [2] Depth of thaw or active layer, in metres from the top (z[2])
    permaz::Vector{T}

    # Vector of the deposited elements, with the bed surface at the end of the vector
    strata::Vector{Sediment{T}}

    # Inner constructor method; used to ensure that:
    # (i)  `z` and `permaz` each has only 2 elements and
    # (ii) that `z[1]` and `z[2]` are in the right order.
    function SedimentColumn{T}(z, permaz, strata) where T
        if ~(length(z) ≡ length(permaz) ≡ 2)
            error("the fields `z` and `permaz` of the SedimentColumn type must each have a length of 2.")
        elseif z[2] < z[1]
            error("the top of the sediment column, `z[2]`, should not be set to be below its bottom, `z[1]`.")
        else
            return new{T}(z, permaz, strata)
        end
    end

end
# Some outer constructor(s) for the type SedimentColumn{T}
SedimentColumn(z::Vector{T}, permaz::Vector{T}, strata::Vector{Sediment{T}}) where T =
    SedimentColumn{T}(z, permaz, strata)
SedimentColumn(z::Vector{T}, permaz::Vector{T}) where T = SedimentColumn{T}(z, permaz, empty([], Sediment{T}))
SedimentColumn(z1::T) where T = SedimentColumn{T}([z1, z1], T.([0, 0]), empty([], Sediment{T}))

#------- Some Augmentations to the Julia Base to Better Handle the Custom Sediment Types Declared Above. -------#

# Enabling the use of length(SC::SedimentColumn) to give the length of the SC.strata::Vector{Sediment{T}} directly.
Base.length(SC::SedimentColumn) = length(SC.strata)

# Enabling the use of eachindex(SC::SedimentColumn) to generate the most efficient iterator over each element of
# SC.strata::Vector{Sediment{T}}
Base.eachindex(SC::SedimentColumn) = eachindex(SC.strata)

#=
Enabling the use of indexing to access the elements of `strata` directly. For example, let's say:
    typeof(seabed[3]) ≡ SedimentColumn{Float64}
We can then execute operations such as:
```
julia> seabed[3][1] # Access the 1st element of seabed[3].strata
Sediment{Float64}(0.4, 50000.0, 1.0)

julia> seabed[3][:] # List all elements of seabed[3].strata
6-element Array{Sediment{Float64},1}:
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0

julia> seabed[3][1:3] # List elements 1 through 3 of seabed[3].strata
3-element Array{Sediment{Float64},1}:
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0

julia> seabed[3][5] = Sediment{Float64}(0.3, 25000, 4) # Replace the 5th element of seabed[3].strata
Sediment{Float64}(0.3, 25000.0, 4.0)
```
=#
Base.getindex(SC::SedimentColumn{T}, inds::Vararg{Int,N}) where {T,N} = SC.strata[inds...]
Base.getindex(SC::SedimentColumn{T}, colon::Colon) where {T} = SC.strata[colon]
Base.getindex(SC::SedimentColumn{T}, unit_range::UnitRange{N}) where {T,N} = SC.strata[unit_range::UnitRange{Int64}]
Base.setindex!(SC::SedimentColumn{T}, val, inds::Vararg{Int,N}) where {T,N} = SC.strata[inds...] = val

#=
Augment the Base.show method to display Sediment{T} type as:
 sand fraction: 0.4 , time deposited: 25000.0 , frozen since: 2.0
such that an array of Sediment{T} can be displayed as, e.g.:
6-element Array{Sediment{Float64},1}:
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.4 , time deposited: 50000.0 , frozen since: 1.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0
 sand fraction: 0.3 , time deposited: 25000.0 , frozen since: 4.0
=#
Base.show(io::IO, S::Sediment) = print(
                                       io,
                                       "sand fraction: ", S.sandfrac,
                                       " , time deposited: ", S.timeofdep,
                                       " , frozen since: ", S.frostysince
                                       )

#=
Augment the Base.show method to display the Sediment{T} type as:
Sediment{Float64} deposit grid cell:
 sand fraction: 0.4 , time deposited: 25000.0 , frozen since: 2.0
=#
Base.show(io::IO, ::MIME"text/plain", S::Sediment{T}) where {T} = print(io, "Sediment{$T} deposit grid cell:\n ", S)

# Augment the Base.copy! method to copy SedimentColumn{T} type, in-place, to another SedimentColumn{T} type.
Base.copy!(SC1::SedimentColumn{T}, SC2::SedimentColumn{T}) where {T} = begin
    copy!(SC1.z, SC2.z); copy!(SC1.permaz, SC2.permaz); copy!(SC1.strata, SC2.strata);
end

# Augment the Base.copy method to make a copy of the SedimentColumn{T} type.
Base.copy(SC::SedimentColumn{T}) where {T} = begin
    dest = SedimentColumn(T(0))
    copy!(dest, SC)
    dest
end

#=
Augment the Base.isequal method to compare the Sediment{T} type.
=#
Base.isequal(x::Sediment{T}, y::Sediment{T}) where {T} = all(
                                                             [
                                                              isequal(x.sandfrac, y.sandfrac);
                                                              isequal(x.timeofdep, y.timeofdep);
                                                              isequal(x.frostysince, y.frostysince)
                                                              ]
                                                             )

#------- Composite Type Containing All Simulation Parameters -------#
"""
    Params{K,T,S}

Composite Type containing all simulation parameters (i.e., settings) for ArcDelRCM.jl.

The keyword fields map one-to-one to the variables defined in "Run.jl" and in the
[`setparams`](@ref) function in the "src/init.jl" source file, and have identical names.
"""
Base.@kwdef struct Params{K,T,S}

    Nx::K
    Ny::K
    δc::T
    δc²::T
    δz::T
    N₀::K
    Nwall::K
    Ny½::K
    inletsidewalls::Tuple{K,K}
    inletsides::Tuple{K,K}

    maxstep::K

    Δt::T
    lasttime::K
    nw::K
    ns::K
    lastiterw::K

    iceextent::T
    hᵢmax::T
    𝐸::T
    å::T

    sedfraction::T
    f_sand::T
    f_mud::T

    S₀::S
    h₀::S
    hB::T
    hdry::T
    hΔ::T
    u₀::S
    uₘₐₓ::T
    u_muddep::T
    u_mudero::T
    u_sandero::T
    uΔ::T
    V₀::T

    Qw₀::S
    Qs₀::S
    Qwp::S
    qw₀::S
    q_sand₀::S
    ΔVs::S
    Vsp::S

    daysperΔt::T
    yearsperΔt::T

    lastbeddiffpass::K
    beddiffusecoef::T

    α::T
    β::T
    γ::T
    ϵ::T
    θ::NamedTuple{(:water, :sand, :mud),Tuple{T,T,T}}
    ϖ::T
    ω::NamedTuple{(:iterfirst, :itersubsq),Tuple{T,T}}

    backwater_method!::Function
    manningscoef²::T
    𝐶𝔣::T

end # struct Params

"""
    DomainParams{K,T}

Auxiliary composite type containing only the defining parameters of a simulation domain:
`Nx`, `Ny`, `δc`, `δz`, `N₀`, and `Nwall`.

!!! note
    This exists only to facilitate the save-state functionality of ArcDelRCM. The full
    [`Params`](@ref) type contains a field for the backwater method, which is of the
    Function type, making saving to JLD2 (or HDF5) somewhat tricky and error-prone. Since
    the purpose here is to ensure matching domain dimensions in cases of inheriting or
    continuing simulations, this auxiliary type is used for the saved states instead.
"""
struct DomainParams{K,T}
    Nx::K
    Ny::K
    δc::T
    δz::T
    N₀::K
    Nwall::K
end # struct domainparams
