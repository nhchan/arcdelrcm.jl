#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: init.jl

Allocate all the variables required for an ArcDelRCM simulation, and initialise them as
necessary through the `populate_initial_state!` function. Also included are functions to
reimpose boundary conditions at the inlet (`reimpose_boundary_conds_*!`).

```
========================================
|           TABLE of CONTENTS          |
|          (look-up along the          |
|            right-hand edge           |
|             of this file)            |
----------------------------------------
## ======================== setparams ##
## ====================== preallocate ##
## ========== populate_initial_state! ##
## ========= print_inheritance_domain ##
## ========== inherit_previous_state! ##
## ===================== matchstructs ##
## == reimpose_boundary_conds_dischg! ##
## === reimpose_boundary_conds_depth! ##
## ================== backwater_check ##
## =================== timestep_check ##
## =============== scaleheightmeasure ##
## ==================== seabedtilt_*! ##
## ==== Helper: seabedtilt_sculpter_! ##
## ======================== island_*! ##
## ========= Helper: island_builder_! ##
## ====================== printstruct ##
## =================== recordsettings ##
## ================== init_outputfile ##

=#

using FileIO

include("constants.jl")  # Contains the declarations of simulation parameters as global constants and of custom types

## ======================================================================================================== setparams ##
"""
    setparams(arctic_or_not::Bool;
               Nx, Ny, δc, δz, N₀, Nwall, Δt,
               lasttime, nw, ns, lastiterw,
               iceextent, hᵢmax, 𝐸, å,
               sedfraction, f_sand, f_mud,
               Sₘᵢₙ, hₘᵢₙ, hB, hdry, hΔ, uₘᵢₙ,
               uₘₐₓ, u_muddep_coef, u_mudero_coef, u_sandero_coef, uΔ,
               Qw₀, Qs₀,
               α, β, γ, ϵ, θ, ϖ, ω,
               backwater_method,
               manningscoef²,
               𝐶𝔣,
               ignore_timestep_check = false
              )

Return all parameters of an ArcDelRCM simulation as a struct (`::Params`), which contains
some additional values derived from the input keyword arguments. The `arctic_or_not::Bool`
argument is used to trigger the removal of any unnecessary parameter values.

The optional argument `ignore_timestep_check` is passed directly to the
[`timestep_check`](@ref) function. See its documentation for explanations.
"""
function setparams(arctic_or_not::Bool;
                    Nx, Ny, δc, δz, N₀, Nwall, Δt,
                    lasttime, nw, ns, lastiterw,
                    iceextent, hᵢmax, 𝐸, å,
                    sedfraction, f_sand, f_mud,
                    Sₘᵢₙ, hₘᵢₙ, hB, hdry, hΔ,
                    uₘᵢₙ, uₘₐₓ, u_muddep_coef, u_mudero_coef, u_sandero_coef, uΔ,
                    Qw₀, Qs₀,
                    α, β, γ, ϵ, θ, ϖ, ω,
                    backwater_method,
                    manningscoef²,
                    𝐶𝔣,
                    ignore_timestep_check = false
                    )

    # Use the symbol in `backwater_method!` to get the actual function with the same name from the current module;
    # replace the symbol with the actual reference to the function.
    backwater_method!::Function = getfield(@__MODULE__, backwater_method)

    # Void non-applicable parameters.
    backwater_method! ≡ backwater_gvfmanning! || (manningscoef² = 𝔽(NaN))
    backwater_method! ≡ backwater_gvfflowrcm! || (𝐶𝔣 = 𝔽(NaN))
    if ~arctic_or_not # non-Arctic?
        iceextent = hᵢmax = 𝐸 = å = 𝔽(NaN)
    end

    #———— Secondary definitions of parameters––those that depend on the user-defined params. ————#

    # Area of square cells [m²].
    δc²::𝔽 = δc * δc

    # The index of the cells forming the central line of the domain, along the centre of the inlet,
    # rounded down to the nearest discrete value.
    Ny½::Int = Ny ÷ 2 # Unicode \1/2

    # Two sides of the inlet.
    inletsidewalls::Tuple{Int,Int} = ((Ny½ - N₀ ÷ 2) - 1, (Ny½ + N₀ ÷ 2) + 1) # Exclusive boundary: wall
    inletsides::Tuple{Int,Int}     = ((Ny½ - N₀ ÷ 2),     (Ny½ + N₀ ÷ 2)    ) # Inclusive boundary: channel edges

    # Maximum number of steps a packet can take. Default: The domain perimeter, 2 × (Nx + Ny).
    maxstep::Int = <<(Nx + Ny, 1) # The bit shift operation <<(𝓍, n) ≡ 𝓍 × 2ⁿ.

    # TODO: Add mathematical explanation of the Manning's formula scaling as a comment.

    # If the input discharge, Qw₀, is a time series, calculate the corresponding time series of the inlet
    # flow depth, h₀, according to the scaling based on Manning's formula:
    #   h₀ = (Qw₀ ./ Qwₘᵢₙ).^(3//5) .* hₘᵢₙ, where hₘᵢₙ is the "baseline h₀" and Qwₘᵢₙ is the minimum of Qw₀.
    # Otherwise, h₀ will simply be a single-element array, [hₘᵢₙ].
    h₀::Array{𝔽,1} = (Qw₀ ./ min(Qw₀...)).^(3 // 5) .* hₘᵢₙ

    # If the input discharge, Qw₀, and thus the inlet flow depth, h₀, are time series, calculate the corresponding
    # inlet flow speed, u₀, according to the scaling based on Manning's formula:
    #   u₀ = (h₀ ./ hₘᵢₙ).^(2//3) * uₘᵢₙ, where uₘᵢₙ is the "baseline u₀" and hₘᵢₙ is the minimum of h₀.
    # Otherwise, u₀ will simply be a single-element array, [uₘᵢₙ].
    u₀::Array{𝔽,1} = (h₀ ./ hₘᵢₙ).^(2 // 3) .* uₘᵢₙ

    # Average inlet flow speed [m/s]; this is equal to uₘᵢₙ if u₀ is constant.
    uₘₑₐₙ::𝔽 = sum(u₀) / length(u₀)

    # Flow speedlimit, below which mud is deposited [m/s]. Finer grain size ⟹ Slower flow limit for deposition.
    u_muddep::𝔽 = u_muddep_coef * uₘₑₐₙ

    # Flow speedlimit, above which mud is eroded [m/s].
    u_mudero::𝔽 = u_mudero_coef * uₘₑₐₙ

    # Flow speedlimit, above which sand is eroded [m/s].
    u_sandero::𝔽 = u_sandero_coef * uₘₑₐₙ

    # If the input discharge, Qw₀, and thus the inlet flow depth, h₀, are time series, calculate the corresponding
    # reference water-surface slope, S₀ (used as the backwater slope), using a simple scaling that is a tiny-angle
    # approximation:
    #   S₀ = (h₀ ./ Sᵦ), where Sᵦ ≡ hₘᵢₙ / Sₘᵢₙ is the "baseline reach" used to calculate S₀ at minimum Qw₀.
    # Otherwise, S₀ will simply be a single-element array, [Sₘᵢₙ].
    S₀::Array{𝔽,1} = h₀ .* inv(hₘᵢₙ / Sₘᵢₙ)

    # Reference volume, which is the volume to fill an inlet cell to the characteristic flow depth, h₀.
    V₀::𝔽 = max(h₀...) * δc^2
    # !!!! Modify with Caution: literal definition `V₀ ≡ h₀ * δc^2` !!!!

    # TODO: Check that Qw₀ and Qs₀ are the same lengths.

    # Water discharge (volume velocity) per packet [m³/s], denoted as “Qp_water” in Liang et al. (2015, Part I).
    Qwp::Array{𝔽,1} = Qw₀ / nw

    # Unit water discharge at inlet [m²/s].
    qw₀::Array{𝔽,1} = Qw₀ * inv(N₀ * δc) # Unicode \_0

    # Unit sand flux at inlet [m²/s].
    q_sand₀::Array{𝔽,1} = Qs₀ * inv(N₀ * δc) * f_sand # Unicode \_0

    # Volume of sediment entering the simulation domain per time step [m³],
    # and, if applicable, the adjusted time-step size (otherwise, the same Δt is returned)
    ΔVs::Array{𝔽,1}, Δt = timestep_check(δc², hB, Δt, Qs₀; ignore_timestep_check) # Unicode \Delta

    # Volume of each sediment packet [m³].
    Vsp::Array{𝔽,1} = ΔVs / ns

    # Days per time step [days].
    daysperΔt::𝔽 = Δt / sperday # Unicode \Delta

    # Years per time step [yrs].
    yearsperΔt::𝔽 = Δt / speryear # Unicode \Delta

    # Number of passes of bed-elevation diffusive filtering to perform;
    # this accounts for the bed-slope's influence on sediment flux.
    lastbeddiffpass::Int = max(round(Int, max(ΔVs...) / V₀), 1)

    # Coefficient for the bed-elevation diffusion [s/m²].
    beddiffusecoef::𝔽 = α * Δt / (2 * lastbeddiffpass * δc²)

    return Params(
                  Nx, Ny, δc, δc², δz, N₀, Nwall, Ny½, inletsidewalls, inletsides,
                  maxstep,
                  Δt, lasttime, nw, ns, lastiterw,
                  iceextent, hᵢmax, 𝐸, å,
                  sedfraction, f_sand, f_mud,
                  S₀, h₀, hB, hdry, hΔ, u₀, uₘₐₓ, u_muddep, u_mudero, u_sandero, uΔ, V₀,
                  Qw₀, Qs₀, Qwp, qw₀, q_sand₀, ΔVs, Vsp,
                  daysperΔt, yearsperΔt,
                  lastbeddiffpass, beddiffusecoef,
                  α, β, γ, ϵ, θ, ϖ, ω,
                  backwater_method!,
                  manningscoef², 𝐶𝔣
                  )

end

## ====================================================================================================== preallocate ##
"""
    preallocate(params::Params; typesonly = false)

Return preallocated variables, `all_gridded_vars::AllGriddedVars` and sea_surface_height::𝔽,
with specifications given by `params`. The variables are mostly empty and will require
initialisation. If the optional keyword argument, `typesonly` is set to true, the types of
the variables will be returned instead of the variables themselves.
"""
function preallocate(params::Params; typesonly = false)
    #=
    Some of these preallocations are initialised with some values, whilst others are uninitialised. Whether or not the
    variable is initialised with some value(s) depends on what is done in the next section of the code. There, the
    assignment of the initial state of the variable fields are performed only to the extent that is necessary. If those
    operations do not touch on every element of an array, they are preallocated with zero or `false` values here. Some
    part(s) of some array(s) are preallocated but remain uninitialised despite not being touched upon during the
    initialisations below; this is because we do not expect them to be accessed during the simulation. If the possible
    non-numerical values (e.g. NaNs, Infs) are accessed during the simulation, the resulting errors would actually help
    us catch the problem.
    =#

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny, maxstep, iceextent, hᵢmax = params

    # Sea-surface elevation.
    sea_surface_height = 𝔽(0)

    # Bed and Surface Elevations, Water Depth, and Ice Thickness (optional).
    if isnan(iceextent) && isnan(hᵢmax)

        waters = Waters(
                        surf_elev = Array{𝔽,2}(undef, Nx, Ny),
                        bed_elev = Array{𝔽,2}(undef, Nx, Ny),
                        ice_thickness = Array{Nothing,2}(undef, Nx, Ny),
                        water_depth = Array{𝔽,2}(undef, Nx, Ny),
                        dry_flag = falses(Nx, Ny),
                        flow_speed_x = Array{𝔽,2}(undef, Nx, Ny),
                        flow_speed_y = Array{𝔽,2}(undef, Nx, Ny),
                        flow_speed   = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_x = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_y = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg   = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_x_nxt = zeros(𝔽, Nx, Ny),
                        unit_dischg_y_nxt = zeros(𝔽, Nx, Ny),
                        unit_dischg_nxt   = zeros(𝔽, Nx, Ny)
                        )

    else

        waters = Waters(
                        surf_elev = Array{𝔽,2}(undef, Nx, Ny),
                        bed_elev = Array{𝔽,2}(undef, Nx, Ny),
                        ice_thickness = zeros(𝔽, Nx, Ny), # The zeros are for the ramp-up phase, which is also recorded.
                        water_depth = Array{𝔽,2}(undef, Nx, Ny),
                        dry_flag = falses(Nx, Ny),
                        flow_speed_x = Array{𝔽,2}(undef, Nx, Ny),
                        flow_speed_y = Array{𝔽,2}(undef, Nx, Ny),
                        flow_speed   = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_x = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_y = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg   = Array{𝔽,2}(undef, Nx, Ny),
                        unit_dischg_x_nxt = zeros(𝔽, Nx, Ny),
                        unit_dischg_y_nxt = zeros(𝔽, Nx, Ny),
                        unit_dischg_nxt   = zeros(𝔽, Nx, Ny)
                        )

    end

    # Variables to Track: Water-packet Paths, and Packet-visit Counts for the Purpose of Determining Surface-Elevation.
    water_tracking = WaterTracking(
                                   path_history = zeros(CartesianIndex{2}, maxstep),
                                   step_history = zeros(Int, maxstep - 1),
                                   visit_count = zeros(Int, Nx, Ny),
                                   cumul_surf_elev = zeros(𝔽, Nx, Ny),
                                   )

    # Unit Discharge, Unit-Discharge Place-holder, Flow Speed, All Their x-, y- Components, and Sand Flux.
    sediment_transport = SedimentTransport(
                                           erodibility = ones(𝔽, Nx, Ny),
                                           sand_flux = zeros(𝔽, Nx, Ny),
                                           sand_deposit_volume = zeros(𝔽, Nx, Ny),
                                           mud_deposit_volume = zeros(𝔽, Nx, Ny)
                                           )

    # Boundary and Status flags.
    boundaries = Boundaries(
                            wall_flag = falses(Nx, Ny),
                            ocean_flag = BitArray{2}(undef, Nx, Ny)
                            )

    #=
    Preallocate the two-dimensional sediment strata variable, `seabed`, containing a `SedimentColumn` object in each
    cell. The resulting `typeof(seabed)` is `Array{SedimentColumn{𝔽},2}`, and  `size(seabed)` is `(Nx, Ny)`.
    =#
    seabed = Array{SedimentColumn{𝔽𝕤},2}(undef, Nx, Ny)
    #= NB:
    The numerical type declaration `𝔽𝕤` is kept explicit for clarity. It can be switched to a different type from 𝔽 to
    save memory in the case of high-resolution strata and/or a very-large simulation domain. In that case, make sure
    that the values being assigned are also stable in `𝔽𝕤` (and will not transform or revert back to `𝔽`).
    =#

    all_gridded_vars = AllGriddedVars(waters, water_tracking, seabed, sediment_transport, boundaries)

    # Choose between returning the the actual variables or the types of them.
    if typesonly
        return typeof(all_gridded_vars), typeof(sea_surface_height)
    else
        return all_gridded_vars, sea_surface_height
    end
    #= NB:
    Admittedly, the effort of allocating the variables is wasted if only the type is returned. However, this is a safer
    approach that will be more resilient to any future changes made to AllGriddedVars or any of its component types.
    This is in comparison to the alternative, potentially more efficient approach of using `params` to directly
    determine the type of `all_gridded_vars` (bypassing the actual alloations). Since this only happens once per
    simulation, simplicity and safety were chosen above increased efficiency in this particular case.
    =#

end

## ========================================================================================== populate_initial_state! ##
"""
    populate_initial_state!(params::Params, all_gridded_vars::AllGriddedVars, sea_surface_height::𝔽)

Set all the variables to their initial states at the start of an ArcDelRCM simulation, with
additional specifications provided by `params`.
"""
function populate_initial_state!(params::Params, all_gridded_vars::AllGriddedVars, sea_surface_height::𝔽)

    # Unpack the relevant fields from the Composite Types
    @unpack Nx, Ny, δc, Nwall, Ny½, inletsides, iceextent, S₀, h₀, hB, hdry, qw₀ = params
    @unpack waters, seabed, boundaries = all_gridded_vars
    @unpack surf_elev, bed_elev, water_depth,
            dry_flag,
            unit_dischg_x, unit_dischg_y, unit_dischg,
            flow_speed_x, flow_speed_y, flow_speed    = waters
    @unpack wall_flag, ocean_flag = boundaries

    # Determine whether or not to assign Infinity to the initial/prescribed permafrost thaw depth:
    # - Yes if non-Arctic run (signified by `iceextent` being NaN as assigned in `setparams()`).
    # - No otherwise.
    thaw_depth = ifelse(isnan(iceextent), Inf, 0.5)

    # The seeding unit discharge direction in the ocean basin for direction-weighing of the water packets.
    qw₀seedx = min(qw₀...) / 50
    # qw₀seedy = 0.0

    # Go through all the elements of an array with the same size as `bed_elev` (i.e., all the gridded arrays above);
    # the index `cartesiani` will sequentially take on the values of `CartesianIndex(x::Int, y::Int)` within the
    # bounds of array `bed_elev`, from the top-left of the grid in column-major order to the bottom-right.
    for cartesiani in CartesianIndices(bed_elev)

        if cartesiani ∈ CartesianIndices((1:Nwall, inletsides[1]:inletsides[2])) # <--------------------- Inlet channel

            surf_elev[cartesiani] = (Nwall - cartesiani[1]) * S₀[1] * δc # Gives the channel a positive surface elevation,
                                                                      # with slope S₀ rising from the shoreline inland.
            water_depth[cartesiani] = h₀[1]   # Channel depth

            # Assign initial flow field (initially all parallel to the inlet channel, so the y-component is 0).
            unit_dischg_x[cartesiani] = qw₀[1]

        elseif cartesiani[1] ≤ Nwall # <----------------------------------------------------------------- Inlet wall

            surf_elev[cartesiani] = 0.0   # Wall surface, set to zero to not affect any spatial averaging or smoothing.
            water_depth[cartesiani] = 0.0  # No water atop the inlet wall.
            unit_dischg_x[cartesiani] = 0.0 # No flow atop the inlet wall.
            wall_flag[cartesiani] = true  # Flags will ensure that the wall cells are free from water or sediments.

        else # <----------------------------------------------------------------------------------------- Sea

            surf_elev[cartesiani] = sea_surface_height # Sea surface
            water_depth[cartesiani] = hB               # Sea depth

            # Assign initial flow field (initially all parallel to the inlet channel, so the y-component is 0).
            unit_dischg_x[cartesiani] = qw₀seedx # False, small value only to give a direction for direction-weighing.

        end

        # Define bed elevation.
        bed_elev[cartesiani] = surf_elev[cartesiani] - water_depth[cartesiani]

        # The cell is dry if the water depth is not deep enough.
        dry_flag[cartesiani] = water_depth[cartesiani] < hdry

        # Assign the rest of the flow and velocity fields (Recall: y-component is 0 initially).
        unit_dischg[cartesiani] = unit_dischg_x[cartesiani]
        unit_dischg_y[cartesiani] = flow_speed_y[cartesiani] = 0.0
        flow_speed_x[cartesiani] = unit_dischg_x[cartesiani] / water_depth[cartesiani]
        flow_speed[cartesiani] = unit_dischg[cartesiani] / water_depth[cartesiani]

        # Open-ocean boundary (semi-circular shape if room allows, but at least two-cell wide on the domain edges).
        ocean_flag[cartesiani] = (√( abs2(cartesiani[1] - Nwall) + abs2(cartesiani[2] - Ny½) ) > (Nx - Nwall - 2)) ||
                                 cartesiani[1] > Nx - 2 || cartesiani[2] < 3 || cartesiani[2] > Ny - 2

        #=
        Each cell of `seabed` is initialised to carry a `SedimentColumn` object with attributes:
            z::Vector{T} = a two-element vector with value `-hB` for the top and bottom of the sediment column
            permaz::Vector{T} = a two-element vector with the first value being he thickness of the initial
                                permafrost layer and the second value being the thaw depth.
            strata::Array{Sediment{𝔽𝕤},1} = an empty vector of `Sediment` objects
        =#
        seabed[cartesiani] = SedimentColumn{𝔽𝕤}([-hB, -hB], [0.0, thaw_depth], empty([0], Sediment{𝔽𝕤}))
        #= NB:
        The numerical type `𝔽𝕤` can be switched to other floating-point types (e.g. `Float32`; see "ArcDelConst.jl")
        to save memory in the case of high-resolution strata and/or a very-large simulation domain. In that case, make
        sure that the values being assigned are also compatible with `𝔽𝕤` (and will not tranform or revert back to
        `𝔽`).
        =#
    end

    return nothing

end

## ========================================================================================= print_inheritance_domain ##
"""
    print_inheritance_domain(inheritance::String)

Print the defining parameters of the simulation domain of a saved state to screen. Input
argument, `inheritance`, should specific the full path to a saved state JLD2 file, as
signified by the filename suffix, "_savedstate.jld2".
"""
function print_inheritance_domain(inheritance::String)

    # File existence check.
    isfile(inheritance) || error("cannot find the saved-state file.")

    # Read the `params` variable from the inheritance file.
    inherited_params::DomainParams = jldopen(inheritance, "r") do file
        read(file, "domainparams")
    end # do file

    # Print to screen the defining parameters of the simulation domain.
    println("The following parameters must be matched when inheriting this saved state:\n")
    printstruct(inherited_params, [:Nx, :Ny, :δc, :δz, :N₀, :Nwall])

end

## ========================================================================================= inherit_previous_state! ##
"""
    inherit_previous_state!(
                            inheritance::String, params::Params,
                            all_gridded_vars::AllGriddedVars, sea_surface_height::𝔽
                            )

Set all the variables to their inherited states, read in from existing JLD2 saved-state and
output files at the start of an ArcDelRCM simulation, with additional specifications
provided by `params`. The following input arguments are modified in-place:
  * `all_gridded_vars` of the (concrete) type `all_gridded_vars_type`;
  * `sea_surface_height` of the (concrete) type `sea_surface_height_type`.

!!! note "If there are type-assertion or inexact errors..."
    Type-assertion errors here would signify the type/precision incompatibility between the
    saved state and the current simulation, either software or “hardware”.
"""
function inherit_previous_state!(
                                 inheritance::String, params::Params,
                                 all_gridded_vars::AllGriddedVars, sea_surface_height::𝔽
                                 )

    # File existence check.
    isfile(inheritance) || error("cannot find the saved-state file.")

    print("\n┌ Inheriting simulation state from the file:\n│\n│    ")
    println(inheritance)
    print("│\n└ which may take a while... ")

    # Read everything from the saved state.
    saved_state = load(inheritance)

    inherited_params::DomainParams{Int,𝔽} = saved_state["domainparams"]

    if !matchstructs(params, inherited_params, [:Nx, :Ny, :δc, :δz, :N₀, :Nwall])

        # Print to screen the defining parameters of the simulation domain.
        println("The following parameters must be matched when inheriting this saved state:\n")
        printstruct(inherited_params, [:Nx, :Ny, :δc, :δz, :N₀, :Nwall])

        error("domain dimensions in the current settings do not match the inherited state.")

    end # if !match_inherited_domain(params, inherited_params, (domainfields = [:Nx, :Ny, :δc, :δz, :N₀, :Nwall]))

    @unpack waters, water_tracking, seabed, sediment_transport, boundaries = all_gridded_vars

    @unpack surf_elev, bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters

    @unpack erodibility = sediment_transport

    @unpack wall_flag, ocean_flag = boundaries

    # Assign the values read in from the saved state into the gridded variables.
    for lineari in eachindex(bed_elev)

        surf_elev[lineari] = saved_state["surf_elev"][lineari]
        bed_elev[lineari] = saved_state["bed_elev"][lineari]
        ice_thickness[lineari] = saved_state["ice_thickness"][lineari]
        water_depth[lineari] = saved_state["water_depth"][lineari]

        dry_flag[lineari] = saved_state["dry_flag"][lineari]

        flow_speed_x[lineari] = saved_state["flow_speed_x"][lineari]
        flow_speed_y[lineari] = saved_state["flow_speed_y"][lineari]
        flow_speed[lineari] = saved_state["flow_speed"][lineari]
        unit_dischg_x[lineari] = saved_state["unit_dischg_x"][lineari]
        unit_dischg_y[lineari] = saved_state["unit_dischg_y"][lineari]
        unit_dischg[lineari] = saved_state["unit_dischg"][lineari]

        erodibility[lineari] = saved_state["erodibility"][lineari]

        wall_flag[lineari] = saved_state["wall_flag"][lineari]
        ocean_flag[lineari] = saved_state["ocean_flag"][lineari]

        seabed[lineari] = copy(saved_state["seabed"][lineari])

    end

    sea_surface_height::𝔽 = saved_state["sea_surface_height"]

    println("DONE!\n")

    return all_gridded_vars, sea_surface_height

end

## ===================================================================================================== matchstructs ##
"""
    matchstructs(
                 @nospecialize(structvar1::CP1),
                 @nospecialize(structvar2::CP2),
                 matchfields::Array{Symbol} = empty([], Symbol)
                 ) where {CP1, CP2}

Check if two Composite-Type variables (i.e., structs) are the same. If the optional
`matchfields` argument is provided (must be an array of Symbol(s) matching the relevant
field names), the comparison is performed only on the specified fields.
"""
function matchstructs(
                      @nospecialize(structvar1::CP1),
                      @nospecialize(structvar2::CP2),
                      matchfields::Array{Symbol} = empty([], Symbol)
                      ) where {CP1, CP2}

    # These lines will trigger an error if the input has zero fields or is not a struct.
    fieldcount(CP1);
    fieldcount(CP2);

    # If `fields` is empty, try to assign the list of every available field in Composite Type `CP` into `fields`.
    if isempty(matchfields)

        # Check if the fields are the same in both struct inputs:
        # if they are equal, proceed with the field names stored in `matchfields`;
        # if not, return false immediately.
        (matchfields = fieldnames(CP1)) ≡ fieldnames(CP2) || return false

    end

    # For each field in the list of fields:
    for field in matchfields

        try

            # Either the value of the field in each structvars are equal, or return false immediately.
            getfield(structvar1, field) == getfield(structvar2, field) || return false

        catch # in case either of the getfield(...) fails due to no matching field in one of the structs:

            # Interpret the field-not-found situation as the structs being not equal.
            return false

        end # try

    end # for field in fieldnames(P)

    return true

end

## ================================================================================== reimpose_boundary_conds_dischg! ##
"""
    reimpose_boundary_conds_dischg!(tᵢ::Int, params::Params, waters::Waters)

Reimpose the boundary conditions for unit discharge at time-step number tᵢ.

The input fields `unit_dischg_x`, `unit_dischg_y`, and `unit_dischg` of the struct `waters`
are modified in-place.
"""
function reimpose_boundary_conds_dischg!(tᵢ::Int, params::Params, waters::Waters)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack inletsides, qw₀ = params
    @unpack unit_dischg_x, unit_dischg_y, unit_dischg = waters

    for cartesiani in CartesianIndices((1, inletsides[1]:inletsides[2]))

        unit_dischg_x[cartesiani] = unit_dischg[cartesiani] = qw₀[tᵢ]
        unit_dischg_y[cartesiani] = 0.0

    end

end
## =================================================================================== reimpose_boundary_conds_depth! ##
"""
    reimpose_boundary_conds_depth!(tᵢ::Int, params::Params, waters::Waters)

Reimpose the boundary conditions for water depth at time-step number tᵢ and, by keeping
surface elevation unchanged, the bed elevation too.

Input arguments `bed_elev` and `water_depth` are modified in-place.
"""
function reimpose_boundary_conds_depth!(tᵢ::Int, params::Params, waters::Waters)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack inletsides, h₀ = params
    @unpack surf_elev, bed_elev, water_depth = waters

    # Access the inlet coordinates
    for cartesiani in CartesianIndices((1, inletsides[1]:inletsides[2]))

        water_depth[cartesiani] = h₀[tᵢ]
        bed_elev[cartesiani] = surf_elev[cartesiani] - water_depth[cartesiani]

    end

end

## ================================================================================================== backwater_check ##
"""
    backwater_check(backwater_method::Function)

Give a warning about using methods for backwater calculations that are not intended for a
delta simulation, and provide an option to exit.
"""
@inline function backwater_check(backwater_method::Function)

    if backwater_method in [backwater_gvfmanning!, backwater_gvfflowrcm!]

        @warn "`backwater_method!` is set to be the `$((string ∘ nameof)(backwater_method))` function,\n" *
              "which is only suitable for testing flow field under prescribed and controlled\n" *
              "channel geometries. If this is a delta simulation, please go to the simulation-\n" *
              "starting script (default: \"parameters.jl\") to set the `backwater_method!`\n" *
              "field to `backwater_constslope!`, and re-run the script.\n";

        while (ans = match(r"(?i)^y(es)?$|^n(o)?$", Base.prompt("Proceed with the simulation?"))) ≡ nothing end

        return occursin(r"(?i)^y(es)?$", ans.match)

    else

        return true

    end

end

## =================================================================================================== timestep_check ##
"""
    timestep_check(δc²::𝔽, hB::𝔽, Δt::𝔽, Qs₀::Array{𝔽,1}; ignore_timestep_check)

Check user-selected time-step size to keep it within a "safe" range, where it is less likely
to produce results affected by significant numerical instability.

The criteria is based on the input sediment volume flux, Qs₀, and a dimensionless scale-
height measure,

    ζ ≡ ΔVₛ / (hB × δc²),

where ΔVₛ is the volume of sediments entering the simulation domain in each time step (Δt),
hB is the depth of the basin, and δc² is the area of a single grid cell.

Specifically, the aim is to keep ζ ∈ [1, 3]. If the supplied parameters give an ζ out of
this range, the user will be prompted whether or not Δt should be automatically adjusted
such that ζ will be within the "safe" range. If yes, Δt will be adjusted; otherwise,
nothing is touched.

If Qs₀ is a time series, the highest value is used to get ζ.

If `ignore_timestep_check` is true and ζ is outside of the safe range, a warning will be
displayed, but no further actions will be taken. This is useful for running on computing
clusters when the user knowingly wants to proceed with a particular ζ.

Returns ΔVs, Δt as tuple.
"""
@inline function timestep_check(δc²::𝔽, hB::𝔽, Δt::𝔽, Qs₀::Array{𝔽,1}; ignore_timestep_check)

    # Volume of sediment entering per time step
    ΔVs = Qs₀ * Δt

    # Get the dimensionless scale-height measure that should be kept to 1 ⪅ ζ ⪅ 3
    ζ = scaleheightmeasure(δc², hB, Δt, max(Qs₀...))

    # If ζ is out of the "safe" range
    if ζ > 3 || ζ < 1

        @warn "The Δt specified is outside of the (relatively) safe range in terms of avoiding\n" *
              "results being affected by significant numerical instability. This can be\n" *
              "adjusted automatically. For details, check the documentation of `timestep_check`\n" *
              "for explanations. \n\n" *
              "Be very careful of the results if proceeding without adjustments!\n\n" *
              "FYI: ζ = $(ζ)\n"

        # Still print the warning when `ignore_timestep_check` is true, but then proceed without prompts.
        if ~ignore_timestep_check

            while (
                   ans = match(
                               r"(?i)^y(es)?$|^n(o)?$",
                               Base.prompt("Do you want the time-step size automatically adjusted before proceeding?")
                               )
                   ) ≡ nothing
            end

            # If the user answered positively to the prompt above
            if occursin(r"(?i)^y(es)?$", ans.match)

                # Adjust to the closest "safe" Δt to the user's original choice
                ΔVs_max = ζ > 3 ? 3 * δc² * hB : δc² * hB
                Δt = ΔVs_max * inv(max(Qs₀...))
                ΔVs = Qs₀ * Δt

            end

        end # if ~ignore_timestep_check

    end # if ζ > 3 || ζ < 1

    return ΔVs, Δt

end

## =============================================================================================== scaleheightmeasure ##
"""
    scaleheightmeasure(δc², hB, Δt, Qs₀)

Returns the the dimensionaless scale-height measure, ζ, defined as

    ζ ≡ ΔVₛ / (hB × δc²),

where ΔVₛ is the volume of sediments entering the simulation domain in each time step (Δt),
hB is the depth of the basin, and δc² is the area of a single grid cell. Given input
sediment volume flux Qs₀, ΔVₛ = Qs₀ × Δt.
"""
@inline function scaleheightmeasure(δc², hB, Δt, Qs₀)

    # Volume of sediment entering per time step
    ΔVs = Qs₀ * Δt

    # Volume of each simulation grid cell
    Vc = δc² * hB

    # Return a dimensionless scale-height measure
    return ΔVs * inv(Vc)

end

## ==================================================================================================== seabedtilt_*! ##
"""
    seabedtilt_simple!(
                      alongslopedistance::Function,
                      h_shallow,
                      baselength,
                      params::Params,
                      waters::Waters,
                      seabed::Array{SedimentColumn{𝔽𝕤},2}
                      )

Assign values to the bed-elevation grid (`bed_elev` field in `waters`) such that there is a
tilt from an elevation of `h_shallow` to the ocean-basin depth (`hB` field in `params`) over
a physical distance of `baselength`, starting from the inlet channel. The anonymous function
`alongslopedistance` calculates the distance along-slope such that the elevation change from
grid cell to grid cell can be determined; therefore, whether the tilt is a linear-plane tile
or a radial (conic-like) tilt is fully dependent on `alongslopedistance`, which takes four
arguments: i) the current grid-cell coordinate in the x direction, ii) the starting co-
ordinate of the tile in the x direction (`≡ Nwall + 1`), iii) the current grid-cell co-
ordinate in the y direction, iv) the y coordinate of the centre of the inlet channel.
"Current" in the context of the description above means "wherever the loop has arrived at"
when iterating over the entire grid.

The water depth, any flow speed, and the `z[1:2]` field of all `SedimentColumn`s in `seabed`
are all updated according to the resulting `bed_elev`.

This function is intended to be called right after [`populate_initial_state!`](@ref), before
any physical processes are simulated.
"""
function seabedtilt_simple!(
                           alongslopedistance::Function,
                           η_shallow,
                           baselength,
                           params::Params,
                           waters::Waters,
                           seabed::Array{SedimentColumn{𝔽𝕤},2}
                           )

    @unpack Nx, Ny, δc, Nwall, Ny½, hB, hdry = params
    @unpack surf_elev, bed_elev, water_depth, flow_speed_x, flow_speed, unit_dischg_x, unit_dischg = waters

    # Restrict the water depth to be no shallower than `hdry`.
    η_shallow = min(η_shallow, -hdry)

    # Base length in terms of number of grid cells.
    baselength_δc = baselength / δc

    # Intended slope of the tilted bed (in terms of number of grid cells).
    bed_slope = (-hB - η_shallow) / baselength_δc

    # Starting index in the x direction, just after the inlet wall.
    Nstart = Nwall + 1
    Nend = min(Nstart + round(Int, baselength_δc), Nx)

    @inbounds @simd for cartesiani in @view CartesianIndices(bed_elev)[Nstart:Nend, 1:Ny]

        # Sculpt the tilted bed surface.
        bed_elev[cartesiani] = η_shallow + alongslopedistance(cartesiani[1], Nstart, cartesiani[2], Ny½) * bed_slope

        # Re-define water depth.
        water_depth[cartesiani] = surf_elev[cartesiani] - bed_elev[cartesiani]

        # Re-define flow speed.
        flow_speed_x[cartesiani] = unit_dischg_x[cartesiani] / water_depth[cartesiani]
        flow_speed[cartesiani] = unit_dischg[cartesiani] / water_depth[cartesiani]

        # Re-define the bed-rock and bed-surface elevations of the empty sedimentary record grid.
        seabed[cartesiani].z[1] = bed_elev[cartesiani]  # Bed-rock/-bottom elevation
        seabed[cartesiani].z[2] = bed_elev[cartesiani]  # Bed-surface elevation

    end

end

"""
    seabedtilt_linear!(η_shallow, baselength, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2})

Tilt the bed elevation linearly (i.e., as a plane), starting from `η_shallow` from the
inlet wall towards the original ocean-basin bed elevation over a distance of `baselength`
towards the open ocean. See [`seabedtilt_simple!`](@ref) for a more detailed description.
"""
seabedtilt_linear!(η_shallow, baselength, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2}) =
    seabedtilt_simple!(η_shallow, baselength,
                      params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2}) do x, Nstart, _, _
        return x - Nstart
    end # do

"""
    seabedtilt_radial!(η_shallow, baselength, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2})

Tilt the bed elevation radially (i.e., like a fan), starting from `η_shallow` from the
middle of the inlet-channel exit towards the original ocean-basin bed elevation over a
distance of `baselength` towards the three ocean boundaries of the simulation domain.
See [`seabedtilt_simple!`](@ref) for a more detailed description.
"""
seabedtilt_radial!(η_shallow, baselength, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2}) =
    seabedtilt_simple!(η_shallow, baselength,
                      params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2}) do x, Nstart, y, Ny½
        return √( abs2(x - Nstart) + abs2(y - Ny½) )
    end # do

## ==================================================================================== Helper: seabedtilt_sculpter_! ##
"""
    seabedtilt_sculpter_!(specifier::Vector, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2})

Helper function to choose the appropriate `seabedtilt_*!(...)` function to call. The first
element of `specifier` determines the shape/nature of the tilt, from which the initial bed
elevation goes from the specified `η_shallow` to the originally initialised ocean-bottom
elevation `-hB` over a physical distance of `baselength`.

Current choices are:

  * `:linear` -> [`seabedtilt_linear!`](@ref)
  * `:radial` -> [`seabedtilt_radial!`](@ref)

The remaining elements of `specifier` should exactly give the arguments of the
`seabedtilt_*!` function specified by the symbol keyword, excluding `params`, `waters` and
`seabed`. For example, for a linear tilt starting from the inlet wall boundary at -5 metres
elevation going to the originally initialised bed elevation over a distance of 2400 metres,
`specifier` would be `[:linear, -5, 2400]`. See the documentations of the `seabedtilt_*!`
functions for more details on the nature of the tilt shapes.
"""
@inline function seabedtilt_sculpter_!(specifier::Vector, params::Params, waters::Waters, seabed::Array{SedimentColumn{𝔽𝕤},2})

    if isempty(specifier)
        # Do nothing.
    elseif specifier[1] ≡ :linear
        seabedtilt_linear!(specifier[2], specifier[3], params, waters, seabed)
    elseif specifier[1] ≡ :radial
        seabedtilt_radial!(specifier[2], specifier[3], params, waters, seabed)
    else
        @error "The specified seabed-tilting shape is not recognised or has not yet been built." specifier[1]
    end

end

## ======================================================================================================== island_*! ##
"""
    island_rectangle!(xrange::UnitRange, yrange::UnitRange, waters::Waters, boundaries::Boundaries)

Create a rectangular island in the simulation domain such that flow has to go around it. The
rectangle covers `xrange` in the x direction and `yrange` in the y direction inclusively.
Within the island, the surface elevation, water depths, and unit discharges are set to 0.0
to avoid influencing other calculations. The flow-blocking itself is signaled by the wall
flag, which is set to `true`.

Input arguments `waters` (specifically the `surf_elev`, `water_depth`, and `unit_dischg_x`
fields) and `boundaries` (specifically the `wall_flag` field) are modified in-place.
"""
function island_rectangle!(xrange::UnitRange, yrange::UnitRange, waters::Waters, boundaries::Boundaries)

    # Unpack the relevant fields from the Composite Types
    @unpack surf_elev, water_depth, unit_dischg_x = waters
    @unpack wall_flag = boundaries

    # Get the dimensions of the simulation domain (to avoid going out of bounds).
    Nx, Ny = size(wall_flag)

    # Limit the ranges to within boundaries of the Nx-by-Ny grid.
    xrange = max(xrange.start, 1):min(xrange.stop, Nx)
    yrange = max(yrange.start, 1):min(yrange.stop, Ny)

    # Go through the grid cells included in the rectangle.
    for cartesiani in CartesianIndices((xrange, yrange))

        surf_elev[cartesiani] = 0.0   # Wall surface, set to zero to not affect any spatial averaging or smoothing.
        water_depth[cartesiani] = 0.0  # No water atop the inlet wall.
        unit_dischg_x[cartesiani] = 0.0 # No flow atop the inlet wall.
        wall_flag[cartesiani] = true  # Flags will ensure that the wall cells are free from water or sediments.

    end # for cartesiani in CartesianIndices((xrange, yrange))

end

"""
    island_ellipse!(centrexy::Tuple{Int, Int}, rxy::Tuple{Int, Int}, waters::Waters, boundaries::Boundaries)

Create an elliptical island in the simulation domain such that flow has to go around it.
The island will be centred at coordinate (x, y) = `centrexy` and has radii of `rxy`, a two-
element `Tuple`, along the x- and y- direction, respectively, varying smoothly in between.
Within the island, the surface elevation, water depths, and unit discharges are set to 0.0
to avoid influencing other calculations. The flow-blocking itself is signaled by the wall
flag, which is set to `true`.

Input arguments `waters` (specifically the `surf_elev`, `water_depth`, and `unit_dischg_x`
fields) and `boundaries` (specifically the `wall_flag` field) are modified in-place.
"""
function island_ellipse!(centrexy::Tuple{Int, Int}, rxy::Tuple{Int, Int}, waters::Waters, boundaries::Boundaries)

    # Unpack the relevant fields from the Composite Types
    @unpack surf_elev, water_depth, unit_dischg_x = waters
    @unpack wall_flag = boundaries

    # Get the dimensions of the simulation domain (to avoid going out of bounds).
    Nx, Ny = size(wall_flag)

    # Get the bounding box around the circle
    xrange = max(centrexy[1] - rxy[1], 1):min(centrexy[1] + rxy[1], Nx)
    yrange = max(centrexy[2] - rxy[2], 1):min(centrexy[2] + rxy[2], Ny)

    # Go through the grid cells included in the bounding box.
    for cartesiani in CartesianIndices((xrange, yrange))
        # If the grid-coordinate lies on or within the limits of the ellipse:
        if sum(abs2, (Tuple(cartesiani) .- centrexy) ./ rxy) ≤ 1

            surf_elev[cartesiani] = 0.0   # Wall surface, set to zero to not affect any spatial averaging or smoothing.
            water_depth[cartesiani] = 0.0  # No water atop the inlet wall.
            unit_dischg_x[cartesiani] = 0.0 # No flow atop the inlet wall.
            wall_flag[cartesiani] = true  # Flags will ensure that the wall cells are free from water or sediments.

        end # if √sum(abs2, Tuple(cartesiani) .- centrexy) ≤ r²
    end # for cartesiani in CartesianIndices((xrange, yrange))

end

"""
    island_circle!(centrexy::Tuple{Int, Int}, radius::Int, waters::Waters, boundaries::Boundaries)

Create a circular island in the simulation domain such that flow has to go around it. The
island will be centred at coordinate (x, y) = `centrexy` and has a radius of `radius`.
Within the island, the surface elevation, water depths, and unit discharges are set to 0.0
to avoid influencing other calculations. The flow-blocking itself is signaled by the wall
flag, which is set to `true`.

Input arguments `waters` (specifically the `surf_elev`, `water_depth`, and `unit_dischg_x`
fields) and `boundaries` (specifically the `wall_flag` field) are modified in-place.
"""
island_circle!(centrexy::Tuple{Int, Int}, radius::Int, waters::Waters, boundaries::Boundaries) =
    island_ellipse!(centrexy, radius, radius, waters, boundaries)

## ========================================================================================= Helper: island_builder_! ##
"""
    island_builder_!(specifier::Vector, waters::Waters, boundaries::Boundaries)

Helper function to choose the appropriate `island_*!(...)` function to call. The first
element of `specifier` determines the shape of the island, where flow is prohibited and
the wall flag is `true`. Current choices are:

  * `:rectangle` -> [`island_rectangle`](@ref)
  * `:ellipse` -> [`island_ellipse`](@ref)
  * `:circle` -> [`island_circle`](@ref)

The remaining elements of `specifier` should exactly give the arguments of the `island_*!`
function specified by the symbol keyword, excluding `waters` and `boundaries`. For example,
for a rectangular island, `specifier` would be `[:rectangle, 6:20, 60:80]`. See the
documentations of the `island_*!` functions for their expected arguments.
"""
@inline function island_builder_!(specifier::Vector, waters::Waters, boundaries::Boundaries)

    if isempty(specifier)
        # Do nothing.
    elseif specifier[1] ≡ :rectangle
        island_rectangle!(specifier[2], specifier[3], waters, boundaries)
    elseif specifier[1] ≡ :ellipse
        island_ellipse!(specifier[2], specifier[3], waters, boundaries)
    elseif specifier[1] ≡ :circle
        island_circle!(specifier[2], specifier[3], waters, boundaries)
    else
        @error "The specified island-block shape is not recognised or has not yet been built." specifier[1]
    end
    # Aside: I could have used multiple dispatch in the above, since the shapes have the same number of arguments,
    # but this is more flexible when adding future shapes, which may require different number of arguments.

end

## ====================================================================================================== printstruct ##
"""
    printstruct(io::IO, structvar, fields = empty([], Symbol))
    printstruct(structvar, fields = empty([], Symbol))

Print to an IO stream, `io`, each field and value pair of `structvar`, line by line. If `io`
is not specified, `stdout` is assumed. Optionally, a list of fields to be printed can be
specified (default is a list of one empty String).
"""
function printstruct(io::IO, @nospecialize(structvar::CP), fields::Array{Symbol} = empty([], Symbol)) where CP

    fieldcount(CP); # This line will trigger an error if the input has zero fields or is not a struct.

    # If `fields` is empty, assign the list of every available field in Composite Type `CP` into `fields`.
    isempty(fields) && (fields = fieldnames(CP))

    # For each field in the list of fields:
    for field in fields

        # Load the field
        fieldval = getfield(structvar, field)

        # Try to get the length of the field (if possible)
        fieldlen::Int = try
            length(fieldval)
        catch MethodError
            0
        end # try

        # If the field is not too long to print every element:
        if fieldlen < 6
            # Print, line by line, <fieldname>:<tab-space><value of structvar.<fieldname>>.
            println(io, field, ":\t", fieldval)
        else # Otherwise, print only the minimum, mean, and maximum of the values of the field.
            # Print, line by line, <fieldname>:<tab-space><statistics of structvar.<fieldname>>.
            println(io, field, " [min, mean, max]:\t", [min(fieldval...), sum(fieldval) / fieldlen, max(fieldval...)])
        end

    end # for field in fieldnames(P)

end
printstruct(structvar) = printstruct(stdout, structvar)
printstruct(structvar, fields::Array{Symbol}) = printstruct(stdout, structvar, fields)

## =================================================================================================== recordsettings ##
"""
    recordsettings(file_name_stem::String, parent_dir_path::String, params::Params)

Write the simulation settings as contained in the struct `params` to a text file. The file
will be located at "<`parent_dir_path`>/OUTPUT/<`file_name_stem`>_settings.txt".
"""
function recordsettings(file_name_stem::String, parent_dir_path::String, params::Params)

    # Open a new file for writing; create or replace if necessary:
    open(
         joinpath(parent_dir_path, "OUTPUT", join([file_name_stem, "_settings.txt"]));
          create = true,
          truncate = true
         ) do io

        println(io, "#")
        println(io, "#     record of simulation settings: ", file_name_stem, "_settings.txt\n#")
        println(io, "# The list of values below are all the fields contained in the parameter struct")
        println(io, "# (of Composite Type `::Params`) generated by `setparams()` function.\n#\n")

        # Print, line by line, <fieldname>:<tab-space><value of params.<fieldname>> to file.
        printstruct(io, params)

    end # do io

end

## ================================================================================================== init_outputfile ##
# Create the Output File with the Appropriate Spaces and Attributes to Hold the State Variables (at each time step)
"""
    init_outputfile(
                    start_time::Int,
                    time_steps_per_year::Int,
                    file_name_stem::String,
                    parent_dir_path::String,
                    params::Params;
                     steps_per_output::Int = 1,
                     ice_free::Bool = true, give_time_in_years::Bool = false,
                     include_flags::Bool = false
                    )

Create the NetCDF file that will hold all the state variables as output. Dimension
variables (vectors) are generated as part of this function. All variables to be written
also have their attributes defined herein. Optionally, custom paths and filenames could be
specified as keyword arguments, as are the boolean “triggers” to save the time as seconds
(instead of time-step numbers) and to include various status flags as part of the output.

Return four values:
  * `pf_nc`:  full path and filename of the output NetCDF file;
  * `pf_jld`: full path and filename of the sediment strata JLD2 file;
  * `pf_ss`:  full path and filename of the saved-state JLD2 file;
  * `include_flags`: identical to the `include_flags` input argument.

The argument `file_name_stem` is the “base” part of the output-file names. Suffix will be
appended to this base name for the NetCDF file containing all state-variables output
("_statevars.nc") and for the sediment-strata output ("_strata.jld2"). Note the inclusion of
an underscore and file extensions in both cases. A directory "OUTPUT", if not already
existing, is created under `parent_dir_path` in order hold the output files.

!!! tip "NetCDF compression"
    Compression of the NetCDF file is possible, as is changing the variables' numeric
    types. These can be achieved by modifying the `nccreate(...)` function calls near the
    end of this function. See documentations of [`nccreate`](@ref) for more details.
    Compression always carries trade-offs between performance (including the future reading-
    back of the file) and space.
"""
function init_outputfile(
                         start_time::Int,
                         time_steps_per_year::Int,
                         file_name_stem::String,
                         parent_dir_path::String,
                         params::Params;
                          steps_per_output::Int = 1,
                          ice_free::Bool = true, give_time_in_years::Bool = false,
                          include_flags::Bool = false
                         )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny, δc, lasttime, Δt, yearsperΔt = params

    # Calculate the time to start recording, such that the last time step is naturally included given the
    # skipping step size of `steps_per_output`.
    record_start_time = start_time + (lasttime - start_time) % steps_per_output

    # # If skipping every `steps_per_output` steps leads to the last time step being excluded:
    # if last(start_time:steps_per_output:lasttime) != lasttime
    #     # Include the last time step explicitly.
    #     timestep_vector = [collect(start_time:steps_per_output:lasttime); lasttime]
    # else
    #     timestep_vector = collect(start_time:steps_per_output:lasttime)
    # end

    # Create vectors for each of the dimensions (see the definitions of attributes below for details):
    𝐱 = Cfloat.(collect(1:Nx) .* δc)       # Unicode \bfx
    𝐲 = Cfloat.(collect(1:Ny) .* δc)       # Unicode \bfy
    give_time_in_years ? time = (collect(record_start_time:steps_per_output:lasttime) |>
                                 x -> @. Cfloat(muladd(yearsperΔt, x % time_steps_per_year, x ÷ time_steps_per_year))) :
                         time = Cint.(collect(record_start_time:steps_per_output:lasttime))
    #= Explanation:
    To give accurate time in years whilst allowing for model-years containing less than 365.25 days, do the following:
    Set up a vector containing each time steps: `[start_time, start_time - 1, ... , lasttime - 1, lasttime]`.
    Pipe (with the `|>` operator) it to an anonymous function with argument x, such that
        (1) Number of integer years that has elapsed is `x ÷ time_steps_per_year` (truncated to an integer);
        (2) Number of time steps since the completion of the previous integer year is `(x % time_steps_per_year)`;
        (3) Number of (fraction) years in each time step is `yearsperΔt ≡ Δt / speryear`, where `speryear` is
            `365.25 × 24 × 60 × 60 == 31557600`).
    The accurate time is then (1) + (2) × (3), equivalent to `muladd((2), (3), (1))`, broadcast with the `@.` macro to
    each element of the time-step vector.
    =#

    # Define attributes for the following:
    # ▶ Dimensions:
    𝐱_attr = Dict(
                  "longname" => "Domain-coordinate component in the direction parallel to the inlet flow",
                  "remarks"  => "Distance from the inlet-wall edge of the domain.",
                  "units"    => "metre"
                  )

    𝐲_attr = Dict(
                  "longname" => "Domain-coordinate component in the direction perpendicular to the inlet flow",
                  "remarks"  => "Distance from one of the side walls (perpendicular to the inlet wall) of the domain.",
                  "units"    => "metre"
                  )

    if give_time_in_years #----------------------------> Giving time in seconds:

        time_attr = Dict(
                         "longname" => "Time",
                         "remarks"  => "The first time step is at time = Delta_t.",
                         "units"    => "second"
                         )

    else #-----------------------------------------------> Not giving time in seconds:

        time_attr = Dict(
                         "longname" => "Time-step number",
                         "remarks"  => "The start of the simulation is time step number 1.",
                         "units"    => "timestep"
                         )

    end # if give_time_in_years; else ...

    # ▶ Bed elevation, η, in metres:
    η_attr = Dict(
                  "longname" => "Sea-bed elevation",
                  "remarks"  => "Defined as the top of sediments or, if no accumulations, the bed rock.",
                  "units"    => "metre"
                  )

    # ▶ Water surface elevation, H, in metres:
    H_attr = Dict(
                  "longname" => "Water-surface elevation",
                  "units"    => "metre"
                  )

    # ▶ Water depth, h, in metres:
    h_attr = Dict(
                  "longname" => "Water depth",
                  "remarks"  => "Defined as water surface minus bed elevation.",
                  "units"    => "metre"
                  )

    # ▶ Unit water discharge, 𝐪 (flow rate per unit width of cross section, like a slit), in metres squared per second:
    qw_attr = Dict(
                   "longname" => "Unit water discharge",
                   "remarks"  => "Defined as the flow rate per unit width of cross section, like a slit.",
                   "units"    => "metre squared per second"
                   )

    # ▶ Flow velocity, 𝐮, in metres per second:
    u_attr = Dict(
                  "longname" => "Flow velocity",
                  "remarks"  => "Calculated as: unit discharge divided by water depth.",
                  "units"    => "metre per second"
                  )

    # ▶ Ice thickness, hᵢ, in metres (optional):
    ice_free || (hᵢ_attr = Dict(
                               "longname" => "Ice thickness",
                               "remarks"  => "Effective water depth is the water depth minus ice thickness.",
                               "units"    => "metre"
                               ))

    # ▶ Boundary and Status flags (optional):
    if include_flags

        wallflag_attr = Dict(
                             "longname" => "Inlet-wall cells",
                             "remarks"  => "Flags marking the cells forming the inlet wall.",
                             "units"    => "binary"
                             )
        oceanflag_attr = Dict(
                              "longname" => "Ocean-boundary cells",
                              "remarks"  => "Flags marking the cells belonging to the ocean boundary of the domain.",
                              "units"    => "binary"
                              )
        dryflag_attr = Dict(
                            "longname" => "Dry cells",
                            "remarks"  => "Flags marking where the water is shallower than a threshold value, `hdry`.",
                            "units"    => "binary"
                            )

    end # if include_flags

    # Layout the path and name of the output file:
    # ▸ Either an `OUTPUT` directory already exists at the end of the path, OR create one.
    isdir(joinpath(parent_dir_path, "OUTPUT")) || mkdir("OUTPUT")

    # ▸ Full path of the NetCDF to use for the output of state variables. (`pf` stands for “path & filename”.)
    pf_nc::String  = joinpath(parent_dir_path, "OUTPUT", join([file_name_stem, "_statevars.nc"]))
    pf_jld::String = joinpath(parent_dir_path, "OUTPUT", join([file_name_stem, "_strata.jld2"]))
    pf_ss::String  = joinpath(parent_dir_path, "OUTPUT", join([file_name_stem, "_savestate.jld2"]))

    # Display for information purposes.
    print("\n┌ The output files are going to be saved as the following:\n│\n│    ")
    println(pf_nc)
    print("│           and\n│    ")
    println(pf_jld)
    println("└\n")

    # Check for existing file:
    # ▸ If file already exists:
    while isfile(pf_nc) || isfile(pf_jld)

        # Ask the user to decide whehter to overwrite or to save as a different file.
        @warn "Files with the same path and filenames already exist:";
        println("┌ Do you want to replace the existing files?\n│")
        println("├─ Press “return”/“enter” without typing to replace the files.")
        println("│    OR")
        println("├─ Enter a new filename “stem” to save in the same directory.\n│")
        println("│   ╭┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╮")
        println("│   ┊ NB: If you are giving a new filename, avoid using     ┊")
        println("│   ┊     spaces and try to stick to standard characters.   ┊")
        println("│   ┊     Any <suffix.extension> (e.g. “_strata.jld2”, with ┊")
        println("│   ┊     the underscore) will be added for you. You just   ┊")
        println("│   ┊     need to provide the “stem” part of the filename.  ┊")
        println("│   ╰┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╯\n│")
        replace_or_rename = String(
                                   Base.prompt("└ Your response")
                                   )

        # Remove or assign new filenames, depending on user input:
        if length(replace_or_rename) ≡ 0

            # Remove the existing files.
            rm(pf_nc, force = true)
            rm(pf_jld, force = true)
            break

        else

            # Replace with the new filename in the same path.
            pf_nc  = joinpath(parent_dir_path, "OUTPUT", join([replace_or_rename, "_statevars.nc"]))
            pf_jld = joinpath(parent_dir_path, "OUTPUT", join([replace_or_rename, "_strata.jld2"]))
            pf_ss  = joinpath(parent_dir_path, "OUTPUT", join([replace_or_rename, "_savestate.jld2"]))

            # Display for information purposes.
            print("\n┏ The output files are now going to be saved as the following:\n┃\n┃    ")
            println(pf_nc)
            print("┃           and\n┃    ")
            println(pf_jld)
            println("┗\n")

        end # if length(replace_or_rename) ≡ 0; else ...

        file_name_stem = replace_or_rename

    end # while isfile(pf_nc) || isfile(pf_jld)

    print("[ Writing simulation parameters to a “settings” text file to accompany the output files... ")
    recordsettings(file_name_stem, parent_dir_path, params)
    println("DONE!\n")

    print("[ Creating and preparing NetCDF file to record the state variables... ")

    # Create the NetCDF file (first line) and create spaces therein to hold the variables to be written:
    # (NB: Compression is possible, as is changing the variables' numeric types. See documentation of `nccreate`.)
    nccreate(pf_nc, "eta", "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr; atts = η_attr,  t = NC_FLOAT)
    nccreate(pf_nc, "H",   "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr; atts = H_attr,  t = NC_FLOAT)
    nccreate(pf_nc, "h",   "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr; atts = h_attr,  t = NC_FLOAT)
    nccreate(pf_nc, "qw",  "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr; atts = qw_attr, t = NC_FLOAT)
    nccreate(pf_nc, "u",   "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr; atts = u_attr,  t = NC_FLOAT)
    ice_free || # ← Either this is an ice-free run, OR ↓ create the variable for ice thickness in the output
        nccreate(pf_nc, "hi",  "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr;
                 atts = hᵢ_attr, t = NC_FLOAT)
    if include_flags # (Recall: The inclusion of flags is optional.)

        nccreate(pf_nc, "wall",  "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr;
                 atts = wallflag_attr, t = NC_BYTE)
        nccreate(pf_nc, "ocean", "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr;
                 atts = oceanflag_attr, t = NC_BYTE)
        nccreate(pf_nc, "dry",   "x", 𝐱, 𝐱_attr, "y", 𝐲, 𝐲_attr, "time-step", time, time_attr;
                 atts = dryflag_attr, t = NC_BYTE)

    end # if include_flags

    println("DONE!\n")

    # `pf_nc` to pass onto NetCDF write functions;
    # `pf_jld` to pass onto JLD2 file writers;
    # `pf_ss` to pass onto the `save_state` function;
    # `include_flags` to trigger saving of flags in the NetCDF file.
    return pf_nc, pf_jld, pf_ss, include_flags
end
