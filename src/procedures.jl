#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
#=
    source file: procedures.jl

Define all the functions that constitute the main procedures in an ArcDelRCM (Arctic Delta
RCM) simulation. Some of the individual tasks are, in turn, grouped together and executed
one after another (often in loops) to form procedure units that are more complicated. Those
are refered to as “tasks”, and are located in the file, "src/tasks.jl". Some of the
“support” functions called by the procedures herein are defined in a separate file,
"src/support.jl". They are functions that provide “finding and choosing” or arithmetic
services, and do not have Composite-Typed structs as input arguments.

*A note on the order of arguments:*

The following is the guideline set up so that the input arguments of the functions herein
follow some patterns. In the order of appearence (and only if the field is applicable):

  1. {Functions (in the case of higher-order functions, HOF)},
  2. {File and path information of the output file},
  3. {Time-step- and/or iteration-counter- related variables},
  4. {Explicit coordinates, such as `px`, `py`, or `last_step`},
  5. {Variables of Composite Type, Params, or their enclosed fields},
  6. {Variables of Composite Type, Waters, or their enclosed fields},
  7. {Variables of Composite Type, WaterTracking, or their enclosed fields},
  8. {Array of SedimentColumns, typically of type `Array{SedimentColumn{T},2}`},
  9. {Variables of Composite Type, SedimentTransport, or their enclosed fields},
  10. {Any scalar variables, such as `sea_surface_height` or `packet_residual_volume`},
  11. {Variables of Composite Type, Boundaries, or their enclosed fields},
  12. {Any optional arguments, often represented as `args...`}.

In the cases of higher-order functions (HOF) with aliases to their function call with
attached `do ... end` blocks, it is their aliases that follow the above order of arguments.

==============================================
|              TABLE of CONTENTS             |
|             (look-up along the             |
|               right-hand edge              |
|                of this file)               |
----------------------------------------------
##         (include, import, set up)        ##
## ===================== impose_winter_ice! ##
## ============================== melt_ice! ##
## ================================ startxy ##
## =========================== spawn_water! ##
## =========================== stepn_water! ##
## ======================== HOF: backwater! ##
## ----------- Alias: backwater_constslope! ##
## ----------- Alias: backwater_gvfmanning! ##
## ----------- Alias: backwater_gvfflowrcm! ##
## ========================== full_surface! ##
## ==================== migrate_shore_bank! ##
## ==================== update_after_water! ##
## ============================ spawn_sand! ##
## ============================ stepn_sand! ##
## ================= deposit_or_erode_sand! ##
## ============================== spawn_mud ##
## ============================= stepn_mud! ##
## ================== deposit_or_erode_mud! ##
## ========================= bed_diffusion! ##
## ========================== thaw_stefans! ##
## ============== HOF: sedimentary_records! ##
## ---- Alias: sedimentary_records_icefree! ##
## ---- Alias: sedimentary_records_withice! ##
## ======================== write_statevars ##
## ======================= write_stateflags ##
## =========================== write_strata ##
## ============================= save_state ##

*Table-of-Contents Note:*
Functions are defined in the order of first appearance in a simulation,
then in the order of enclosure, but also grouped by multiple dispatch.
=#

include("support.jl") # Contains the functions that are used by the procedures of the simulation

# Choose one of the following two ways to enable a `convolve` function:
if ((fastconvpath = joinpath(pwd(), "../../FastConv.jl/src/FastConv.jl")) |> isfile) || # if pwd() is the "test" sub-dir
   ((fastconvpath = joinpath(pwd(), "../FastConv.jl/src/FastConv.jl")) |> isfile)

    include(fastconvpath);
    import .FastConv: fastconv

    convolve(A::BitArray{T}, K::Array{N}) where {T, N} = fastconv(Array{N}(A), K)[2:(end - 1), 2:(end - 1)]
    convolve(A::Array{T}, K::Array{N}) where {T, N} = fastconv(A, Array{T}(K))[2:(end - 1), 2:(end - 1)]

    # Next six lines: `@doc ... -> convolve`
    # Add documentation to `convolve`, which were defined to wrap around `fastconv`.
    @doc "
    convolve(A::BitArray{T}, K::Array{N}) where {T, N}
    convolve(A::Array{T}, K::Array{N}) where {T, N}

Wrappers around the [`fastconv`](@ref) function in the FastConv.jl package.
" -> convolve

else # Otherwise, if `FastConv` cannot be found or is deprecated, revert to using `LocalFilters`:

    @info "The preferred package for performing convolutions, FastConv.jl, is not found
      in the path:
        \"$fastconvpath\"
      Check if its package folder is in the same parent directory as ArcDelRCM.jl.
      It is available from https://github.com/aamini/FastConv.jl [as of 2nd July, 2020].
      Reverting to use the `convolve` function from LocalFilters.jl."

    import LocalFilters: convolve

end # if isfile("FastConv.jl/src/FastConv.jl")

using NetCDF, JLD2

#------- Directions and Distances -------#
# It may help to visualise the grid as
# follows:
#
#        ⬇  Inlet is from the top ⬇
#    ____________________________________
#   |  1, 1 |  1, 2 |  1, 3 | ⋯ |  1, Ny |
#   |  2, 1 |  2, 2 |  2, 3 | ⋯ |  2, Ny |
#   |  3, 1 |  3, 2 |  3, 3 | ⋯ |  3, Ny |
#       ⋮        ⋮       ⋮     ⋱     ⋮
#   | Nx, 1 | Nx, 2 | Nx, 3 | ⋯ | Nx, Ny |
#    ————————————————————————————————————
#
# This way, the indexing of any distance-
# or directional matrices (3-by-3) and
# vectors (9-element) related to a single
# step are as follows:
#
#        ⬇  Inlet is from the top ⬇
#                     ⋮
#             ___________
#            | 1 | 4 | 7 |
#   ⋯        | 2 | 5 | 8 |        ⋯
#            | 3 | 6 | 9 |
#             ———————————
#                     ⋮
# where 5 represents the current/original
# location.
# # # # # # # # # # # # # # # # # # # # # #
## =============================================================================================== impose_winter_ice! ##
# Impose the Maximum Reach and Thickness of Winter Ice Cover on the Delta
"""
    impose_winter_ice!(𝕀⁺°::Array{Float64,2}, params::Params, waters::Waters; I⁺°₀ = 0.0)

Impose the maximum thickness and extent of winter ice on the delta, and set the initial
positive-degree-day index for the underlying bed.

The ice extent is the fraction of the radius of the semi-circle (with the origin at the
centre of the inlet exit point, with an equivalent area as the delta), measured from the
ocean towards the delta, that is covered with ice. The ice thickness must not exceed 99.99%
of the local water depth; it also tapers off over a ten-grid-cell distance from the maximum
to exactly zero at just beyond the ice-extent boundary. These measures are to ensure the
numerical stability of the simulation.

The positive-degree-day index, 𝕀⁺°, is the days since the end of the frozen season (i.e.,
winter) during which the temperature is above 0ºC. In the usage here, it is to track how
long has the sediment bed surface been exposed to above-zero temperatures. If the imposed
ice thickness at a grid cell is such that the ice would not float, the ice is assumed to
be bed-fast, and 𝕀⁺° for the cell is set to 0.0. For all other grid cells, this is set to
an offset value, I⁺°₀, which represents the number of positive-degree days at the start of
the model year.

Input arguments `ice_thickness` and `𝕀⁺°` are modified in-place.
"""
function impose_winter_ice!(𝕀⁺°::Array{Float64,2}, params::Params, waters::Waters; I⁺°₀ = 0.0)
    #= NB: All radii are in units of cell-width, δc. =#

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny, Nwall, Ny½, iceextent, hᵢmax, hΔ, uΔ = params
    @unpack ice_thickness, water_depth, flow_speed = waters

    # Determine the area of the delta (inlet channel excluded), 𝒜, then its equivalent-semi-circle radius,
    # then multiply by the complement of the ice extent to get the ice-free radius.
    ice_free_radius = delta_area(
                                 hΔ, uΔ,
                                 view(water_depth, (Nwall+1):Nx, 1:Ny),
                                 view(flow_speed, (Nwall+1):Nx, 1:Ny)
                                 ) |> 𝒜 -> √(2𝒜 / π) * (1 - iceextent)

    # The ice thickness goes from zero at the ice-free radius to full thickness over the distance of ten grid cells;
    # this radius at which full thickness is achieved will serve as the second argument to `sculpt_ice!` below.

    # “Lay down” the ice on the delta. NB: The radii are _not_ rounded.
    sculpt_ice!(ice_free_radius, ice_free_radius + 10., Nwall, Ny½, hᵢmax, I⁺°₀, 𝕀⁺°, water_depth, ice_thickness)

end

## ======================================================================================================== melt_ice! ##
# Reduce the Ice Thickness in Each Grid Cell Due to Melting
"""
    melt_ice!(𝕀⁺°::Array{Float64,2}, params::Params, waters::Waters, atmos_melting_perΔt::𝔽;
              water_temperature::𝔽 = 2.)

Update the ice thickness across the simulation domain due to melting, and increment the
positive-degree-day index by one time step (in terms of days) if the ice-thickness is less
than the water depth in the grid cell (do not increment otherwise). The ice-melt could be
due to discharge and, depending on the coefficient, å, due to atmospheric heat. The amount
of ice-melt in one time step, Δt, is

``
Δhᵢ(x, y) = -(ρ_{w} c_{p} C_{s} |𝐮(x, y)| T(t) / (ρ_{i} L_{f}) + å hᵢ_{,max} / t_{melt}) Δt,
``

where ``ρ_{w}`` is the density of water, 1000 kg⋅m⁻³; ``c_{p}`` is the specific heat of
water, 4184 J⋅kg⁻¹⋅K⁻¹; ``C_{s}`` is the heat transfer coefficient of water, 0.0002;
``|𝐮(x, y)|`` is the flow speed at (x, y); ``T(t)`` is the temperature difference between
ice and the flowing water, which may vary with time (default here is 2.0); ``ρ_{i}`` is the
density of ice, 917 kg⋅m⁻³; ``L_{f}`` is the latent heat of fusion of water, 333 550 J⋅kg⁻¹;
``å`` is a coefficient scaling the contribution from atmospheric heating to the melting of
the ice; ``hᵢ_{,max}`` is the maximum ice thickness in the domain; and ``t_{melt}`` is the
total time for the thickest ice to melt due to atmospheric heating alone.

!!! note "Note on input arguments"
    The atmospheric melting term is entered as a single-value argument, as it does not tend
    to change from time step to time step. Water temperature is an optional keyword argument
    that could be used if water temperature varies with time.
"""
function melt_ice!(𝕀⁺°::Array{Float64,2}, params::Params, waters::Waters, atmos_melting_perΔt::𝔽;
                   water_temperature::𝔽 = 2.)
    #=
    Given:
        ρ_water =    1000           kg⋅m⁻³      Density of water
        ρ_ice   =     917           kg⋅m⁻³      Density of ice
        c_p     =    4184           J⋅kg⁻¹⋅K⁻¹  Specific heat of water
        C_s     =       2    × 10⁻⁴             Heat transfer coefficient of water
        L_f     =     333.55 × 10³  J⋅kg⁻¹      Latent heat of fusion of water

    We have:
        (ρ_water c_p C_s) / (ρ_ice L_f) = 2.7358443838113736e-6
    =#

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Δt, daysperΔt = params
    @unpack ice_thickness, water_depth, flow_speed = waters

    # For each element
    @inbounds @simd for lineari in eachindex(ice_thickness, flow_speed)

        ice_thickness[lineari] -= min(
                                      atmos_melting_perΔt +
                                        2.7358443838113736e-6 * (𝕀⁺°[lineari] > eps(0.0)) *
                                        water_temperature * Δt * flow_speed[lineari] * ρiρw,
                                      ice_thickness[lineari]
                                      )
        # Note: multipled by ρiρw because ice thickness is actually recorded as its water-depth equivalent

        # # Increment 𝕀⁺° by Δt in terms of days, if the ice is not bed-fast.
        𝕀⁺°[lineari] += ifelse(ice_thickness[lineari] < 0.9999 * water_depth[lineari], daysperΔt, 0.0)

    end # for lineari in eachindex(ice_thickness, flow_speed)

end

## ========================================================================================================== startxy ##
# Choose an Inlet-entry Cell at Random as the Starting Point of a Packet
"""
    px, py = startxy(params::Params)

Determine the inlet cell for a packet to start its journey. Returns x, y coordinates of the
starting point as integers.
"""
startxy(params::Params) = (1, rand(1:params.N₀) + params.inletsidewalls[1])
# `rand(1:N₀)` gives a y-coordinate offset value ℕ ∈ [1, N₀] from an inlet side-wall.
## ===================================================================================================== spawn_water! ##
# Spawn Water Packet
"""
    px, py = spawn_water!(tᵢ::Int, params::Params, waters::Waters, water_tracking::WaterTracking)

Generate (i.e., spawn) a new water packet at a randomly chosen inlet cell, reset the path-
history record to zeros, and and adjust the discharge fields accordingly. Return x, y
coordinates of the packet as integers.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `waters.unit_dischg_x_nxt`, `waters.unit_dischg_nxt`, and
`water_tracking.path_history` are modified in-place.
"""
function spawn_water!(tᵢ::Int, params::Params, waters::Waters, water_tracking::WaterTracking)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack path_history = water_tracking
    @unpack unit_dischg_x_nxt, unit_dischg_nxt = waters
    @unpack Qwp, δc = params

    # Determine the (inlet) cell for this packet to start its journey, and
    # record the initial position of the packet as Cartesian coordinates.
    path_history[1] = CartesianIndex(startxy(params))

    # Apportion the discharge of the newly spawned water packet.
    unit_dischg_x_nxt[path_history[1]] += 1. # and the y-component, unit_dischg_y_nxt[path_history[1]] += 0; unchanged.
    unit_dischg_nxt[path_history[1]]   += Qwp[tᵢ] / δc / 2 # Flux discretised by dividing between pixels.

    return Tuple(path_history[1]) # Convert the CartesianIndex to a Tuple for returning as px, py.

end

## ===================================================================================================== stepn_water! ##
# Onwards Journey of the Water Packet
"""
    px, py, isteps, looped = stepn_water!(
                                          next_step::Function,
                                          tᵢ::Int,
                                          packet_number::Int,
                                          px::Int, py::Int,
                                          params::Params,
                                          waters::Waters,
                                          water_tracking::WaterTracking,
                                          boundaries::Boundaries
                                          )

Continue taking subsequent (≥ 2) steps for the water packet, whose movement is limited to
`max_step` number of steps, then records the step in the path history, and adjust the
discharge fields accordingly. Boundary checks are also performed so that the water packet
ends its journey once it hits a boundary. Return the final x, y coordinates of the packet
as integers, the number of steps taken as an integer, and whether or not the path has ever
looped on itself (boolean).

The `next_step::Function` argument provides the method for directional guidance through a
weighted-random-walk process. This is because the methods differ between ice-free and with-
ice scenarios (they are [`nextstep_icefree`](@ref) and [`nextstep_withice`](@ref),
respectively). In the with-ice case, the `ice_thickness` argument should be an ice-thickness
array in the same format as `water_depth`; in the ice-free case, the `ice_thickness`
argument should contain [`nothing`](@ref).

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `px`, `py`, the fields `unit_dischg_x_nxt`, `unit_dischg_y_nxt`, and
`unit_dischg_nxt` of the struct `water`, and the fields `path_history` and `step_history` of
the struct `water_tracking` are modified in-place.

---
Requires the following global constants:
  * `Δᵢ`        Cellular distance vector between each of the adjacent cells.
  * `onestepx,
     onestepy`  The x and y component-pair representing one step in each direction.
... and all the global constants required by the functions called herein.
"""
function stepn_water!(
                      next_step::Function,
                      tᵢ::Int,
                      packet_number::Int,
                      px::Int, py::Int,
                      params::Params,
                      waters::Waters,
                      water_tracking::WaterTracking,
                      boundaries::Boundaries
                      )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny, δc, Nwall, Ny½, maxstep, Qwp, γ, θ = params
    @unpack surf_elev, ice_thickness, water_depth,
            dry_flag,
            unit_dischg_x, unit_dischg_y,
            unit_dischg_x_nxt, unit_dischg_y_nxt, unit_dischg_nxt = waters
    @unpack path_history, step_history = water_tracking
    @unpack wall_flag, ocean_flag = boundaries

    # Initialise the boolean variable that will indicated a water packet that looped on its own path.
    looped = false

    for ith_step in 2:maxstep

        # Determine the direction of the next step.
        # If the packet is not on the inlet edge:
        if px > 1

            # No padding required
            next_step_index = @views next_step(
                                               surf_elev[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               water_depth[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               ice_thickness[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               γ,
                                               θ.water,
                                               dry_flag[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               wall_flag[(px - 1):(px + 1), (py - 1):(py + 1)]
                                               )

        else # Otherwise, the packet is on the inlet edge:

            # Input here are padded with values outside the domain, i.e., "before" the inlet.
            next_step_index = @views next_step(
                                               [ [0. 0. 0.]; surf_elev[1:2, (py - 1):(py + 1)] ],
                                               [ [0. 0. 0.]; water_depth[1:2, (py - 1):(py + 1)] ],
                                               [ [0. 0. 0.]; ice_thickness[1:2, (py - 1):(py + 1)] ],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               γ,
                                               θ.water,
                                               [ trues(1, 3); dry_flag[1:2, (py - 1):(py + 1)] ],
                                               [ trues(1, 3); wall_flag[1:2, (py - 1):(py + 1)] ]
                                               )

        end # if px > 1; else ...

        # Register the direction index of the upcoming step (i.e., from the current cell to the next).
        step_history[ith_step - 1] = next_step_index

        # Register the position of the new step.
        px += onestepx[next_step_index]
        py += onestepy[next_step_index]

        # Record the new step in the history of this water packet.
        path_history[ith_step] = CartesianIndex((px, py))

        # Loop check:
        # If the water packet has revisited a cell along its path after (likely) leaving the inlet channel:
        if CartesianIndex(1, Ny½) ≢ path_history[ith_step] ∈ path_history[1:(ith_step - 1)] && ith_step > Nwall
        #= Note:
        Depending on the typical length of a packet's path (which depends on the size of the simulation domain), it
        might be more efficient to use `!allunique(path_history[1:ith_step])` as the condition. The “crossover” length
        may be in the range of high-thousands to ten thousand. Otherwise, using
            `path_history[ith_step] ∈ path_history[1:(ith_step - 1)]`
        will give faster performance.
        =#

            # Setting `looped` to true will prevent this water packet's path from participating in along-path surface
            # slope calculations, and thus will not contribute to the water surface elevation of this round of water.
            looped = true

            # @debug "Water packet number $packet_number has looped at $(path_history[ith_step]).";

            # “Teleport” the water packet five unit-vector steps away from the centre of the inlet channel.
            px, py = loop_disposal(px, py, Nx, Ny, Nwall, Ny½)
            #= Additional comment by N.H. Chan:
            It is unclear what is the rationale behind this procedure, especially the number of unit-vector steps, 5,
            which seems abitrary. The discharge contributions are retained, but the surface-elevation contributions are
            discarded. Perhaps this is more efficient than completely regenerating a new water packet every time a loop
            occurs, and perhaps this might prevent loops from recurring as much by retaining the discharge information?
            My own testing shows that, despite this, up to a few percent of water packets suffer from looping, and some
            can develop new loops tens of times. This procedure is performed identically in both the MATLAB and the
            Python version. An enquiry email to the author of the original MATLAB model remains unanswered as of this
            writing, which may be due to an outdated email address, although no server-generated “failed-to-deliver”
            messages have been received by me. (2020.05.06)
            =#

            # Replace the record of the current, “looped” step with the “teleported” step.
            path_history[ith_step] = CartesianIndex((px, py))

            # @debug "         It has now been teleported to $(path_history[ith_step]).
            # All-unique check of the path history, post-teleportation:" allunique(path_history[1:ith_step]);

        end # if

        # Apportion the discharge across the new step.
        unit_dischg_x_nxt[path_history[ith_step - 1]] += onestepx[next_step_index] / Δᵢ[next_step_index]
        unit_dischg_y_nxt[path_history[ith_step - 1]] += onestepy[next_step_index] / Δᵢ[next_step_index]
        unit_dischg_nxt[path_history[ith_step - 1]]   += Qwp[tᵢ] / δc / 2

        unit_dischg_x_nxt[path_history[ith_step]] += onestepx[next_step_index] / Δᵢ[next_step_index]
        unit_dischg_y_nxt[path_history[ith_step]] += onestepy[next_step_index] / Δᵢ[next_step_index]
        unit_dischg_nxt[path_history[ith_step]]   += Qwp[tᵢ] / δc / 2

        # Boundary check:
        # Either the packet is not at any boundaries, OR stop the packet's journey and return its current status.
        ~ocean_flag[path_history[ith_step]] && ~wall_flag[path_history[ith_step]] || return px, py, ith_step, looped

    end # for ith_step in 2:maxstep

    # When the packet reaches the maximum number of steps without hitting any boundaries, return the status with
    # `maxstep` in place of `ith_step`.
    return px, py, maxstep, looped

end

## ======================================================================================================= backwater! ##
# Sculpting the Slope of One Packet's Path
"""
    backwater!(
               backwater_helper::Function,
               tᵢ::Int,
               last_step::Int,
               params::Params,
               waters::Waters,
               water_tracking::WaterTracking,
               sea_surface_height::𝔽
               )

Retrace the path of a single water packet backwards towards the inlet. As soon as the
“shoreline” is located, starts adding surface elevation to each cell so that the surface
slope along the path (projected onto the stream lines) follow some backwater profile with
respect to the bed. The specific method for computing the backwater profile is supplied by
the `backwater_helper` function argument.

Input arguments `visit_count` and `cumul_surf_elev` are modified in-place.
"""
function backwater!(
                    backwater_helper::Function,
                    tᵢ::Int,
                    last_step::Int,
                    params::Params,
                    waters::Waters,
                    water_tracking::WaterTracking,
                    sea_surface_height::𝔽
                    )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack hΔ, uΔ = params
    @unpack bed_elev, ice_thickness, water_depth, flow_speed_x, flow_speed_y, flow_speed = waters
    @unpack path_history, step_history, visit_count, cumul_surf_elev = water_tracking

    # Locate the step immediately onshore, treated as shoreline cell and assigned the sea-surface height.
    shore_step = locate_shore(last_step, path_history, water_depth, flow_speed, hΔ, uΔ)::Int

    # Allocate a placeholder array to temporarily store the along-path surface elevation.
    surf_elev_new = Array{𝔽}(undef, shore_step)

    # The surface elevation of all cells down the packet's path from the shore towards the ocean should be the sea-
    # surface height. In practice, we only need to assign it to the `surf_elev_new` at position `shore_step`, as this
    # is the only value that will be used below.
    surf_elev_new[shore_step] = sea_surface_height

    # Note: If locate_shore(...) returns 1, the following function will return immediately.
    backwater_helper(
                     tᵢ,
                     shore_step,
                     params,
                     surf_elev_new, bed_elev, ice_thickness, water_depth,
                     flow_speed_x, flow_speed_y, flow_speed,
                     path_history, step_history, visit_count, cumul_surf_elev
                     )

end

## ------------------------------------------------------------------------------------- Alias: backwater_constslope! ##
"""
    backwater_constslope!(tᵢ, last_step, params, waters, water_tracking, sea_surface_height)

Alias of the [`backwater!`](@ref) function. Calculate the backwater profile of an individual
water packet by giving its path a constant surface slope, S₀, from shoreline upstream. Under
Arctic conditions, `waters` has the type `::Waters{<:Number,<:Number}` (i.e., the field
`waters.ice_thickness` contains numeric-typed values instead of `nothing`). In that case,
the surface at cells with ice cover are kept at their existing values, effectively pausing
the along-path rising slope.

---
Requires the following global constants:
  * `Δᵢ`    Cellular distance vector between each of the adjacent cells.
  * `𝐝ᵢ`    Cellular direction (unit) vector from the central cell towards each neighbour.
"""
backwater_constslope!(tᵢ, last_step, params, waters, water_tracking, sea_surface_height) =
    backwater!(tᵢ, last_step, params, waters, water_tracking, sea_surface_height) do tᵢ, shore_step, params,
        surf_elev_new, bed_elev, ice_thickness, water_depth, flow_speed_x, flow_speed_y, flow_speed,
        path_history, step_history, visit_count, cumul_surf_elev

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc, S₀ = params

    # From the shoreline step, tracing backwards towards the inlet:
    for ith_step in (shore_step - 1):-1:1

        # If there is ice cover:
        if nothingmeanszero(ice_thickness[ path_history[ith_step] ]) ≢ 0.0

            # Keep the original surface elevation
            surf_elev_new[ith_step] = bed_elev[ path_history[ith_step] ] + water_depth[ path_history[ith_step] ]

        elseif flow_speed[ path_history[ith_step] ] > 𝟘⁻ # OR: if the local flow speed is not zero (beyond noise):

            # Add to the surface height such that the along-path slope (for this packet) is S₀ towards the inlet.
            surf_elev_new[ith_step] = surf_elev_new[ith_step + 1] + Δᵢ[ step_history[ith_step] ] * δc * S₀[tᵢ] *
                                       ( (flow_speed_x[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][1] +
                                          flow_speed_y[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][2]) /
                                        flow_speed[ path_history[ith_step] ] )
            # In the above, the discretised step-wise directions are projected onto the smoother flow field-lines.

        else # Otherwise:

            # No rise in surface elevation.
            surf_elev_new[ith_step] = surf_elev_new[ith_step + 1]

        end # if flow_speed[ path_history[ith_step] ] > 0.; else ...

        # Record the values to be used to average over multiple packets later.
        visit_count[ path_history[ith_step] ] += 1
        cumul_surf_elev[ path_history[ith_step] ] += surf_elev_new[ith_step]

    end # for ith_step in (shore_step - 1):-1:1

end

## ------------------------------------------------------------------------------------- Alias: backwater_gvfmanning! ##
@doc raw"""
    backwater_gvfmanning!(_, last_step, params, waters, water_tracking, sea_surface_height)

Alias of the [`backwater!`](@ref) function. Calculate the backwater profile of an individual
water packet, from the shoreline or (land-fast) ice-cover limit upstream, by using the
governing equation of gradually varied flow (GVF) in an open channel:
```math
    \frac{dy}{dx} = \frac{S₀ - S𝔣}{1 - Fr^2} \ ,
```
where ``Fr`` is the Froude number; or, equivalently,
```math
    \frac{d}{dx} ( y + \frac{u^2}{2g} ) = S₀ - S𝔣 \ ,
```
where ``y`` is the water depth, ``u`` is the flow speed, ``g`` is the gravitational
acceleration, ``S₀`` is the channel slope, and ``S𝔣`` is the friction slope, which is
approximated by the [Manning formula](https://en.wikipedia.org/wiki/Manning_formula):
```math
    S𝔣 ≈ \frac{n^2 u^2}{R^{4/3}} \ ,
```
where ``n`` is the Manning coefficient (giving the amount of friction aginst flow), and
``R`` is the hydraulic radius. In the case where the channel is much wider than its flow
depth, ``h``, ``R ≈ h``.

---
Requires the following global constants:
  * `ℊ`            The gravitational acceleration on the Earth's surface.
  * `Δᵢ`            Cellular distance vector between each of the adjacent cells.
  * `𝐝ᵢ`            Cellular direction (unit) vector from the central cell towards each neighbour.
"""
backwater_gvfmanning!(_, last_step, params, waters, water_tracking, sea_surface_height) =
    backwater!(1, last_step, params, waters, water_tracking, sea_surface_height) do _, shore_step, params,
        surf_elev_new, bed_elev, ice_thickness, water_depth, flow_speed_x, flow_speed_y, flow_speed,
        path_history, step_history, visit_count, cumul_surf_elev

    # ❓ The mathematical explanation is at the end of the function; equation numbers in the comments refer to there.

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc, manningscoef² = params

    # From the shoreline step, tracing backwards towards the inlet:
    for ith_step in (shore_step - 1):-1:1

        # If there is constriction from ice cover (assumed to be land-fast):
        if nothingmeanszero(ice_thickness[ path_history[ith_step] ]) ≢ 0.0

            # No modifications to the water surface (including any ice) since the flow is constricted by land-fast ice.
            surf_elev_new[ith_step] = bed_elev[ith_step] + water_depth[ith_step]
            # Reminder: `water_depth` in this code *includes* any ice thickness.

        elseif flow_speed[ path_history[ith_step] ] > 𝟘⁻ # OR, if the local flow speed is not zero (beyond noise):

            # Surface elevation = bed elevation + water depth, where water depth is 𝑦ᵢ in equation (5) below.
            surf_elev_new[ith_step] = bed_elev[ path_history[ith_step] ] +
                                      # ⇃ water depth (under the ice, if any) at path_history[ith_step + 1] ⇂
                                      (
                                       surf_elev_new[ith_step + 1] -
                                       nothingmeanszero(ice_thickness[ path_history[ith_step + 1] ]) -
                                       bed_elev[ path_history[ith_step + 1] ]
                                      ) +
                                      (
                                       # ⇃ (𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ ⇂
                                       inv(2ℊ) *
                                       (
                                        abs2(flow_speed[ path_history[ith_step + 1] ]) -
                                        abs2(flow_speed[ path_history[ith_step] ])
                                       ) +
                                       # ⇃ (𝑧ᵢ - 𝑧ᵢ₊₁) ⇂
                                       (bed_elev[ path_history[ith_step + 1] ] - bed_elev[ path_history[ith_step] ]) +
                                       # ⇃ ½(𝑆𝔣,ᵢ₊₁ + 𝑆𝔣,ᵢ) Δ𝑥 ⇂
                                       0.5 * manningscoef² *
                                       (
                                        abs2(flow_speed[ path_history[ith_step + 1] ]) /
                                          water_depth[ path_history[ith_step + 1] ]^(4/3) +
                                        abs2(flow_speed[ path_history[ith_step] ]) /
                                          water_depth[ path_history[ith_step] ]^(4/3)
                                       ) * δc * Δᵢ[ step_history[ith_step] ]
                                      ) *
                                      # ⇃ Projection onto flow-field streamline ⇂
                                      ( (flow_speed_x[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][1] +
                                         flow_speed_y[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][2]) /
                                       flow_speed[ path_history[ith_step] ] )

        else # Otherwise, the water depth is kept the same in the no-flow cell as an approximation:

            # Surface elevation = bed elevation + water depth, where water depth is 𝑦ᵢ in equation (5) below.
            surf_elev_new[ith_step] = bed_elev[ path_history[ith_step] ] +
                                      # ⇃ water depth (under the ice, if any) at path_history[ith_step + 1] ⇂
                                      (
                                       surf_elev_new[ith_step + 1] -
                                       nothingmeanszero(ice_thickness[ path_history[ith_step + 1] ]) -
                                       bed_elev[ path_history[ith_step + 1] ]
                                      )

        end # if nothingmeanszero(ice_thickness[ path_history[ith_step] ]) ≢ 0.0; elseif ...; else ...

        # Record the values to be used to average over multiple packets later.
        visit_count[ path_history[ith_step] ] += 1
        cumul_surf_elev[ path_history[ith_step] ] += surf_elev_new[ith_step]

    end # for ith_step in (shore_step - 1):-1:1

    #= Mathematical Explanation:
    The governing equation of gradually varied flow (GVF) is

        𝑑𝑦/𝑑𝑥 = (𝑆₀ - 𝑆𝔣) / (1 - 𝐹𝑟²), ..........................................(1)

    where 𝑑𝑦/𝑑𝑥 is the change in water depth towards downstream, 𝑆₀ ≡ −𝑑𝑧/𝑑𝑥 is
    the bottom slope (𝑧 is the bottom elevation), and 𝑆𝔣 is the slope of the energy
    grade line (sometimes referred to as “friction slope”), and 𝐹𝑟 ≡ 𝑢 / √(ℊh) is
    the Froude number (𝑢 is the flow speed, ℊ is the gravitational acceleration, h
    is the flow depth). 𝑆𝔣 is often approximated by Manning's equation, giving

        𝑆𝔣 ≈ 𝑛²𝑢² / 𝑅^(4/3), ....................................................(2)

    where 𝑛 is the empirical Manning's (friction) coefficient, 𝑢 is the flow speed,
    and 𝑅 ≡ 𝐴 / 𝑃 is the hydraulic radius (𝐴 is the cross-sectional area of the flow;
    and 𝑃 is the “wetted perimeter”, i.e., the length of the bottom and sides that
    are in contact with the water, perpendicular to the flow). In channels that are
    much wider than they are deep, 𝑅 ≈ h, where h is the water depth.

    The governing equation above is equivalent to the energy equation,

        𝑑/𝑑𝑥 (𝑦 + 𝑢² / 2ℊ) = 𝑆₀ - 𝑆𝔣, ...........................................(3)

    which can be manipulated into equation (1) above by writing 𝑢 in terms of the
    discharge, 𝑄, and flow cross-sectional area, 𝐴, and applying the chain rule of
    differentiation.

    Taking a discretised approach, 𝑑𝑦/𝑑𝑥 = Δ𝑦 / Δ𝑥. Equation (3) can be written as

        Δ(𝑦 + 𝑢² / 2ℊ) = (𝑆₀ - 𝑆𝔣) Δ𝑥 ⟹ Δ𝑦 = -(𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ + (𝑆₀ - ⟨𝑆𝔣⟩) Δ𝑥 ,

    where ⟨⟩ denotes the average across discrete steps. Now, in the upstream
    direction, Δ ⟶ -Δ, giving

        𝑦ᵢ - 𝑦ᵢ₊₁ = (𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ - (𝑆₀ - ⟨𝑆𝔣⟩) (𝑥ᵢ - 𝑥ᵢ₊₁). ...............(4)

    Recall that 𝑆₀ ≡ −𝑑𝑧/𝑑𝑥 = -Δz / Δx = (𝑧ᵢ - 𝑧ᵢ₊₁) / (𝑥ᵢ₊₁ - 𝑥ᵢ), we now have

        𝑦ᵢ - 𝑦ᵢ₊₁ = (𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ + (𝑧ᵢ - 𝑧ᵢ₊₁) - ⟨𝑆𝔣⟩ (𝑥ᵢ - 𝑥ᵢ₊₁)

    ⟹ 𝑦ᵢ = 𝑦ᵢ₊₁ + (𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ + (𝑧ᵢ - 𝑧ᵢ₊₁) - ½(𝑆𝔣,ᵢ₊₁ + 𝑆𝔣,ᵢ) (𝑥ᵢ - 𝑥ᵢ₊₁)

    ⟹ 𝑦ᵢ = 𝑦ᵢ₊₁ + (𝑢²ᵢ₊₁ - 𝑢²ᵢ) / 2ℊ + (𝑧ᵢ - 𝑧ᵢ₊₁) + ½(𝑆𝔣,ᵢ₊₁ + 𝑆𝔣,ᵢ) Δ𝑥 .......(5)

    Recall, also, from equation (2), that 𝑆𝔣 ≈ 𝑛²𝑢² / 𝑅^(4/3) ≈ 𝑛²𝑢² / h^(4/3).
    Therefore, the last term of equation (5) can be discretised as

        ½(𝑆𝔣,ᵢ₊₁ + 𝑆𝔣,ᵢ) Δ𝑥 ≈ ½𝑛²(𝑢²ᵢ₊₁ / hᵢ₊₁^(4/3) + 𝑢²ᵢ / hᵢ^(4/3)) Δ𝑥 .
    =#

end

## ------------------------------------------------------------------------------------- Alias: backwater_gvfflowrcm! ##
@doc raw"""
    backwater_gvfflowrcm!(_, last_step, params, waters, water_tracking, sea_surface_height)

Alias of the [`backwater!`](@ref) function. Calculate the backwater profile of an individual
water packet, from the shoreline or (land-fast) ice-cover limit upstream, by adopting the
approach taken by Liang et al. (2015; Part II) in their Flow RCM model (which is Delta RCM
modified to demonstrate flow calculations). There, for Froude number (``Fr ≡ 𝑢² / gh``,
where ``u`` is the flow speed, ``g`` is the gravitational acceleration, and ``h`` is the
water depth) ``Fr ≤ 0.5``, they use the governing equation of gradually varied flow (GVF)
in an open channel:
```math
    \frac{dh}{dx} = \frac{S₀ - S𝔣}{1 - Fr^2} \ ,
```
where , ``S₀`` is the channel slope, and
``S𝔣`` is the friction slope:
```math
    S𝔣 ≈ C𝔣 ⋅ Fr^2 \ ,
```
where ``C𝔣`` is a “coefficient of friction” (which took on values of 0.1 or 0.01 in
Liang et al., 2015). For ``Fr > 0.5``, they directly prescribe the water-surface slope to be
the friction slope, ``S𝔣``.

---
REFERENCE:
  - Liang, M., Geleynse, N., Edmonds, D. A., and Passalacqua, P. 2015, Earth Surface Dynamics, 3, 87–104.
        doi:10.5194/esurf-3-87-2015

---
Requires the following global constants:
  * `ℊ`    The gravitational acceleration on the Earth's surface.
  * `Δᵢ`    Cellular distance vector between each of the adjacent cells.
  * `𝐝ᵢ`    Cellular direction (unit) vector from the central cell towards each neighbour.
"""
backwater_gvfflowrcm!(_, last_step, params, waters, water_tracking, sea_surface_height) =
    backwater!(1, last_step, params, waters, water_tracking, sea_surface_height) do _, shore_step, params,
        surf_elev_new, bed_elev, ice_thickness, water_depth, flow_speed_x, flow_speed_y, flow_speed,
        path_history, step_history, visit_count, cumul_surf_elev

    # ❓ The mathematical explanation is at the end of the function.

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc, 𝐶𝔣 = params

    # From the shoreline step, tracing backwards towards the inlet:
    for ith_step in (shore_step - 1):-1:1

        # Froude number squared: 𝐹𝑟² ≡ 𝑢² / ℊh, where h is the water depth.
        𝐹𝑟² = abs2(flow_speed[ path_history[ith_step] ]) * inv(ℊ * water_depth[ path_history[ith_step] ])

        # If there is constriction from ice cover (assumed to be land-fast):
        if nothingmeanszero(ice_thickness[ path_history[ith_step] ]) ≢ 0.0

            # No modifications to the water surface (including any ice) since the flow is constricted by land-fast ice.
            surf_elev_new[ith_step] = bed_elev[ith_step] + water_depth[ith_step]
            # Reminder: `water_depth` in this code *includes* any ice thickness.

        elseif 𝐹𝑟² > 0.25 && flow_speed[ path_history[ith_step] ] > 𝟘⁻

            # Prescribe surface elevation to be equal to the friction slope, 𝑆𝔣.
            surf_elev_new[ith_step] = surf_elev_new[ith_step + 1] +
                                      # ⇃ 𝑆𝔣 ⋅ -Δ𝑥 ; -Δ𝑥 because we are calculating upstream ⇂
                                      𝐶𝔣 * 𝐹𝑟² * δc * Δᵢ[ step_history[ith_step] ] *
                                      # ⇃ Projection onto flow-field streamline ⇂
                                      ( (flow_speed_x[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][1] +
                                         flow_speed_y[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][2]) /
                                       flow_speed[ path_history[ith_step] ] )

        elseif flow_speed[ path_history[ith_step] ] > 𝟘⁻ # OR, if the local flow speed is not zero (beyond noise):

            # Surface elevation = bed elevation + water depth, where water depth is the changing quantity to compute.
            surf_elev_new[ith_step] = bed_elev[ path_history[ith_step] ] +
                                      # ⇃ water depth at path_history[ith_step + 1], the step immediately downstream ⇂
                                      (
                                       surf_elev_new[ith_step + 1] -
                                       nothingmeanszero(ice_thickness[ path_history[ith_step + 1] ]) -
                                       bed_elev[ path_history[ith_step + 1] ]
                                      ) +
                                      # ⇃ 1 / (1 - 𝐹𝑟²) ⇂
                                      inv(1 - 𝐹𝑟²) *
                                      (
                                       # ⇃ (𝑧ᵢ - 𝑧ᵢ₊₁) = 𝑆₀ ⋅ -Δ𝑥 ; -Δ𝑥 because we are calculating upstream ⇂
                                       (bed_elev[ path_history[ith_step + 1] ] - bed_elev[ path_history[ith_step] ]) +
                                       # ⇃ 𝑆𝔣 ⋅ -Δ𝑥 ; -Δ𝑥 because we are calculating upstream ⇂
                                       𝐶𝔣 * 𝐹𝑟² * δc * Δᵢ[ step_history[ith_step] ]
                                      ) *
                                      # ⇃ Projection onto flow-field streamline ⇂
                                      ( (flow_speed_x[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][1] +
                                         flow_speed_y[ path_history[ith_step] ] * 𝐝ᵢ[ step_history[ith_step] ][2]) /
                                       flow_speed[ path_history[ith_step] ] )

        else # Otherwise, the water depth is kept the same in the no-flow cell as an approximation:

            # Surface elevation = bed elevation + water depth, where water depth is 𝑦ᵢ in equation (5) below.
            surf_elev_new[ith_step] = bed_elev[ path_history[ith_step] ] +
                                      # ⇃ water depth (under the ice, if any) at path_history[ith_step + 1] ⇂
                                      (
                                       surf_elev_new[ith_step + 1] -
                                       nothingmeanszero(ice_thickness[ path_history[ith_step + 1] ]) -
                                       bed_elev[ path_history[ith_step + 1] ]
                                      )

        end # if nothingmeanszero(ice_thickness[ path_history[ith_step] ]) ≢ 0.0; elseif ...; elseif ...; else ...

        # Record the values to be used to average over multiple packets later.
        visit_count[ path_history[ith_step] ] += 1
        cumul_surf_elev[ path_history[ith_step] ] += surf_elev_new[ith_step]

    end # for ith_step in (shore_step - 1):-1:1

    #= Mathematical Explanation:
    See the comment in the source code of `backwater_gvfmanning`. The governing equation is identical, but
    there are a few key difference here:

    (1) Liang et al., 2015 (Part II) uses 𝑆𝔣 ≡ 𝐶𝔣 𝐹𝑟², for the friction slope, where 𝐶𝔣 is some friction
        coefficient. Two values of 𝐶𝔣 had been used in that article, 0.01 and 0.1, but the reason is unclear.

    (2) For 𝐹𝑟 ≳ 0.5 (or 0.7, see item 4 below), Liang et al., 2015 (Part II) prescribe a surface slope
        that is equal to 𝑆𝔣, the friction slope.

    (3) In the “commented-out” lines of the MATLAB source code of Delta RCM (lines 511-521, as retrieved from
        the CSDMS Model Repository: https://csdms.colorado.edu in July, 2020), what appears to be the remnant
        of the backwater-profile calculation is still visible. However, it does not appear to apply a finite-
        difference treatment to all variables involved, instead using only local (i.e., current-pixel) values
        for flow speed, water depth, and thus 𝐹𝑟 and 𝑆𝔣 too.

    (4) The condition for 𝐹𝑟, above which the surface slope is simply prescribed as 𝑆𝔣, is also left at 0.7,
        instead of 0.5 in the published paper (probably as a result of further testing/usage by the person who
        uploaded the code). The value of 0.5 is adopted above, following the original article.
    =#

end

## ==================================================================================================== full_surface! ##
# Combine the Surface Elevations of Individual Water-Packet Paths and Smooth Out the Sharp Bits and Abrupt Changes
"""
    full_surface!(
                  is_first_time::Bool,
                  params::Params,
                  waters::Waters,
                  water_tracking::WaterTracking,
                  sea_surface_height::𝔽,
                  boundaries::Boundaries
                  )

Construct the updated surface elevation by: (i) combining and averaging the individual
elevations sculpted by individual water packets; (ii) smooth out any roughness by applying
a diffusive filter; and (iii) damp the changes in time by including a fraction of the
previous surface-elevation state.

At the end of this function, the fields `visit_count` and `cumul_surf_elev` in the struct
`water_tracking` are reset to zero.

Input arguments `waters.surf_elev`, and the fields `visit_count` and `cumul_surf_elev` in
the struct `water_tracking` are modified in-place.

!!! warning "Assumption of boundary padding"
    This code assumes that at least 1 cell on the “ocean” edges of the domain is classified
    as boundary cell, so that there is always a 1-cell padding. This code will need to be
    modified if boundary flags are false on the “ocean” sides of the simulation domain.

---
Requires the following global constants:
  * `kernel_addneighbours`
                    When convolved with, each cell will contain the sum of its 8 neighbours.
"""
function full_surface!(
                       is_first_time::Bool,
                       params::Params,
                       waters::Waters,
                       water_tracking::WaterTracking,
                       sea_surface_height::𝔽,
                       boundaries::Boundaries
                       )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack inletsides, ϵ, ϖ = params
    @unpack surf_elev, bed_elev = waters
    @unpack visit_count, cumul_surf_elev = water_tracking
    @unpack wall_flag, ocean_flag = boundaries

    #===== First, merge all the surface-elevation profiles from individual water packets =====#

    surf_elev_temp       = similar(surf_elev) # Placeholder for the combined surface elevation
    surf_elev_smoothed   = similar(surf_elev) # Placeholder for the smoothed surface elevation

    # Assign the surface elevation that was initialised at the beginning or updated at the end of previous timestep,
    # sea level, or the surface sculpted from the water packets' paths.
    @inbounds @simd for lineari in eachindex(visit_count, surf_elev_temp)

        # If no packets visited, assign latest surface, which has not changed since the end of the previous time step
        # (if any), with the minimum at sea-surface height. (Tip: Even though there will be a shoreline/bank migration
        # that will update the surface elevation in isolated spots, this function contains the last full-field update
        # of the surface elevation until the next time step.)
        if visit_count[lineari] ≡ 0

            surf_elev_temp[lineari] = max(surf_elev[lineari], sea_surface_height)

        # Otherwise, take the average of all surface elevations calculated from all packets' paths.
        else

            surf_elev_temp[lineari] = cumul_surf_elev[lineari] / visit_count[lineari]

            # Zero the visit counter and cumulative surface-elevation placeholder for the next iteration or time step.
            visit_count[lineari] = 0
            cumul_surf_elev[lineari] = 0.0

        end # if visit_count[lineari] ≡ 0; else ...

    end # for lineari in eachindex(visit_count, surf_elev_temp)

    #===== Now, it's time to smooth the surface =====#


    # Applying the filter, `kernel_addneighbours`, by performing a convolution with `surf_elev_temp` (which now holds
    # the newly combined and averaged surface-elevation grid), we get a grid containing the sum of each cell's eight
    # neighbours.
    surf_elev_neighbours = convolve(surf_elev_temp, kernel_addneighbours)::Array{𝔽,2}

    # Along the inlet wall boundary, the wall cells are to be excluded from the averaging. The wall cells have already
    # been imposed a value of 0.0 in the surface-elevation definition. Therefore, they do not contribute to the
    # cumulative sum from the convolution in the previous line of code. However, we still have to make sure that the
    # cumulative sum is divided by the correct number (i.e., without the excluded cells being counted) to get an
    # accurate average. This can be done by convolving the same filter and the wall-flag boolean array. Each `true`
    # value is counted as 1, and each `false` 0.
    neighbours_excluded = convolve(wall_flag, kernel_addneighbours)::Array{Int,2}

    # At the N₀ inlet cells, the hypothetical “out-of-bounds” cells are also to be excluded. There are 3 of these
    # hypothetical cells to every cell along the edge of the grid.
    @. neighbours_excluded[1, inletsides[1]:inletsides[2]] += 3
    #= NB:
    We are not dealing with the other edges because they are expected to be covered by other boundary flags, which
    means that they are not included in the averaging any way. The above procedure will need to be modified if we
    change the way “boundary cells” are treated in the smoothing.
    =#

    @inbounds @simd for lineari in eachindex(ocean_flag, surf_elev_smoothed, neighbours_excluded)

        # If the cell does not lie on any boundaries:
        if ~ocean_flag[lineari] && ~wall_flag[lineari]

            # Apply diffusive smoothing.
            surf_elev_smoothed[lineari] = underrelax(
                                                     surf_elev_neighbours[lineari] / (8 - neighbours_excluded[lineari]),
                                                     surf_elev_temp[lineari],
                                                     ϵ
                                                     )
            # Recall: (8 - neighbours_excluded[...]) gives the correct number of (non-wall and non-“out-of-bounds”)
            #         neighbours included in the averaging.

        else # Otherwise:

            # Assign the original value.
            surf_elev_smoothed[lineari] = surf_elev_temp[lineari]

        end # if ~ocean_flag[lineari] && ~wall_flag[lineari]; else ...

        # ----- Last but not least, damp out the changes in time ----- #
        # If we are beyond the first time step (which is most of the time):
        if !is_first_time

            # Damp the surface-elevation changes by including a fraction of the previous surface elevation.
            surf_elev[lineari] = underrelax(surf_elev_smoothed[lineari], surf_elev[lineari], ϖ)

        else # Otherwise:

            # Transfer the values in `surf_elev_smoothed` directly to `surf_elev`.
            surf_elev[lineari] = surf_elev_smoothed[lineari]

        end # if !is_first_time; else ...

    end # for lineari in eachindex(ocean_flag, surf_elev_smoothed, neighbours_excluded)

end

## ========================================================================================== migrate_shore_bank_HOF! ##
# Find Any Dry Cells with Taller Water Surface around and Flood Them Accordingly (i.e., Shoreline/Bank Migration)
"""
    migrate_shore_bank!(params::Params, waters::Waters)

Locate any cells currently classified as “dry” and check if it has neighbouring cells with
higher water-surface elevation (henceforth “eligible cells”) than its bed elevation. If so,
take the average water-surface elevation across the eligible cells, and assign it to the
cell in question. The cell is now no longer dry. This can be thought of as shoreline or
bank migration.

Input arguments `surf_elev`, `dry_flag`, and `water_depth` in the struct `waters` are
modified in-place.

!!! warning "Assumption of boundary padding"
    This code assumes that at least 1 cell on the “ocean” edges of the domain is classified
    as boundary cell, so that there is always a 1-cell padding. This code will need to be
    modified if boundary flags are false on the “ocean” side-edges of the simulation domain.
"""
function migrate_shore_bank!(params::Params, waters::Waters)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny, Nwall, hdry = params
    @unpack surf_elev, bed_elev, ice_thickness, water_depth, dry_flag = waters

    # Making use of Cartesian coordinates here; `cartesiani` and `neighbour_cartesiani` below will take on values of
    # `CartesianIndex(x::Int, y::Int)`, from the top-left of the grid in column-major order to the bottom-right.

    # Go through the entire domain except the inlet wall (which here includes the inlet) and the three “ocean” edges:
    @inbounds for cartesiani in @view CartesianIndices(dry_flag)[(Nwall + 1):(Nx - 1), 2:(Ny - 1)]

        # If the cell is dry:
        if dry_flag[cartesiani]

            # If we are still here, that means the cell is dry. Prepare to check if we need to flood it. Initialise:
            # • Sum of surface elevations of any neighbouring cells with higher water surface
            neighbours_surf_elev = 0.0
            # • Number of cells with their surface elevations added to `neighbours_surf_elev`
            neighbours_surf_elev_count = 0

            # Go through each of the neighbours in a 3-by-3 cell area centred on the current cell——the one with
            # coordinates given by `cartesiani`:
            @simd for neighbour_cartesiani in (cartesiani - CartesianIndex(1, 1)):(cartesiani + CartesianIndex(1, 1))

                # If this neighbour cell is _not_ dry (recall that the central cell at `cartesiani` is also dry), AND
                # if this neighbour's (liquid) water surface is higher than the bed of the current cell, then
                # add this “eligible” neighbour's surface elevation to `neighbours_surf_elev`, and
                # add 1 to `neighbours_surf_elev_count`; this is for averaging later.
                # Otherwise, do nothing.
                if ~dry_flag[neighbour_cartesiani] &&
                    surf_elev[neighbour_cartesiani] - nothingmeanszero(ice_thickness[neighbour_cartesiani]) >
                        bed_elev[cartesiani]

                    neighbours_surf_elev += surf_elev[neighbour_cartesiani]
                    neighbours_surf_elev_count += 1

                end # if

            end # for neighbour_cartesiani in (cartesiani - CartesianIndex(1, 1)):(cartesiani + CartesianIndex(1, 1))

            # If there are eligible cells around:
            if neighbours_surf_elev_count > 0

                # Assign their average surface elevation to the current cell, and set as not dry()*).
                surf_elev[cartesiani] = neighbours_surf_elev / neighbours_surf_elev_count

            end # if

        end # if dry_flag[cartesiani]

        # This helper function will update the water depth and, accordingly, the dry flag.
        update_water_depth_dry_flag!(cartesiani, hdry, surf_elev, bed_elev, water_depth, dry_flag)

    end # for cartesiani in @view CartesianIndices(dry_flag)[(Nwall + 1):(end - 1), 2:(end - 1)]

end

## ============================================================================================== update_after_water! ##
# Update All of the Water-depth, Unit-discharge, and Flow-speed Fields after All `nw` Water Packets Have Been Released
"""
    update_after_water!(is_first_time::Bool, iter_number::Int, params::Params, waters::Waters)

Perform updates to the following fields: water depth, unit discharge (components and
magnitude) “proper”, unit discharge (components and magnitude) “next” (i.e., temporary
holding place for the latest updates, which can then be used together with the “proper”
fields for a damped/underrelaxed change in time or between iterations), and flow speed
(componenets and magnitude). Whether or not `waters.ice_thickness` is empty (i.e., contains
[`nothing`](@ref)) will trigger flow-speed update conditions for ice-free or with-ice
scenarios.

At the end of this function, the “next” fields are reset to zero to prepare for the next
iteration or time step.

Input arguments `unit_dischg_x`, `unit_dischg_y`, `unit_dischg`, `unit_dischg_x_nxt`,
`unit_dischg_y_nxt`, `unit_dischg_next`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`
in the struct `waters` are modified in-place.
"""
function update_after_water!(is_first_time::Bool, iter_number::Int, params::Params, waters::Waters)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack inletsides, uₘₐₓ, ω = params
    @unpack surf_elev, bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg,
            unit_dischg_x_nxt, unit_dischg_y_nxt, unit_dischg_nxt = waters

    #=
    Prepare the “update-fraction” coefficient for the underrelaxation technique; the coefficient is the fraction of
    freshly calculated value to include in the update (the rest being contributed by the existing/previous value).
    To choose the coefficient:
    Is this the first time step? Then there are no previous values to damp the update: coefficient = 1
    Otherwise, is this the first iteration? Depending on yes or no, assign different values to the coefficient. =#
    is_first_time ? coef = 1.0 : (iter_number ≡ 1.0 ? coef = ω.iterfirst : coef = ω.itersubsq)

    # We loop through the grid only once, performing all the necessary updates together, making use of optimal indexing.
    @inbounds @simd for lineari in eachindex(water_depth, dry_flag)

        # Decompose the unit-discharge value into x and y components:
        # First, calculate the norm of the directional vector.
        unit_dischg_nxt_norm = √(abs2(unit_dischg_x_nxt[lineari]) + abs2(unit_dischg_y_nxt[lineari]))
        # Then, if `unit_dischg_nxt_norm` is greater than zero:
        if unit_dischg_nxt_norm > 0.0
            # (x-, y- components) = (magnitude) × (x-, y- components) / (norm of the directional vector).
            unit_dischg_x_nxt[lineari] *= unit_dischg_nxt[lineari] / unit_dischg_nxt_norm
            unit_dischg_y_nxt[lineari] *= unit_dischg_nxt[lineari] / unit_dischg_nxt_norm
            # Note: The ratio is kept explicit (intead of merging into the line before to save time) for readability.
        end # if

        # Update the unit-discharge fields with the underrelaxation technique.
        unit_dischg_x[lineari] = underrelax(unit_dischg_x_nxt[lineari], unit_dischg_x[lineari], coef)
        unit_dischg_y[lineari] = underrelax(unit_dischg_y_nxt[lineari], unit_dischg_y[lineari], coef)
        unit_dischg[lineari] = √( abs2(unit_dischg_x[lineari]) + abs2(unit_dischg_y[lineari]) )

        # Update the flow-velocity fields, which are calculated from the unit-discharge fields.
        # If the water depth is too shallow OR water discharge is zero:
        if dry_flag[lineari] || unit_dischg[lineari] ≡ 0.0

            flow_speed[lineari] = flow_speed_x[lineari] = flow_speed_y[lineari] = 0.0 # No flow

        else # Otherwise:

            # Flow-speed update.
            update_flow_velocity!(
                                  lineari,
                                  uₘₐₓ,
                                  ice_thickness,
                                  water_depth,
                                  flow_speed_x, flow_speed_y, flow_speed,
                                  unit_dischg_x, unit_dischg_y, unit_dischg
                                  )
            #= NB (for simulations with ice):
            During flow iterations, because of updates to the surface elevation, the water depth may become shallower
            than the ice thickness. This not only violates the requirement that ice thickness cannot exceed 99% of the
            water depth, but will also cause negative flow speed (magnitude) in the `update_flow_velocity!` call above.
            Therefore, a clamping operation is added to the variant of `update_flow_velocity` that takes ice into
            account. Specifically, flow speed must remain between zero and `uₘₐₓ`. (The decision here is to leave ice
            thickness untouched to avoid “artificial” melting.)
            =#

        end # if dry_flag[lineari] || unit_dischg[lineari] < 0.0; else ...

        # Zero the “next” fields for use in the next iteration or time step.
        unit_dischg_x_nxt[lineari] = unit_dischg_y_nxt[lineari] = unit_dischg_nxt[lineari] = 0.0

    end # for lineari in eachindex(water_depth, dry_flag)

end

## ====================================================================================================== spawn_sand! ##
# Spawn a Packet of Sand Sediment
"""
    px, py, Vsp = spawn_sand!(tᵢ::Int, params::Params, sediment_transport::SedimentTransport)

Generate (i.e., spawn) a new “sand” sediment packet at a randomly chosen inlet cell, and
adjust the sand-flux field accordingly. Returns x, y coordinates as integers and the volume
of the spawned packet as float.

tᵢ is the current time-step number.

Input argument `sediment_transport.sand_flux` is modified in-place.
"""
function spawn_sand!(tᵢ::Int, params::Params, sediment_transport::SedimentTransport) # This method is for a “sand” packet

    # Unpack the necessary field of the input structs so that their call-names are available individually.
    @unpack δc, Vsp, Δt = params
    @unpack sand_flux = sediment_transport # 👈 Could have used the “dot” notation, but for uniformity's sake...

    # Determine the (inlet) cell for this packet to start its journey.
    px::Int, py::Int = startxy(params)

    # Apportion the flux of the newly spawned sand packet.
    sand_flux[px, py] += Vsp[tᵢ] / Δt / δc / 2 # Flux quantity discretised by dividing between pixels.

    return px, py, Vsp[tᵢ]

end

## ====================================================================================================== stepn_sand! ##
# Onwards Journey of the Sand Packet
"""
    px, py, packet_residual_volume = stepn_sand!(
                                                 next_step::Function,
                                                 tᵢ::Int,
                                                 px::Int, py::Int,
                                                 params::Params,
                                                 waters::Waters,
                                                 sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                                                 sediment_transport::SedimentTransport,
                                                 packet_residual_volume::𝔽,
                                                 boundaries::Boundaries
                                                 )

Continue taking subsequent (≥ 2) steps for the sediment (sand) packet, whose movement is
limited to `maxstep` number of steps, perform any deposition or erosion (bed and water-
depth elevation updates), and adjust the flow velocity fields accordingly. Boundary checks
are also performed so that the sediment packet ends its journey once it hits a boundary.
Return the final x, y coordinates as integers and the remaining volume of the packet as
float.

Whether or not the presence of ice is taken into account depends on the value of the input
argument, `ice_thickness` inside the struct `waters`. If it contains [`nothing`](@ref),
ice-free condition is implied. If it is an array similar to, say, `water_depth`, freezing
condition is implied.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `px`, `py`; `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and
`flow_speed` in the struct `waters`; `sand_flux` and `sand_deposit_volume` in the struct
`sediment_transport`; and `packet_residual_volume`, are modified in-place.

---
Requires the following global constants:
  * `onestepx,
     onestepy`  The x and y component-pair representing one step in each direction.
... and all the global constants required by the functions called herein.
"""
function stepn_sand!(
                     next_step::Function,
                     tᵢ::Int,
                     px::Int, py::Int,
                     params::Params,
                     waters::Waters,
                     sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                     sediment_transport::SedimentTransport,
                     packet_residual_volume::𝔽,
                     boundaries::Boundaries
                     )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc, maxstep, Vsp, Δt, θ = params
    @unpack bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters
    @unpack erodibility, sand_flux, sand_deposit_volume = sediment_transport
    @unpack wall_flag, ocean_flag = boundaries

    for nothing in 2:maxstep

        # Determine the direction of the next step.
        # If the packet is not on the inlet edge:
        if px > 1

            # No padding required
            next_step_index = @views next_step(
                                               water_depth[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               ice_thickness[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               θ.sand,
                                               dry_flag[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               wall_flag[(px - 1):(px + 1), (py - 1):(py + 1)]
                                               )

        else # Otherwise, the packet is on the inlet edge:

            # Input here are padded with values outside the domain, i.e., "before" the inlet.
            next_step_index = @views next_step(
                                               [ [0. 0. 0.]; water_depth[1:2, (py - 1):(py + 1)] ],
                                               [ [0. 0. 0.]; ice_thickness[1:2, (py - 1):(py + 1)] ],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               θ.sand,
                                               [ trues(1, 3); dry_flag[1:2, (py - 1):(py + 1)] ],
                                               [ trues(1, 3); wall_flag[1:2, (py - 1):(py + 1)] ]
                                               )

        end # if px > 1; else ...

        # Apportion the flux across the new step, if the step distance is non-zero.
        # Half of it goes to the original cell.
        sand_flux[px, py] += Vsp[tᵢ] / Δt / δc / 2 # Flux quantity discretised by dividing between pixels.

        # Register the position of the new step.
        px += onestepx[next_step_index]
        py += onestepy[next_step_index]

        # Half of it goes to the next cell.
        sand_flux[px, py] += Vsp[tᵢ] / Δt / δc / 2 # Flux quantity discretised by dividing between pixels.

        # Perform any deposition or erosion;
        # deposit_or_erode!(...) returns the change in residual sediment volume of the current packet.
        packet_residual_volume += deposit_or_erode_sand!(
                                                         tᵢ,
                                                         px, py,
                                                         params, waters, sea_bottom, sediment_transport,
                                                         packet_residual_volume
                                                         )

        # Boundary check:
        # Either the packet is not at any boundaries, OR stop the packet's journey and return its current state.
        ~ocean_flag[px, py] && ~wall_flag[px, py] || return px, py, packet_residual_volume

    end # for nothing in 2:max_step

    return px, py, packet_residual_volume

end

## =========================================================================================== deposit_or_erode_sand! ##
# Help to Decide Whether Deposition or Erosion will Happen and then Do It (Sand)
"""
    ΔVp = deposit_or_erode_sand!(
                                 tᵢ::Int,
                                 px::Int, py::Int,
                                 params::Params,
                                 waters::Waters,
                                 sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                                 sediment_transport::SedimentTransport,
                                 packet_residual_volume::𝔽
                                 )

Determine whether deposition or erosion should happen (if so, update the bed elevation,
water depth, and flow speed accordingly), and keep track of any changes in sediment volume.
Return the change in the sediment volume being carried by the current packet as a float.

The flow-speed update can take ice thickness into account if the input argument,
`ice_thickness` in the struct `waters`, is an array like the argument `water_depth`;
otherwise, if `ice_thickness` contains [`nothing`](@ref), ice-free condition is assumed.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`
in the struct `waters`, and `sand_deposit_volume` in the struct `sediment_transport` are
modified in-place.
"""
function deposit_or_erode_sand!(
                                tᵢ::Int,
                                px::Int, py::Int,
                                params::Params,
                                waters::Waters,
                                sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                                sediment_transport::SedimentTransport,
                                packet_residual_volume::𝔽
                                )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc², hdry, u₀, uₘₐₓ, u_sandero, q_sand₀, Vsp, β = params
    @unpack bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters
    @unpack erodibility, sand_flux, sand_deposit_volume = sediment_transport

    # Modify flow-speed erosional limit by erodibility; e.g. if erodibility is 95%, limit is multiplied by 105%.
    u_sand_erode = u_sandero * (2. - erodibility[px, py])

    # If the local sand flux exceeds the transport capacity of sand in terms of flux:
    if sand_flux[px, py] > q_sand₀[tᵢ] * (flow_speed[px, py] / u₀[tᵢ])^β

        # Change in the sediment volume being carried by the packet: deposition.
        ΔVp = - min(
                    packet_residual_volume,
                    water_depth[px, py] * δc² * 0.25 # Limit change to ¼ of water depth
                    ) * (nothingmeanszero(ice_thickness[px, py]) < 0.9999  * water_depth[px, py])
                    # ↑ 0 if ice is effectively as thick as the water depth; 1 if not.

        # Record the deposited volume to calculate the sand fraction in the stratigraphy in the end.
        sand_deposit_volume[px, py] -= ΔVp   # Recall: `ΔVp` is negative when it is deposition——packet is losing mass.

    elseif flow_speed[px, py] > u_sand_erode # OR, if the local flow speed is higher than the erosional limit:
                                             # (Reminder: This is only checked if the first condition above is false.)

        # Change in the sediment volume being carried by the packet: erosion.
        ΔVp = min(
                  Vsp[tᵢ] * (flow_speed[px, py]^β - u_sand_erode^β) / u_sand_erode^β,
                  water_depth[px, py] * δc² * 0.25,  # Limit change to ¼ of water depth
                  (bed_elev[px, py] - sea_bottom[px, py].z[2] + sea_bottom[px, py].permaz[2]) * δc²
                  # ↑ Limit ersion to thawed sediment only; tracks what other packets have already eroded during
                  #   the current time step. (🔔 `sea_bottom[...].z` is only updated once per time step.)
                  ) * (nothingmeanszero(ice_thickness[px, py]) < 0.9999  * water_depth[px, py])
                  # ↑ 0 if ice is effectively as thick as the water depth; 1 if not.

    else # Otherwise, neither conditions of depotision or erosion are satisfied:

        ΔVp = 0.0 # No mass exchanges between the packet and the bed

    end # if sand_flux[px, py] > q_sand₀[tᵢ] * (flow_speed[px, py] / u₀[tᵢ])^β; elseif ...; else ...

    # If there would be mass exchanges:
    if ΔVp ≢ 0.0

        # Perform the deposition or erosion (depending on the sign of `ΔVp`); in practice, this means elevation updates.
        topo_depo_ero!(px, py, bed_elev, water_depth, -ΔVp / δc²)
        # Note: If `ΔVp` is negative, that means depostion; i.e., the packet is losing mass to the bed;
        #       therefore, Δη = -ΔVp / δc² is positive.

        #= Update the dryness status and flow velocity after deposition or erosion. =#
        # First: Is the water is deep enough? Yes:No —— the cell should be considered as wet:dry.
        # Then: If this cell is considered dry:
        if (dry_flag[px, py] = water_depth[px, py] < hdry)

            # Zero flow speed.
            flow_speed[px, py] = flow_speed_x[px, py] = flow_speed_y[px, py] = 0.0

        elseif unit_dischg[px, py] > 0.0 # Otherwise, if the discharge is non-negligible:

            # Perform the flow-velocity update.
            update_flow_velocity!(
                                  (px, py),
                                  uₘₐₓ,
                                  ice_thickness,
                                  water_depth,
                                  flow_speed_x, flow_speed_y, flow_speed,
                                  unit_dischg_x, unit_dischg_y, unit_dischg
                                  )

        end # if (dry_flag[px, py] = ifelse(water_depth[px, py] ≥ hdry, false, true)); elseif ...

    end # if ΔVp ≢ 0.0

    return ΔVp

end

## ======================================================================================================== spawn_mud ##
# Spawn a Packet of Mud Sediment
"""
    px, py, Vsp = spawn_mud(tᵢ::Int, params::Params)

Generate (i.e., spawn) a new mud sediment packet at a randomly chosen inlet cell. Return
x, y coordinates as integers and the volume of the spawned packet as float.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.
"""
function spawn_mud(tᵢ::Int, params::Params) # This method is for a “mud” packet

    # Determine the (inlet) cell for this packet to start its journey.
    px::Int, py::Int = startxy(params)

    return px, py, params.Vsp[tᵢ]

end

## ======================================================================================================= stepn_mud! ##
# Onwards Journey of the Mud Packet
"""
    px, py, packet_residual_volume = stepn_mud!(
                                                next_step::Function,
                                                tᵢ::Int,
                                                px::Int, py::Int,
                                                params::Params,
                                                waters::Waters,
                                                sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                                                sediment_transport::SedimentTransport,
                                                packet_residual_volume::𝔽,
                                                boundaries::Boundaries
                                                )

Continue taking subsequent (≥ 2) steps for the sediment (mud) packet, whose movement is
limited to `max_step` number of steps, perform any deposition or erosion (bed and water-
depth elevation updates), and adjust the flow velocity fields accordingly. Boundary checks
are also performed so that the sediment packet ends its journey once it hits a boundary.
Return the final x, y coordinates as integers and the remaining volume of the packet as
float.

Whether or not the presence of ice is taken into account depends on the value of the input
argument, `ice_thickness` inside the struct `waters`. If it contains [`nothing`](@ref),
ice-free condition is implied. If it is an array similar to, say, `water_depth`, freezing
condition is implied.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`
in the struct `waters`, and `mud_deposit_volume` in the struct `sediment_transport` are
modified in-place.

---
Requires the following global constants:
  * `onestepx,
     onestepy`  The x and y component-pair representing one step in each direction.
... and all the global constants required by the functions called herein.
"""
function stepn_mud!(
                    next_step::Function,
                    tᵢ::Int,
                    px::Int, py::Int,
                    params::Params,
                    waters::Waters,
                    sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                    sediment_transport::SedimentTransport,
                    packet_residual_volume::𝔽,
                    boundaries::Boundaries
                    )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc, maxstep, Δt, θ = params
    @unpack bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters
    @unpack erodibility, mud_deposit_volume = sediment_transport
    @unpack wall_flag, ocean_flag = boundaries

    for nothing in 2:maxstep

        # Determine the direction of the next step.
        if px > 1

            # No padding required
            next_step_index = @views next_step(
                                               water_depth[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               ice_thickness[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               θ.mud,
                                               dry_flag[(px - 1):(px + 1), (py - 1):(py + 1)],
                                               wall_flag[(px - 1):(px + 1), (py - 1):(py + 1)]
                                               )

        else # Otherwise, the packet is on the inlet edge:

            # Input here are padded with values outside the domain, i.e., "before" the inlet.
            next_step_index = @views next_step(
                                               [ [0. 0. 0.]; water_depth[1:2, (py - 1):(py + 1)] ],
                                               [ [0. 0. 0.]; ice_thickness[1:2, (py - 1):(py + 1)] ],
                                               unit_dischg_x[px, py],
                                               unit_dischg_y[px, py],
                                               θ.mud,
                                               [ trues(1, 3); dry_flag[1:2, (py - 1):(py + 1)] ],
                                               [ trues(1, 3); wall_flag[1:2, (py - 1):(py + 1)] ]
                                               )

        end # if px > 1; else ...

        # Register the position of the new step.
        px += onestepx[next_step_index]
        py += onestepy[next_step_index]

        # Perform any deposition or erosion;
        # deposit_or_erode!(...) returns the change in residual sediment volume of the current packet.
        packet_residual_volume += deposit_or_erode_mud!(
                                                        tᵢ,
                                                        px, py,
                                                        params, waters, sea_bottom, sediment_transport,
                                                        packet_residual_volume
                                                        )

        # Boundary check:
        # Either the packet is not at any boundaries, OR stop the packet's journey and return its current state.
        ~ocean_flag[px, py] && ~wall_flag[px, py] || return px, py, packet_residual_volume

    end # for nothing in 2:max_step

    return px, py, packet_residual_volume

end

## ============================================================================================ deposit_or_erode_mud! ##
# Help to Decide Whether Deposition or Erosion will Happen and then Do It (Mud)
"""
    ΔVp = deposit_or_erode_mud!(
                                tᵢ::Int,
                                px::Int, py::Int,
                                params::Params,
                                waters::Waters,
                                sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                                sediment_transport::SedimentTransport,
                                packet_residual_volume::𝔽
                                )

Determine whether deposition or erosion should happen (if so, update the bed elevation,
water depth, and flow speed accordingly), and keep track of any changes in sediment volume.
Return the change in the sediment volume being carried by the current packet as a float.

The flow-speed update can take ice thickness into account if the input argument,
`ice_thickness` in the struct `waters`, is an array like the argument `water_depth`;
otherwise, if `ice_thickness` contains [`nothing`](@ref), ice-free condition is assumed.

tᵢ is the current time-step number, used for indexing so should include any ramp-up steps if
this is an "Arctic" run with ice.

Input arguments `bed_elev`, `water_depth`, `flow_speed_x`, `flow_speed_y`, and `flow_speed`,
in the struct `waters`, and `mud_deposit_volume` in the struct `sediment_transport` are
modified in-place.
"""
function deposit_or_erode_mud!(
                               tᵢ::Int,
                               px::Int, py::Int,
                               params::Params,
                               waters::Waters,
                               sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                               sediment_transport::SedimentTransport,
                               packet_residual_volume::𝔽
                               )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δc², hdry, uₘₐₓ, u_muddep, u_mudero, Vsp, β = params
    @unpack bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters
    @unpack erodibility, mud_deposit_volume = sediment_transport

    # Modify flow-speed erosional limit by erodibility; e.g. if erodibility is 95%, limit is multiplied by 105%.
    u_mud_erode = u_mudero * (2. - erodibility[px, py])

    # If the local flow speed is slower than the depositional limit:
    if flow_speed[px, py] < u_muddep

        # Change in the sediment volume being carried by the packet: deposition.
        ΔVp = - min(
                    packet_residual_volume * (u_muddep^β - flow_speed[px, py]^β) / u_muddep^β,
                    water_depth[px, py] * δc² * 0.25 # Limit change to ¼ of water depth
                    ) * (nothingmeanszero(ice_thickness[px, py]) < 0.9999  * water_depth[px, py])
                    # ↑ 0 if ice is effectively as thick as the water depth; 1 if not.

        # Record the deposited volume to calculate the sand fraction in the stratigraphy in the end.
        mud_deposit_volume[px, py] -= ΔVp    # Recall: `ΔVp` is negative when it is deposition——packet is losing mass.

    elseif flow_speed[px, py] > u_mud_erode # OR, if the local flow speed is higher than the erosional limit:
                                            # (Reminder: This is only evaluated if the first condition above is false.)

        # Change in the sediment volume being carried by the packet: erosion.
        ΔVp = min(
                  Vsp[tᵢ] * (flow_speed[px, py]^β - u_mud_erode^β) / u_mud_erode^β,
                  water_depth[px, py] * δc² * 0.25,  # Limit change to ¼ of water depth
                  (bed_elev[px, py] - sea_bottom[px, py].z[2] + sea_bottom[px, py].permaz[2]) * δc²
                  # ↑ Limit ersion to thawed sediment only; tracks what other packets have already eroded during
                  #   the current time step. (🔔 `sea_bottom[...].z` is only updated once per time step.)
                  ) * (nothingmeanszero(ice_thickness[px, py]) < 0.9999  * water_depth[px, py])
                  # ↑ 0 if ice is effectively as thick as the water depth; 1 if not.

    else # Otherwise, neither conditions of depotision or erosion are satisfied:

        ΔVp = 0.0 # No mass exchanges between the packet and the bed

    end # if flow_speed[px, py] < u_muddep; elseif ...; else ...

    # If there would be mass exchanges:
    if ΔVp ≢ 0.0

        # Perform the deposition or erosion (depending on the sign of `ΔVp`); in practice, this means elevation updates.
        topo_depo_ero!(px, py, bed_elev, water_depth, -ΔVp / δc² )
        # If `ΔVp` is negative, that means depostion——the packet is losing mass to the bed;
                         # therefore, Δη = -ΔVp / δc² is positive.

        #= Update the dryness status and flow velocity after deposition or erosion. =#
        # First: Is the water is deep enough? Yes:No —— the cell should be considered as wet:dry.
        # Then: If this cell is considered dry:
        if (dry_flag[px, py] = water_depth[px, py] < hdry)

            # Zero flow speed.
            flow_speed[px, py] = flow_speed_x[px, py] = flow_speed_y[px, py] = 0.0

        elseif unit_dischg[px, py] > 0.0 # Otherwise, if the discharge is non-negligible:

            # Perform the flow-velocity update.
            update_flow_velocity!(
                                  (px, py),
                                  uₘₐₓ,
                                  ice_thickness,
                                  water_depth,
                                  flow_speed_x, flow_speed_y, flow_speed,
                                  unit_dischg_x, unit_dischg_y, unit_dischg
                                  )

        end # if (dry_flag[px, py] = ifelse(water_depth[px, py] ≥ hdry, false, true)); elseif ...

    end # if ΔVp ≢ 0.0

    return ΔVp

end

## =================================================================================================== bed_diffusion! ##
# Perform Diffusive Filtering of the Bed Elevation.
"""
    bed_diffusion!(
                        params::Params,
                        waters::Waters,
                        sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                        sediment_transport::SedimentTransport,
                        boundaries::Boundaries
                        )

Performs a number (defined by `lastbeddiffpass`) of passes of the diffusive filtering of
the bed elevation, after the sediments packets have passed through the simulation domain.
The idea of this filtering process is set out by equation (24) of Liang et al. (2015).

At the end of this function, water depth values are updated and sand flux values are reset
to zero.

Input arguments `bed_elev`, `water_depth`, and `dry_flag` in the struct `waters` are
modified in-place.

!!! warning "Assumption of boundary padding"
    In the implementation herein, we assume that at least 1 cell on the edges of the domain
    is classified as a boundary cell, so that there is always a 1-cell padding. This code
    will need to be modified if boundary flags are false on any of the sides of the
    simulation domain, or there may be unexpected/undesirable results.

---
**Mathematical Explanation:**

The change in bed elevation, η, due to “diffusive flux” (actually could better be called
“diffusive sediment unit discharge”, with dimensions of [m²/s]), `q_sand_diff`, over time,
Δt, and cell area, δc², is

``
    Δη_{diff} = Q_{sand,diff} × Δt ÷ δc² ,
``

where ``Q_{sand,diff}`` is the diffusive sediment discharge (i.e., volume velocity [m³/s]).
Given that ``Q_{sand,diff}`` divided by δc is the diffusive sediment unit discharge (or
“diffusive flux” in Liang et al., 2015), we can rewrite equation (24) of Liang et al.
(2015, Part I) as

``
    Δη_{diff} = q_{sand,diff} × δc × Δt ÷ δc² = α × ∇η × q_{sand} × δc × Δt ÷ δc² ,
``

where α is a scaling constant. The diffusive change can be averaged over multiple number of
passes of this diffusive filtering process. Both ∇η and ``q_{sand}`` are calculated accross
the boundaries between the local coordinate (i.e., current cell) and each of the
neighbouring cells. In practice, the product `∇η × q_{sand} × δc` is rendered as

``
    ∑{½ × [q_{sand}(c) + q_{sand}(i)] × δc × [η(i) - η(c)] / δc} ,
``

where the summation ∑ is over each of the 8 neighbouring cells (i.e., excluding the current
cell itself in the centre) with coordinate index `i`. The current cell is represented by
the coordinate index `c`. The multiplication by ½ reflects the averaging nature of the
``q_{sand}`` term (as the flux across a cell boundary); this ½ factor is absorbed into the
coefficient, `beddiffusecoef`, (together with α, Δt, and δc²) and dropped from the
expressions below.

Multiplying out the above summation and factoring the terms, we get the following:

``
    q_{sand}(c) × [∑η(i) - 8η(c)] - η(c) × ∑q_{sand}(i) + ∑[η(i) × q_{sand}(i)] ,
``

where the summations ∑ are again over each of the neighbours and excluding the central
self. The first summation can be computed from a convolution between the bed-elevation
(i.e., η) grid and the filter/kernel:

    [1  1  1;
     1 -8  1;
     1  1  1],

which we name `kernel_diffneighbours` due to its differencing nature. The second and
third summations can be computed from convolutions between the sand flux (i.e.,
``q_{sand}``) and the bed-elevation—sand-flux product (i.e., ``η(i) × q_{sand}(i)``) grid,
respectively, and the filter/kernel:

    [1  1  1;
     1  0  1;
     1  1  1],

which we name `kernel_addneighbours` due to its addition nature.

---
Requires the following global constants:
  * `kernel_addneighbours`
        When convolved with, each cell will contain the sum of its 8 neighbours.
  * `kernel_diffneighbours`
        When convolved with, each cell will contain the sum of its neighbours minus 8 × itself.
"""
function bed_diffusion!(
                        params::Params,
                        waters::Waters,
                        sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                        sediment_transport::SedimentTransport,
                        boundaries::Boundaries
                        )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack hdry, lastbeddiffpass, beddiffusecoef = params
    @unpack surf_elev, bed_elev, water_depth, dry_flag = waters
    @unpack erodibility, sand_flux = sediment_transport
    @unpack wall_flag, ocean_flag = boundaries

    # Convolution between `sand_flux` and `kernel_add_neighbours`, resulting in a grid in which each cell contains the
    # sum of all the neighbouring cells (but not itself).
    sand_flux_convolved = convolve(sand_flux, kernel_addneighbours)::Array{𝔽,2}

    bedel_sandfl::Array{𝔽,2} = bed_elev .* sand_flux

    # Perform a number of passes of this diffusive filtering processes, each scaled by `1 / lastbeddiffpass` (built into
    # the coefficient, `beddiffusecoef`) to achieve averaging effect.
    for pass_number in 1:lastbeddiffpass

        # Convolution between `η` and `kernel_diff_neighbours`, resulting in a grid in which each cell contains the sum
        # of all the differences between each of the neighbouring cells and itself.
        bed_elev_convolved = convolve(bed_elev, kernel_diffneighbours)::Array{𝔽,2}

        # Convolution between `η .* q_sand` and `kernel_add_neighbours`, resulting in a grid in which each cell contains
        # the sum of all the neighbouring cells (but not itself).
        bedel_sandfl_convolved = convolve(bedel_sandfl, kernel_addneighbours)::Array{𝔽,2}

        # Go through the domain to update η.
        @inbounds @simd for lineari in eachindex(ocean_flag, bed_elev, bedel_sandfl)

            # If the current cell is not at a wall or ocean boundary:
            if ~ocean_flag[lineari] && ~wall_flag[lineari]

                # η += Δη_diff (see mathematical explanation in the documentation of this function).
                bed_elev[lineari] += beddiffusecoef * #⬅ ≡ α * Δt / (2 * lastbeddiffpass * δc²)::typeof(0.0)
                                     erodibility[lineari] *
                                     (sand_flux[lineari] * bed_elev_convolved[lineari] -
                                      bed_elev[lineari] * sand_flux_convolved[lineari] +
                                      bedel_sandfl_convolved[lineari])

                # Limit elevation change (during the current time step) to non-permafrost layers only!
                bed_elev[lineari] = max(bed_elev[lineari], sea_bottom[lineari].z[2] - sea_bottom[lineari].permaz[2])

            end # if ~ocean_flag[lineari] && ~wall_flag[lineari]

            #=
            Whilst we are here going through the grids cell by cell, we might as well do a few things that are also
            cell-by-cell. Even though they are on different grid variables, it is still better for the computer's
            cache to do so (regardless of compiler).
            =#
            # If this is not yet the last diffusive-filtering pass:
            if pass_number < lastbeddiffpass

                # Prepare the updated version of `η .* q_sand` for the next pass of the diffusion. Skipped if this is
                # the last pass, since the value will not be used again in that case.
                bedel_sandfl[lineari] = bed_elev[lineari] * sand_flux[lineari]

            else # Otherwise:

                # Update water depth and dry flag, which changed after the diffusive filtering of the bed elevation.
                # (Recall: the water surface elevation has not changed since the last water update.)
                update_water_depth_dry_flag!(lineari, hdry, surf_elev, bed_elev, water_depth, dry_flag)

                # Sand flux values will not be used again until the next time step; reset to zero.
                sand_flux[lineari] = 0.0

            end # if pass_number < lastbeddiffpass; else ...

        end # for lineari in eachindex(ocean_flag, bed_elev, bedel_sandfl)

    end # for pass_number in 1:lastbeddiffpass

end

## ==================================================================================================== thaw_stefans! ##
# Evolve the thaw depths of sediment columns based on Stefan's solution
"""
    thaw_stefans!(𝕀⁺°::Number, sea_bottom::Array{SedimentColumn{𝔽𝕤},2}; T̄⁺ = 4.0, I⁺°₀ = 0.0)
    thaw_stefans!(𝕀⁺°::Array{Float64,2}, sea_bottom::Array{SedimentColumn{𝔽𝕤},2}; T̄⁺ = 4.0, I⁺°₀ = 0.0)

Assign the thaw depth at positive-degree-day index, T̄⁺ × 𝕀⁺°, to all sediment columns
contained in `sea_bottom` (which is an array of [`SedimentColumn`](@ref) structs). T̄⁺ is
the mean temperature during the period above freezing, and 𝕀⁺° is the accumulated number
of days during that same period. An offset argument, I⁺°₀, is provided such that the actual
postive-degree-day index used is 𝕀⁺° + I⁺°₀.

If 𝕀⁺° is an array, it is spatially dependent (e.g. due to bed-fast ice insulation).

Stefan's solution giving the thaw depth, Χ, is as follows:

     Χ = √(2 × λ❄ × 𝕀⁺° × T⁺ × sperday / Lf),

where `λ❄` is the thermal conductivity of ice near 0°C, which is roughly 2.22 W⋅m⁻¹⋅K⁻¹;
`𝕀⁺° × T⁺` is the positive degree day index, which is the number of days since the start
of the year when the temperature was positive multiplied by the temperature; `sperday` is
seconds per day; `Lf` is the volumetric latent heat of fusion of water, which is
333.55 × 10³ J⋅kg⁻¹ × 1000 kg⋅m⁻³.

The mean temperature, `T̄⁺`, is used here for simplicity. `T⁺` could be expanded to become
a temperature time series in future updates.
"""
function thaw_stefans!(𝕀⁺°::Number, sea_bottom::Array{SedimentColumn{𝔽𝕤},2}; T̄⁺ = 4.0, I⁺°₀ = 0.0)
    #=
    Given:

        # Thermal conductivity of ice near 0°C is roughly 2.22 W⋅m⁻¹⋅K⁻¹
	    λ❄ = 2.22 # Unicode \lambda\:snowflake:

    	# Latent heat of fusion of water is 333.55 × 10³  J⋅kg⁻¹
    	# Volumetric latent heat of fusion is then 333.55 × 10³ J⋅kg⁻¹ × 1000 kg⋅m⁻³
    	Lf = 333.55e3 * 1e3

        # The number of seconds in a day
        sperday = 86400

    We have:

        2 × λ❄ × sperday / Lf = 0.0011501004347174337
    =#

    thaw_depth = √(0.0011501004347174337 * T̄⁺ * (𝕀⁺° + I⁺°₀))

    @inbounds @simd for lineari in eachindex(sea_bottom)

        sea_bottom[lineari].permaz[2] = thaw_depth

    end

end

function thaw_stefans!(𝕀⁺°::Array{Float64,2}, sea_bottom::Array{SedimentColumn{𝔽𝕤},2}; T̄⁺ = 4.0, I⁺°₀ = 0.0)

    @simd for lineari in eachindex(sea_bottom)

        # (See comment in the variant of `thaw_stefans!` above for the origin of 0.0011501004347174337.)
        sea_bottom[lineari].permaz[2] = √(0.0011501004347174337 * T̄⁺ * (𝕀⁺°[lineari] + I⁺°₀))

    end

end

## ============================================================================================= sedimentary_records! ##
# Register the Mass-exchanges Determined during the Current Time Step into the Sedimentary Records
"""
    sedimentary_records!(
                         permafrost_anonymous::Function,
                         current_time::𝔽,
                         params::Params,
                         waters::Waters,
                         sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                         sediment_transport::SedimentTransport,
                         args...
                         )

Register the effects of erosion or deposition into the sedimentary records, keeping track
of the top- and bottom-elevations of the strata columns at individual elements in the
simulation domain, and the properties of each layer of the sediment deposits.

Input arguments `sand_deposit_volume` and `mud_deposit_volume` in the struct
`sediment_transport`, and `sea_bottom` are modified in-place.

!!! note "The bed-rock and bed-surface elevations"
    The elevations stored with the sedimentary records, `z[1]` and `z[2]`, are discretised
    to the nearest `δz`. This is to avoid rounding errors from accumulating and causing
    excessively thick sedimentary columns (or the opposite). The `z` elevations should track
    the bed elevation, `η`, approximately, but will likely not be identical to it.

See the documentations of composite types [`Sediment`](@ref)  and [`SedimentColumn`](@ref)
for additional information.
"""
function sedimentary_records!(
                              permafrost_anonymous::Function,
                              current_time::𝔽,
                              params::Params,
                              waters::Waters,
                              sea_bottom::Array{SedimentColumn{𝔽𝕤},2},
                              sediment_transport::SedimentTransport,
                              args... # Intended to be `happy_new_year`
                              )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack δz, hB, 𝐸 = params
    @unpack bed_elev = waters
    @unpack erodibility, sand_deposit_volume, mud_deposit_volume = sediment_transport

    layers_zapped::Int = 0
    layers_added::Int  = 0

    # Go through the domain element by element using the optimal indexing (most likely linear).
    for lineari in eachindex(bed_elev, sea_bottom)

        # Is it erosion?
        if bed_elev[lineari] < sea_bottom[lineari].z[2]

            # Zap from the current sediment column, `sea_bottom[lineari].strata`, the appropriate number of layers.
            layers_zapped = erode_stratum!(
                                sea_bottom[lineari].strata,
                                min(
                                    length(sea_bottom[lineari].strata),
                                    round(Int, (sea_bottom[lineari].z[2] - bed_elev[lineari]) / δz)
                                )
                            )

            if layers_zapped > 0

                # Update the elevation of the top of the sediment column, discretised as multiples of δz.
                sea_bottom[lineari].z[2] -= layers_zapped * δz

            end # if erode_stratum!(...)

            #=
            If the sediment column is empty, update the bed-rock elevation (which may be lower now) to ensure that it
            is the same as the bed-surface elevation.
            =#
            if length(sea_bottom[lineari].strata) ≡ 0

                sea_bottom[lineari].z[1] = sea_bottom[lineari].z[2]

            end # if length(sea_bottom[lineari].strata) ≡ 0

            #=
            The bed-rock elevation of the sediment column, `z[1]`, is expected to be ≤ -hB, which is the initial
            elevation of the sea bottom at the start of the simulation. Sometimes, due to rounding, the bottom
            side of the sediment column would get “jiggled” to above -hB. This should be alright as long as:
                z[1] - (-hB) ≲ δz .
            The following line will print a message (if debug logging is enabled) for double-checking.
            =#
            sea_bottom[lineari].z[1] > -hB &&
                (@debug begin
                    cartesiani = CartesianIndices(sea_bottom)[lineari]
                    "At $cartesiani, the bottom of the sediment column is at $(sea_bottom[cartesiani].z[1]) m."
                 end)

        # OR: Deposition with at least some sand?
        elseif sand_deposit_volume[lineari] > 0.0

            # Add to the current sediment column, `sea_bottom[lineari].strata`, the appropriate number of layers.
            layers_added = deposit_stratum!(
                               sea_bottom[lineari].strata,
                               sand_deposit_volume[lineari] /                                    # Sand
                                   (sand_deposit_volume[lineari] + mud_deposit_volume[lineari]), #      fraction
                               current_time,                                                     # Current time
                               round(Int, (bed_elev[lineari] - sea_bottom[lineari].z[2]) / δz)   # № of layers
                           )

            if layers_added > 0

                # Update the elevation of the top of the sediment column, discretised as multiples of δz.
                sea_bottom[lineari].z[2] += layers_added * δz

            end # if deposit_stratum!(...)

        # OR: Deposition with just mud?
        elseif mud_deposit_volume[lineari] > 0.0

            # Add to the current sediment column, `sea_bottom[lineari].strata`, the appropriate number of layers.
            layers_added = deposit_stratum!(
                               sea_bottom[lineari].strata,
                               0.0,                                                              # Sand fraction
                               current_time,                                                     # Current time
                               round(Int, (bed_elev[lineari] - sea_bottom[lineari].z[2]) / δz)   # № of layers
                           )

            if layers_added > 0

                # Update the elevation of the top of the sediment column, discretised as multiples of δz.
                sea_bottom[lineari].z[2] += layers_added * δz

            end # if deposit_stratum!(...)

        end # if bed_elev[lineari] < sea_bottom[lineari].z[2]; elseif ...; elseif ...

        # Check whether the current cell satisfies the classification as permafrost, and assign erodibility accordingly.
        # (NB: This function does something only when called from a permafrost-enabled simulation.)
        permafrost_anonymous(lineari, δz, 𝐸, sea_bottom[lineari], erodibility, args...)

        # Zero the to-be-deposited sediment volume for the next time step.
        sand_deposit_volume[lineari] = 0.0
        mud_deposit_volume[lineari]  = 0.0

    end # for lineari in eachindex(bed_elev, sea_bottom)

end

## ------------------------------------------------------------------------------ Alias: sedimentary_records_icefree! ##
"""
    sedimentary_records_icefree!(current_time, params, waters, sea_bottom, sediment_transport)

Alias of the variant of [`sedimentary_records!`](@ref) without permafrost considerations.
"""
sedimentary_records_icefree!(current_time, params, waters, sea_bottom, sediment_transport) =
sedimentary_records!((x...) -> nothing, current_time, params, waters, sea_bottom, sediment_transport)
# Note: (x...) -> nothing is a dummy anonymous function.

## ------------------------------------------------------------------------------ Alias: sedimentary_records_withice! ##
"""
    sedimentary_records_withice!(current_time, params, waters, sea_bottom, sediment_transport, happy_new_year::Int)

Alias of the variant of [`sedimentary_records!`](@ref) with permafrost classification built
in for adjusting erodibility and assigning date-of-freezing.

`happy_new_year` can either be the number of completed years in the simulation so far,
say, `years_gone`, or the value `typemin(years_gone)`. This argument can be entered as
`<'condition marking end-of-model-year'> ? years_gone : typemin(years_gone)`, for example.
Note that, here, `years_gone` includes the year that has just finished. Therefore, to mark
the time of some event that happened just prior to this, mark it as `years_gone - 1`.
"""
sedimentary_records_withice!(current_time, params, waters, sea_bottom, sediment_transport, happy_new_year::Int) =
sedimentary_records!(current_time, params, waters, sea_bottom, sediment_transport, happy_new_year) do lineari,
    δz, 𝐸, sediment_column::SedimentColumn{𝔽𝕤}, erodibility, happy_new_year

    # TODO: Replace measuring frozen fraction with permafrost thickness, `sediment_column.permaz[1]`, by measuring
    #       with an evolving thaw-depth, `sediment_column.permaz[2]`. Instead of using permafrost fraction to assign
    #       reduced erodibility, use instead the frozen fraction. At the end of the model year, the maximum reach
    #       of the thaw depth _during the year (and likely outside the simulation extent during the year)_ should be
    #       assigned to `sediment_column.permaz[2]`, so that the `freeze_or_thaw` function can correctly assign
    #       permafrost states to sediment cells.

    # Is the cell more than 75% frozen? If so, assign permafrost erodibility; otherwise, assign full erodibility.
    erodibility[lineari] = (
                            (sediment_column.permaz[1] - sediment_column.z[1]) /
                            min(sediment_column.z[2] - sediment_column.z[1], 5)
                            ) > 0.75 ? 𝐸 : 1
                           #= A note of concern:
                           Lauzon et al. (2019) wrote in their supplementary material (Text S2) that:

                               “For an entire cell to be treated as permafrost, its total permafrost thickness
                                must be greater than 75% of either the 5 m inlet channel depth (3.75 m) or the
                                local deposit thickness, whichever is greater. This ensures that thinner deposits
                                can still be treated as permafrost; however, the 0.5 m active layer thickness
                                imposes a minimum deposit thickness of 2 m for a cell to be treated as permafrost.”

                           This seems to give an impression that, in order to be classified as a “permafrost cell”, the
                           thickness of permafrost must be 75% of max(5, x). However, that would contradict the second
                           sentence, because there is no way a 2 m sediment column, even if entirely permafrost, could
                           satisfy the classification condition (∵ 2 / 5 < 0.75). I believe the authors meant the
                           threshold is at 75% of the local deposit thickness or of the 5 m inlet channel depth,
                           _whichever gives the greater percentage_. Hence, `min(x, 5)` is used instead of `max(5, x)`.
                           =#

    # If we are at the end of a model year:
    if happy_new_year ≢ typemin(happy_new_year)

        # The second argument to the call to `freeze_or_thaw` below is the number of layers that should be frozen,
        # defined as frozen_layers ≡ top_elevation - thaw_depth - bottom_elevation .
        # A note on the functional form of minus, `-(...)`, used in calculating `frozen_layers` below:
        #   ∵ -(a, b) ≡ a - b
        #   ∴ - -(sediment_column.z...) - 0.5 == - (sediment_column.z[1] - sediment_column.z[2]) - 0.5
        #                                     == (sediment_column.z[2] - sediment_column.z[1]) - 0.5
        # where the value 0.5 was for `sediment_column.permaz[2]`.

        # Update the frozen states and dates of the sediment column.
        freeze_or_thaw!(
                        sediment_column,
                        δz,
                        round(Int, (- -(sediment_column.z...) - sediment_column.permaz[2]) / δz),
                        happy_new_year - 1
                        )
        # NB: `happy_new_year - 1` is the year that just ended, and when the layers below the thaw depth became frozen.

    end

end

## ================================================================================================== write_statevars ##
# Write the State-variables Arrays at the Current Time Step into the Output File
"""
    write_statevars(file_path::String, time_step::Int, waters::Waters)

Write the state variables to the specified NetCDF file in `file_path`, starting at exactly
the `time_step` position specified.

!!! tip "Handling of NetCDF files"
    To read back the file specified in `file_path`, use `ncread(file_path, "...")`, where
    `"..."` is the variable name as listed in the information print-out from
    `ncinfo(file_path)`. See [`ncread`](@ref) and [`ncinfo`](@ref) for details.
"""
function write_statevars(file_path::String, time_step::Int, params::Params, waters::Waters)

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny = params
    @unpack surf_elev, bed_elev, ice_thickness, water_depth, flow_speed, unit_dischg = waters

    ncwrite(reshape(bed_elev,    Nx, Ny, 1), file_path, "eta"; start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(reshape(surf_elev,   Nx, Ny, 1), file_path, "H";   start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(reshape(water_depth, Nx, Ny, 1), file_path, "h";   start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(reshape(unit_dischg, Nx, Ny, 1), file_path, "qw";  start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(reshape(flow_speed,  Nx, Ny, 1), file_path, "u";   start = [1, 1, time_step], count = [-1, -1, 1])
    typeof(ice_thickness) <: Array{Nothing} || # ← Either `ice_thickness` is nothing, OR ↓ write it to output
        ncwrite(reshape(ice_thickness, Nx, Ny, 1), file_path, "hi"; start = [1, 1, time_step], count = [-1, -1, 1])
    #= Note:
    Due to the three-dimensional nature of the variables spaces in the NetCDF file, there is a need to expand the
    dimension of any two-dimensional arrays by 1 to match it; hence the `reshape(...)` calls. The `count` keyword is
    perhaps somewhat redundant, but ensures that the writing is only limited to 1 time step. The value of -1 simply
    means “fill as much as necessary”.
    =#

end

## ================================================================================================= write_stateflags ##
# Write the Status-flags Arrays at the Current Time Step into the Output File
"""
    write_stateflags(
                     file_path::String,
                     time_step::Int,
                     waters::Waters,
                     boundaries::Boundaries
                     )

Write the status-flags variables to the specified NetCDF file in `file_path`, starting at
exactly the `time_step` position specified.

!!! tip "Handling of NetCDF files"
    To read back the file specified in `file_path`, use `ncread(file_path, "...")`, where
    `"..."` is the variable name as listed in the information print-out from
    `ncinfo(file_path)`.  See [`ncread`](@ref) and [`ncinfo`](@ref) for details.
"""
function write_stateflags(
                          file_path::String,
                          time_step::Int,
                          params::Params,
                          waters::Waters,
                          boundaries::Boundaries
                          )

    # Unpack the necessary fields of the input structs so that their call-names are available individually.
    @unpack Nx, Ny = params
    @unpack dry_flag = waters
    @unpack wall_flag, ocean_flag = boundaries

    # The flag variables are all converted from BitArray{2} to Array{UInt8} before being written into the NetCDF file;
    # the receiving variable type (defined when the NetCDF file was created) is `NC_BYTE`.
    ncwrite(convert(Array{UInt8}, reshape(wall_flag,   Nx, Ny, 1)), file_path, "wall";
            start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(convert(Array{UInt8}, reshape(ocean_flag,  Nx, Ny, 1)), file_path, "ocean";
            start = [1, 1, time_step], count = [-1, -1, 1])
    ncwrite(convert(Array{UInt8}, reshape(dry_flag,    Nx, Ny, 1)), file_path, "dry";
            start = [1, 1, time_step], count = [-1, -1, 1])
    #= Note:
    Due to the three-dimensional nature of the variables spaces in the NetCDF file, there is a need to expand the
    dimension of any two-dimensional arrays by 1 to match it; hence the `reshape(...)` calls. The `count` keyword is
    perhaps somewhat redundant, but ensures that the writing is only limited to 1 time step. The value of -1 simply
    means “fill as much as necessary”.
    =#

end

## ===================================================================================================== write_strata ##
# Write the Sediment-Strata Data into a Julia Data File (JLD2)
"""
    write_strata(file_path::String, params::Params, sediment_strata::Array{SedimentColumn{𝔽𝕤},2})

Write the sediment strata data into a Julia Data File (JLD2), which is a special kind of
HDF5 file. Due to the special composite types in which the strata data are stored (hence
the use of JLD2 instead of HDF5 here), a read-me file is generated explaining the fact that
these special types need to be declared before loading the content of the JLD2 file. The
exact code for declaring the types is included in the read-me file.

!!! tip "Handling of the JLD2 file output"
    To read back the data in the JLD2 file, first declare the composite types as described
    in the read-me file (or simply include "src/types.jl"). Then, if the content of the
    file is to be stored in a variable called `seabedstrata`, for example, run the command
    `seabedstrata = read(file, "seabedstrata")`, where `file` is the full path with
    filename as a String. Don't forget to run `using JLD2` first!
"""
function write_strata(file_path::String, params::Params, sediment_strata::Array{SedimentColumn{𝔽𝕤},2})

    print("\n[ Writing sediment strata to file... ")

    # Open a JLD2 file (a new one or in place of one already removed during initialisation of the simulation),
    # write the sediment strata data, and close the file when done.
    jldopen(file_path, "w") do file
        write(file, "seabedstrata", sediment_strata)
        write(file, "dz", params.δz)
    end # do

    #=
    Automatically generate a read-me file explaining the composite data type required to load the strata JLD2 file
    properly. This is because the strata data are saved in specially declared composite types, and require these
    types to be declared before loading the content of the data file.
    =#
    # If a read-me file does not already exist in the directory where the strata output is destined:
    if !isfile(joinpath(dirname(file_path), "README_strata_JLD2.txt"))
        # Create a new text file there.
        readmefile = open(
                          joinpath(dirname(file_path), "README_strata_JLD2.txt"),
                          create = true,
                          truncate = true   # This keyword flag is redundant, but set just in case...
                          )

        # Write the content of the read-me file.
        print(
        readmefile,
        """
The file with the sediment strata data with `.jld2` extension (i.e., with a
filename in the form of “*_strata.jld2”) is a Julia Data File, which is a
special type of HDF5 file. The strata data stored therein is in a special
composite type (as opposed to a standard type, such as a floating-point or
integer array). Therefore, to load the data properly, one should make sure
that the corresponding data type is declared and loaded beforehand.

The two composite data types are: Sediment{T} and SedimentColumn{T}.

They are declared as follows (code quoted in triple backticks ``` ```):

# Original doc-string of Sediment{T}:
# Deposited-sediment type. Contains fields (of parametric type, T) that are
# properties associated with this element of deposited sediment: sand fraction
# `sandfrac`, time of deposition `timeofdep`, and time since frozen `frostysince`.
```
struct Sediment{T}

    sandfrac::T             # Sand fraction
    timeofdep::T            # Time when deposited, in years since the start of simulation
    frostysince::Vector{T}  # Time when it became part of the frozen layer, in model years

end
```

# Original doc-string of SedimentColumn{T}
# Sediment-column type. Contains fields (of parametric type, T) that are properties
# associated with this particular column: the elevations of the sea bottom and the
# underlying bedrock `z[1:2]`, the thickness of permafrost `permaz[1]` and the depth
# of the active layer `permaz[2]`, and the stratigraphic (vector) column of sediments
# `strata`.
```
struct SedimentColumn{T}

    # NB: The Vector construct of any scalar attributes is so that the value is mutable,
    #     even though the size is not.

    # Two-element vector:
    #    [1] Bed-rock elevation; i.e., the bottom of the sediment column, in metres
    #    [2] Bed-surface elevation; i.e., the top of the sediment column, in metres
    z::Vector{T}

    # Two-element vector:
    #    [1] Thickness of permafrost layers, in metres from the bottom (z[1])
    #    [2] Depth of thaw or active layer, in metres from the top (z[2])
    permaz::Vector{T}

    # Vector of the deposited elements, with the bed surface at the end of the vector
    strata::Vector{Sediment{T}}

end
```

Alternatively, one could also load the file “src/types.jl”, if available.

[This read-me file is automatically generated the first time a strata data ]
[ output file is saved in a directory. Version up to date as of Nov, 2020. ]
        """
        )
    end # if !isfile(joinpath(dirname(file_path), "README_strata_JLD2.txt"))

    println("DONE!\n")

end

## ======================================================================================================= save_state ##
"""
    save_state(
               file_path::String,
               params::Params,
               all_gridded_vars::AllGriddedVars,
               sea_surface_height::𝔽
               )

Save all the state variables necessary for the resumption or continuation of an ArcDelRCM
simulation. The resulting file is in JLD2 format and located in the full path specified by
`file_path`.

To inspect the variable names stored inside the JLD2 file, use the [`names`](@ref) function:

    julia> jldopen(file_path, "r") do file # EXAMPLE
               names(file)
           end
    17-element Array{String,1}:
     "bed_elev"
     "domainparams"
     "dry_flag"
     "erodibility"
     "flow_speed"
     "flow_speed_x"
     "flow_speed_y"
     "ice_thickness"
     "ocean_flag"
     "sea_surface_height"
     "seabed"
     "surf_elev"
     "unit_dischg"
     "unit_dischg_x"
     "unit_dischg_y"
     "wall_flag"
     "water_depth"

!!! note "JLD2 versus NetCDF"
    The decision to use JLD2 file to “dump” all relevant state variables is not made without
    thought. Theoretically, the state variables could all be saved into an expanded NetCDF
    record, giving the freedom to inherit a state from any time point of a saved simulation.
    However, the added I/O and the size of the added record is concerning, especially on
    larger domain grids or simulation lengths. The type- and precision- conversions between
    native julia and NetCDF standards add unnecessary complexity and potential for errors.
    Since the sole purpose is for resuming or continuing a simulation, it is simpler and
    less error-prone to save and load everything with as little modifications as possible.
    This same mechanism can be extended to record checkpoints for very long runs, especially
    on remote clusters.
"""
function save_state(
                    file_path::String,
                    params::Params,
                    all_gridded_vars::AllGriddedVars,
                    sea_surface_height::𝔽
                    )

    @unpack Nx, Ny, δc, δz, N₀, Nwall = params
    domainparams = DomainParams(Nx, Ny, δc, δz, N₀, Nwall)

    @unpack waters, water_tracking, seabed, sediment_transport, boundaries = all_gridded_vars

    @unpack surf_elev, bed_elev, ice_thickness, water_depth,
            dry_flag,
            flow_speed_x, flow_speed_y, flow_speed,
            unit_dischg_x, unit_dischg_y, unit_dischg = waters

    @unpack erodibility = sediment_transport

    @unpack wall_flag, ocean_flag = boundaries

    print("[ Writing simulation state to file, which may take a while... ")

    # Open a JLD2 file for writing:
    jldopen(file_path, "w") do file

        # Write each relevant variable-set to file.
        write(file, "domainparams", domainparams)
        write(file, "surf_elev", surf_elev)
        write(file, "bed_elev", bed_elev)
        write(file, "ice_thickness", ice_thickness)
        write(file, "water_depth", water_depth)
        write(file, "dry_flag", dry_flag)
        write(file, "flow_speed_x", flow_speed_x)
        write(file, "flow_speed_y", flow_speed_y)
        write(file, "flow_speed", flow_speed)
        write(file, "unit_dischg_x", unit_dischg_x)
        write(file, "unit_dischg_y", unit_dischg_y)
        write(file, "unit_dischg", unit_dischg)
        write(file, "erodibility", erodibility)
        write(file, "wall_flag", wall_flag)
        write(file, "ocean_flag", ocean_flag)
        write(file, "seabed", seabed)
        write(file, "sea_surface_height", sea_surface_height)

    end # do file

    println("DONE!\n")

end
