#---1----|----2----|----3----|----4----|----5----|----6----|----7----|----8----|----9----|----A----|----B----|----C----|
using ArcDelRCM, JLD2, Test

# Determine the "natural" floating-point type of the system.
𝔽 = typeof(0.0)

# Set up a pair of standard parameters for most of the tests below.
# 0: non-Arctic
settings0 = setparams(
                      false;
                       Nx = 60, Ny = 120, δc = 50., δz = 0.05, N₀ = 5, Nwall = 3, Δt = 25000.0,
                       lasttime = 5005, nw = 2000, ns = 2000, lastiterw = 5,
                       iceextent = 0., hᵢmax = 0., 𝐸 = 1., å = 1.,
                       sedfraction = 0.001, f_sand = 0.5, f_mud = 0.5,
                       Sₘᵢₙ = 2.e-4, hₘᵢₙ = 5., hB = 5., hdry = 0.5, hΔ = 0.5,
                       uₘᵢₙ = 1., uₘₐₓ = 2., u_muddep_coef = 0.3, u_mudero_coef = 1.5, u_sandero_coef = 1.05, uΔ = 0.5,
                       Qw₀ = [1250.], Qs₀ = [1.250],
                       α = 0.1, β = 3., γ = 0.073575, ϵ = 0.1, θ = (water = 1., sand = 2., mud = 1.),
                       ϖ = 0.1, ω = (iterfirst = 0.9, itersubsq = 0.4),
                       backwater_method = :backwater_constslope!,
                       manningscoef² = 0.0016,
                       𝐶𝔣 = 0.01
                      )
# 1: Arctic
settings1 = setparams(
                      true;
                       Nx = 60, Ny = 120, δc = 50., δz = 0.05, N₀ = 5, Nwall = 3, Δt = 25000.0,
                       lasttime = 5005, nw = 2000, ns = 2000, lastiterw = 5,
                       iceextent = 0.4, hᵢmax = 3., 𝐸 = 0.95, å = 1.,
                       sedfraction = 0.001, f_sand = 0.25, f_mud = 0.75,
                       Sₘᵢₙ = 1.5e-4, hₘᵢₙ = 5., hB = 5., hdry = 0.5, hΔ = 0.5,
                       uₘᵢₙ = 1., uₘₐₓ = 2., u_muddep_coef = 0.3, u_mudero_coef = 1.5, u_sandero_coef = 1.05, uΔ = 0.5,
                       Qw₀ = [1250.], Qs₀ = [1.250],
                       α = 0.1, β = 3., γ = 0.073575, ϵ = 0.1, θ = (water = 1., sand = 2., mud = 1.),
                       ϖ = 0.1, ω = (iterfirst = 0.9, itersubsq = 0.4),
                       backwater_method = :backwater_constslope!,
                       manningscoef² = 0.0016,
                       𝐶𝔣 = 0.01
                      )

## Test functions in "init.jl" (includes a test for `save_state()` in "procedures.jl")
@testset "init.jl (incl. `save_state` in procedures.jl)" begin

    # Tests ensuring that setparams(...) outputs as expected.
    @testset "set parameters" begin

        @test typeof(settings0) <: Params{Int,𝔽}
        @test isnan(settings0.iceextent)
        @test isnan(settings0.hᵢmax)
        @test isnan(settings0.𝐸)
        @test isnan(settings0.å)
        @test typeof(settings0.backwater_method!) <: Function

        @test typeof(settings1) <: Params{Int,𝔽}
        @test ~isnan(settings1.iceextent)
        @test ~isnan(settings1.hᵢmax)
        @test ~isnan(settings1.𝐸)
        @test ~isnan(settings1.å)
        @test typeof(settings1.backwater_method!) <: Function

        settings2 = setparams(
                              true;
                               Nx = 60, Ny = 120, δc = 50., δz = 0.05, N₀ = 5, Nwall = 3, Δt = 25000.0,
                               lasttime = 5005, nw = 2000, ns = 2000, lastiterw = 5,
                               iceextent = 0.4, hᵢmax = 3., 𝐸 = 0.95, å = 1.,
                               sedfraction = 0.001, f_sand = 0.25, f_mud = 0.75,
                               Sₘᵢₙ = 1.5e-4, hₘᵢₙ = 5., hB = 5., hdry = 0.5, hΔ = 0.5,
                               uₘᵢₙ = 1., uₘₐₓ = 2.,
                               u_muddep_coef = 0.3, u_mudero_coef = 1.5, u_sandero_coef = 1.05, uΔ = 0.5,
                               Qw₀ = [1250.], Qs₀ = [1.250],
                               α = 0.1, β = 3., γ = 0.073575, ϵ = 0.1, θ = (water = 1., sand = 2., mud = 1.),
                               ϖ = 0.1, ω = (iterfirst = 0.9, itersubsq = 0.4),
                               backwater_method = :backwater_gvfmanning!,
                               manningscoef² = 0.0016,
                               𝐶𝔣 = 0.01
                              )
        @test ~isnan(settings2.manningscoef²)
        @test isnan(settings2.𝐶𝔣)

        settings3 = setparams(
                              true;
                               Nx = 60, Ny = 120, δc = 50., δz = 0.05, N₀ = 5, Nwall = 3, Δt = 25000.0,
                               lasttime = 5005, nw = 2000, ns = 2000, lastiterw = 5,
                               iceextent = 0.4, hᵢmax = 3., 𝐸 = 0.95, å = 1.,
                               sedfraction = 0.001, f_sand = 0.25, f_mud = 0.75,
                               Sₘᵢₙ = 1.5e-4, hₘᵢₙ = 5., hB = 5., hdry = 0.5, hΔ = 0.5,
                               uₘᵢₙ = 1., uₘₐₓ = 2.,
                               u_muddep_coef = 0.3, u_mudero_coef = 1.5, u_sandero_coef = 1.05, uΔ = 0.5,
                               Qw₀ = [1250.], Qs₀ = [1.250],
                               α = 0.1, β = 3., γ = 0.073575, ϵ = 0.1, θ = (water = 1., sand = 2., mud = 1.),
                               ϖ = 0.1, ω = (iterfirst = 0.9, itersubsq = 0.4),
                               backwater_method = :backwater_gvfflowrcm!,
                               manningscoef² = 0.0016,
                               𝐶𝔣 = 0.01
                              )
        @test isnan(settings3.manningscoef²)
        @test ~isnan(settings3.𝐶𝔣)

    end # @testset "setparams"

    # Tests ensuring that preallocation and initialisation output as expected and do not crash.
    @testset "allocate & initialise" begin

        a1, b1 = ArcDelRCM.preallocate(settings0)
        a2, b2 = ArcDelRCM.preallocate(settings1)

        @test typeof(a1) <: AllGriddedVars
        @test typeof(a2) <: AllGriddedVars
        @test typeof(a1.waters) <: Waters{<:Real,<:Nothing}
        @test typeof(a2.waters) <: Waters{<:Real,<:Number}
        @test typeof(a1.waters.ice_thickness) <: Array{Nothing,2}
        @test typeof(a2.waters.ice_thickness) <: Array{<:Real,2}
        @test typeof(a1.seabed)  <: Array{SedimentColumn{T},2} where T <: Real
        @test typeof(a2.seabed)  <: Array{SedimentColumn{T},2} where T <: Real
        @test typeof(a1.water_tracking)  <: WaterTracking{<:Integer,<:Real}
        @test typeof(a2.water_tracking)  <: WaterTracking{<:Integer,<:Real}
        @test typeof(a1.sediment_transport) <: SedimentTransport{<:Real}
        @test typeof(a2.sediment_transport) <: SedimentTransport{<:Real}
        @test typeof(a1.boundaries)  <: Boundaries
        @test typeof(a2.boundaries)  <: Boundaries
        @test typeof(b1) <: Float64
        @test typeof(b2) <: Float64

        @test ArcDelRCM.populate_initial_state!(settings0, a1, b1) ≡ nothing
        @test ArcDelRCM.populate_initial_state!(settings1, a2, b2) ≡ nothing

    end # @testset "preallocate and populate_initial_state!"

    # Tests ensuring that boundary conditions are properly reimposed.
    @testset "boundary conditions" begin

        a, b = ArcDelRCM.preallocate(settings0)

        a.waters.unit_dischg[1, settings0.inletsides[1]:settings0.inletsides[2]] .= Inf
        a.waters.unit_dischg_x[1, settings0.inletsides[1]:settings0.inletsides[2]] .= Inf
        a.waters.unit_dischg_y[1, settings0.inletsides[1]:settings0.inletsides[2]] .= Inf

        a.waters.water_depth[1, settings0.inletsides[1]:settings0.inletsides[2]] .= Inf
        a.waters.bed_elev[1, settings0.inletsides[1]:settings0.inletsides[2]] .= Inf

        ArcDelRCM.reimpose_boundary_conds_dischg!(1, settings0, a.waters)
        @test all(a.waters.unit_dischg[1, settings0.inletsides[1]:settings0.inletsides[2]] .≡ settings0.qw₀[1])
        @test all(a.waters.unit_dischg_x[1, settings0.inletsides[1]:settings0.inletsides[2]] .≡ settings0.qw₀[1])
        @test all(a.waters.unit_dischg_y[1, settings0.inletsides[1]:settings0.inletsides[2]] .≡ 0.0)

        ArcDelRCM.reimpose_boundary_conds_depth!(1, settings0, a.waters)
        @test all(a.waters.water_depth[1, settings0.inletsides[1]:settings0.inletsides[2]] .≡ settings0.h₀[1])
        @test all(a.waters.bed_elev[1, settings0.inletsides[1]:settings0.inletsides[2]] .≡
            a.waters.surf_elev[1, settings0.inletsides[1]:settings0.inletsides[2]] .- settings0.h₀[1])

    end # @testset "reimpose_boundary_conds_*!"

    # Test that `backwater_check` returns as expected in the permissive case.
    # (The other cases require user input at prompt.)
    @testset "check backwater-method" begin

        @test ArcDelRCM.backwater_check(ArcDelRCM.backwater_constslope!) ≡ true

    end

    # Test that `scaleheightmeasure` calculates ζ correctly,
    # and that `timestep_check` returns as expected in the permissive case.
    # (The other cases require user input at prompt.)
    @testset "check timestep_check and scaleheightmeasure" begin

        @test ArcDelRCM.scaleheightmeasure(160000., 15., 100000., 36.) ≡ 1.5

        dvs1, dt1 = ArcDelRCM.timestep_check(40000., 20., 47999.9999, [49.]) # ζ == 2.999999
        @test dvs1[1] ≈ 2.352e6
        @test dt1 ≈ 47999.9999
        dvs2, dt2 = ArcDelRCM.timestep_check(2500., 5., 10000.01, [1.25]) # ζ == 1.000001
        @test dvs2[1] ≈ 12500.0125
        @test dt2 ≈ 10000.01

    end

    # Tests ensuring that saved states are inherited and informed properly.
    @testset "inheritance and domain-matching checks" begin

        # Test printing domain information directly from a saved-state file.
        domainparams0 = DomainParams(
                                     settings0.Nx,
                                     settings0.Ny,
                                     settings0.δc,
                                     settings0.δz,
                                     settings0.N₀,
                                     settings0.Nwall
                                     )
        JLD2.jldopen("test.jld2", "w") do file
           write(file, "domainparams", domainparams0)
        end
        @inferred ArcDelRCM.print_inheritance_domain("test.jld2")
        @test_throws ErrorException ArcDelRCM.print_inheritance_domain("nonexistant.jld2")

        rm("test.jld2")

        # Test the saving of state variables.
        a, b = ArcDelRCM.preallocate(settings1)
        ArcDelRCM.populate_initial_state!(settings1, a, b) # This will get rid of any `#undef` values.
        @inferred ArcDelRCM.save_state("test.jld2", settings1, a, b)
        @test isfile("test.jld2")

        # Test inheriting from a saved state file. (Note: `inherit_previous_state!` contains `matchstructs(...)`)
        ArcDelRCM.inherit_previous_state!("test.jld2", settings1, a, b)
        @test typeof(a) <: AllGriddedVars
        @test typeof(b) <: Float64

        # Test inheriting from a saved state file with non-identical domain.
        settings1x = setparams(
                               true;
                                Nx = 61, Ny = 120, δc = 50., δz = 0.05, N₀ = 5, Nwall = 3, # ← Nx changed to 61
                                Δt = 25000.0,
                                lasttime = 5005, nw = 2000, ns = 2000, lastiterw = 5,
                                iceextent = 0.4, hᵢmax = 3., 𝐸 = 0.95, å = 1.,
                                sedfraction = 0.001, f_sand = 0.25, f_mud = 0.75,
                                Sₘᵢₙ = 1.5e-4, hₘᵢₙ = 5., hB = 5., hdry = 0.5, hΔ = 0.5,
                                uₘᵢₙ = 1., uₘₐₓ = 2.,
                                u_muddep_coef = 0.3, u_mudero_coef = 1.5, u_sandero_coef = 1.05, uΔ = 0.5,
                                Qw₀ = [1250.], Qs₀ = [1.250],
                                α = 0.1, β = 3., γ = 0.073575, ϵ = 0.1, θ = (water = 1., sand = 2., mud = 1.),
                                ϖ = 0.1, ω = (iterfirst = 0.9, itersubsq = 0.4),
                                backwater_method = :backwater_constslope!,
                                manningscoef² = 0.0016,
                                𝐶𝔣 = 0.01
                               )
        @test_throws ErrorException ArcDelRCM.inherit_previous_state!("test.jld2", settings1x, a, b)
        # ↑ tests `matchstructs(...) ↑`

        rm("test.jld2")

        # Testing structs comparisons
        @test !ArcDelRCM.matchstructs(domainparams0, settings0)
        @test ArcDelRCM.matchstructs(domainparams0, settings0, [:Nx, :Ny, :δc, :δz, :N₀, :Nwall])
        @test !ArcDelRCM.matchstructs(domainparams0, a)
        @test !ArcDelRCM.matchstructs(domainparams0, a, [:Nx, :Ny, :δc, :δz, :N₀, :Nwall])

    end # @testset "inheritance"

    ### TODO: Add tests for the file-generating functions.

end # @testset "init.jl (incl. `save_state` in procedures.jl)"

## Test functions in "support.jl"
@testset "support.jl" begin

    @testset "delta area & delimitation" begin

        # Tests ensuring the area of the delta is calculated properly.
        @testset "delta area" begin

            water_depth = [5.01 0 4.99 5.01 5.0; -1 0 5.01 1e300 -1e300]
            flow_speed = [0.99 0.99 0.99 0.99 0.99; 0.99 0.99 0.99 0.99 0.99]
            @test ArcDelRCM.delta_area(5, 1, water_depth, flow_speed) ≡ 6.0

            flow_speed[[1 6 7 8]] .= 1.01
            @test ArcDelRCM.delta_area(5, 1, water_depth, flow_speed) ≡ 10.0

            water_depth = [5.01 5.01 5.01 5.01 5.01; 5.01 5.01 5.01 5.01 5.01]
            flow_speed = [-1e300 1.01 0 -1 1.01; 0.99 0.99 1.01 1 1e300]
            @test ArcDelRCM.delta_area(5, 1, water_depth, flow_speed) ≡ 5.0

        end # @testset "delta_area"

        # Tests ensuring delta cells (versus ocean cells) are identified correctly.
        @testset "is delta cell or not" begin

            indices = CartesianIndex(2, 3)

            water_depth = zeros(5, 10)
            flow_speed = zeros(5, 10)

            water_depth[2, 3] = 9.999999999999999
            flow_speed[2, 3] = 2e300
            @test ArcDelRCM.is_on_delta(Tuple(indices), water_depth, flow_speed, 10.0, 0.5) ≡ true

            water_depth[2, 3] = 2e300
            flow_speed[2, 3] = 0.750000000000001
            @test ArcDelRCM.is_on_delta(Tuple(indices), water_depth, flow_speed, 5.0, 0.75) ≡ true

        end # @testset "is_on_delta"

        # Tests ensuring that the delta shoreline is located correctly and
        # that the backwater profile is shaped correctly for each water packet.
        @testset "locate shoreline, shape backwater" begin

            a, b = ArcDelRCM.preallocate(settings0)
            path_history = a.water_tracking.path_history
            step_history = a.water_tracking.step_history
            u = a.waters.flow_speed
            ux = a.waters.flow_speed_x
            uy = a.waters.flow_speed_y
            h = a.waters.water_depth
            nvisits = a.water_tracking.visit_count
            cumulatedH = a.water_tracking.cumul_surf_elev

            path_history[1:10] = [CartesianIndex(1, settings0.Ny½ + 0);
                                  CartesianIndex(2, settings0.Ny½ + 0);
                                  CartesianIndex(3, settings0.Ny½ + 1);
                                  CartesianIndex(4, settings0.Ny½ + 1);
                                  CartesianIndex(5, settings0.Ny½ + 0);
                                  CartesianIndex(5, settings0.Ny½ - 1);
                                  CartesianIndex(6, settings0.Ny½ - 1);
                                  CartesianIndex(7, settings0.Ny½ + 0);
                                  CartesianIndex(8, settings0.Ny½ + 0);
                                  CartesianIndex(9, settings0.Ny½ + 1)];
            step_history[1:9] = [6; 9; 6; 3; 2; 6; 9; 6; 9];
            for i in 1:10
                u[path_history[i]] = 2.0 * (10 - i) / 10
                h[path_history[i]] = 5.0 * i / 10
            end
            uy[path_history[1]] = 0; ux[path_history[1]] = u[path_history[1]];
            for i in 1:9
                ux[path_history[i+1]] = u[path_history[i+1]] * ArcDelRCM.𝐝ᵢ[step_history[i]][1]
                uy[path_history[i+1]] = u[path_history[i+1]] * ArcDelRCM.𝐝ᵢ[step_history[i]][2]
            end
            nvisits[path_history[1:10]] .= 0;
            cumulatedH[path_history[1:10]] .= 0.0;

            @test ArcDelRCM.locate_shore(10, path_history, h, u, 0.5, 0.5) ≡ 7
            @test ArcDelRCM.locate_shore(10, path_history, h, u, 0.6, 2.0) ≡ 1

            settings0.backwater_method!(1, 10, settings0, a.waters, a.water_tracking, b)
            @test all(nvisits[path_history[1:10]] .≡ [1; 1; 1; 1; 1; 1; 0; 0; 0; 0])
            @test all(
                      cumulatedH[path_history[1:10]] .≈ [0.04414213562373094;
                                                         0.034142135623730946;
                                                         0.024142135623730947;
                                                         0.017071067811865473;
                                                         0.0070710678118654745;
                                                         0.0;
                                                         0.0;
                                                         0.0;
                                                         0.0;
                                                         0.0]
                      )

        end # @testset "locate_shore (dep. on is_on_delta) and backwater_method!"

    end # @testset "area and delimitation"

    @testset "flow handling & updates" begin

        # Tests ensuring that looping water packets are repositioned correctly.
        @testset "loop disposal" begin

            @test ArcDelRCM.loop_disposal(4, 30, 60, 120, 3, 60) ≡ (4, 25)
            @test ArcDelRCM.loop_disposal(4, 120, 60, 120, 3, 60) ≡ (4, 119)
            @test ArcDelRCM.loop_disposal(59, 1, 60, 120, 3, 60) ≡ (59, 2)

        end # @testset "loop_disposal"

        # Tests ensuring that the maximum ice cover is laid down as expected.
        @testset "sculpt ice" begin

            agv, ssh = ArcDelRCM.preallocate(settings1)
            ArcDelRCM.populate_initial_state!(settings1, agv, ssh)

            # Maximum ice thickness thinner than water depth
            ArcDelRCM.sculpt_ice!(5., 15., 3, 60, settings1.hᵢmax, agv.waters.water_depth, agv.waters.ice_thickness)
            @test agv.waters.ice_thickness[5, settings1.Ny½] ≡ 0.0
            @test agv.waters.ice_thickness[4, settings1.Ny½ + 15] ≡ settings1.hᵢmax
            @test 0.0 < agv.waters.ice_thickness[9, settings1.Ny½ + 9] ≡
                        agv.waters.ice_thickness[9, settings1.Ny½ - 9] < 3.0

            # Maximum ice thickness exceeding water depth
            ArcDelRCM.sculpt_ice!(5., 15., 3, 60, 2 * settings1.hᵢmax, agv.waters.water_depth, agv.waters.ice_thickness)
            @test agv.waters.ice_thickness[4, settings1.Ny½ - 15] ≈ 0.9999 * settings1.hB

        end # @testset "sculpt_ice"

        # Tests ensuring that the flow velocity is updated correctly, respecting limits.
        @testset "update flow velocity" begin

            # non-Arctic
            agv0, ssh0 = ArcDelRCM.preallocate(settings0)

            agv0.waters.unit_dischg_x[settings0.Nx, settings0.Ny] = 6.7 / 3
            agv0.waters.unit_dischg_y[settings0.Nx, settings0.Ny] = 6.7 * 2 / 3
            agv0.waters.unit_dischg[settings0.Nx, settings0.Ny] = 6.7
            agv0.waters.water_depth[settings0.Nx, settings0.Ny] = 5.0
            ArcDelRCM.update_flow_velocity!(
                                            (settings0.Nx, settings0.Ny),
                                            settings0.uₘₐₓ,
                                            agv0.waters.ice_thickness,
                                            agv0.waters.water_depth,
                                            agv0.waters.flow_speed_x,
                                            agv0.waters.flow_speed_y,
                                            agv0.waters.flow_speed,
                                            agv0.waters.unit_dischg_x,
                                            agv0.waters.unit_dischg_y,
                                            agv0.waters.unit_dischg
                                            )
            @test agv0.waters.flow_speed_x[settings0.Nx, settings0.Ny] ≈ 0.4466666666666667
            @test agv0.waters.flow_speed_y[settings0.Nx, settings0.Ny] ≈ 0.8933333333333334
            @test agv0.waters.flow_speed[settings0.Nx, settings0.Ny] ≡ 1.34

            agv0.waters.water_depth[settings0.Nx, settings0.Ny] = 2.0
            ArcDelRCM.update_flow_velocity!(
                                            settings0.Nx * settings0.Ny,
                                            settings0.uₘₐₓ,
                                            agv0.waters.ice_thickness,
                                            agv0.waters.water_depth,
                                            agv0.waters.flow_speed_x,
                                            agv0.waters.flow_speed_y,
                                            agv0.waters.flow_speed,
                                            agv0.waters.unit_dischg_x,
                                            agv0.waters.unit_dischg_y,
                                            agv0.waters.unit_dischg
                                            )
            @test agv0.waters.flow_speed_x[settings0.Nx, settings0.Ny] ≈ 0.6666666666666666
            @test agv0.waters.flow_speed_y[settings0.Nx, settings0.Ny] ≈ 1.3333333333333333
            @test agv0.waters.flow_speed[settings0.Nx, settings0.Ny] ≡ settings0.uₘₐₓ

            # Arctic
            agv1, ssh1 = ArcDelRCM.preallocate(settings1)

            agv1.waters.unit_dischg_x[settings1.Nx, settings1.Ny] = 6.7 / 3
            agv1.waters.unit_dischg_y[settings1.Nx, settings1.Ny] = 6.7 * 2 / 3
            agv1.waters.unit_dischg[settings1.Nx, settings1.Ny] = 6.7
            agv1.waters.water_depth[settings1.Nx, settings1.Ny] = 5.0
            agv1.waters.ice_thickness[settings1.Nx, settings1.Ny] = 0.0
            ArcDelRCM.update_flow_velocity!(
                                            settings1.Nx * settings1.Ny,
                                            settings1.uₘₐₓ,
                                            agv1.waters.ice_thickness,
                                            agv1.waters.water_depth,
                                            agv1.waters.flow_speed_x,
                                            agv1.waters.flow_speed_y,
                                            agv1.waters.flow_speed,
                                            agv1.waters.unit_dischg_x,
                                            agv1.waters.unit_dischg_y,
                                            agv1.waters.unit_dischg
                                            )
            @test agv1.waters.flow_speed_x[settings1.Nx, settings1.Ny] ≈ 0.4466666666666667
            @test agv1.waters.flow_speed_y[settings1.Nx, settings1.Ny] ≈ 0.8933333333333334
            @test agv1.waters.flow_speed[settings1.Nx, settings1.Ny] ≡ 1.34

            agv1.waters.water_depth[settings1.Nx, settings1.Ny] = 2.0
            ArcDelRCM.update_flow_velocity!(
                                            settings1.Nx * settings1.Ny,
                                            settings1.uₘₐₓ,
                                            agv1.waters.ice_thickness,
                                            agv1.waters.water_depth,
                                            agv1.waters.flow_speed_x,
                                            agv1.waters.flow_speed_y,
                                            agv1.waters.flow_speed,
                                            agv1.waters.unit_dischg_x,
                                            agv1.waters.unit_dischg_y,
                                            agv1.waters.unit_dischg
                                            )
            @test agv1.waters.flow_speed_x[settings1.Nx, settings1.Ny] ≈ 0.6666666666666666
            @test agv1.waters.flow_speed_y[settings1.Nx, settings1.Ny] ≈ 1.3333333333333333
            @test agv1.waters.flow_speed[settings1.Nx, settings1.Ny] ≡ settings0.uₘₐₓ

            agv1.waters.water_depth[settings1.Nx, settings1.Ny] = 3.0
            agv1.waters.ice_thickness[settings1.Nx, settings1.Ny] = 3.1
            ArcDelRCM.update_flow_velocity!(
                                            (settings1.Nx, settings1.Ny),
                                            settings1.uₘₐₓ,
                                            agv1.waters.ice_thickness,
                                            agv1.waters.water_depth,
                                            agv1.waters.flow_speed_x,
                                            agv1.waters.flow_speed_y,
                                            agv1.waters.flow_speed,
                                            agv1.waters.unit_dischg_x,
                                            agv1.waters.unit_dischg_y,
                                            agv1.waters.unit_dischg
                                            )
            @test agv1.waters.flow_speed_x[settings1.Nx, settings1.Ny] ≡
                    agv1.waters.flow_speed_y[settings1.Nx, settings1.Ny] ≡
                    agv1.waters.flow_speed[settings1.Nx, settings1.Ny] ≡ 0.0

        end # @testset "update_flow_velocity!"

        # Tests ensuring that water depth and dry flag are updated correctly.
        @testset "update water depth & dry flag" begin

            agv, ssh = ArcDelRCM.preallocate(settings0)

            agv.waters.surf_elev[1, settings0.Ny½] = 0.01
            agv.waters.bed_elev[1, settings0.Ny½] = -4.99
            agv.waters.water_depth[1, settings0.Ny½] = NaN
            agv.waters.dry_flag[1, settings0.Ny½] = true

            ArcDelRCM.update_water_depth_dry_flag!(
                                                   settings0.Nx * (settings0.Ny½ - 1) + 1,
                                                   0.5,
                                                   agv.waters.surf_elev,
                                                   agv.waters.bed_elev,
                                                   agv.waters.water_depth,
                                                   agv.waters.dry_flag
                                                   )

            @test agv.waters.water_depth[1, settings0.Ny½] ≡ 5.0
            @test agv.waters.dry_flag[1, settings0.Ny½] ≡ false

            agv.waters.surf_elev[1, settings0.Ny½] = -0.5
            agv.waters.bed_elev[1, settings0.Ny½] = -0.99
            agv.waters.water_depth[1, settings0.Ny½] = NaN
            agv.waters.dry_flag[1, settings0.Ny½] = false

            ArcDelRCM.update_water_depth_dry_flag!(
                                                   settings0.Nx * (settings0.Ny½ - 1) + 1,
                                                   0.5,
                                                   agv.waters.surf_elev,
                                                   agv.waters.bed_elev,
                                                   agv.waters.water_depth,
                                                   agv.waters.dry_flag
                                                   )

            @test agv.waters.water_depth[1, settings0.Ny½] ≡ 0.49
            @test agv.waters.dry_flag[1, settings0.Ny½] ≡ true

        end # @testset "update_water_depth_dry_flag!"

    end # @testset "flow handling and updates"

    @testset "sediment mass exchanges" begin

        # Set up sediment-related variables for the tests in this testset.
        𝔽𝕤 = ArcDelRCM.𝔽𝕤
        a, b = ArcDelRCM.preallocate(settings1)
        ArcDelRCM.populate_initial_state!(settings1, a, b)
        seabed = a.seabed

        # Tests ensuring that permafrost layers are frozen, timed, and thawed correctly, and
        # that the active layer is tracked correctly.
        @testset "freeze & thaw permafrost" begin

            # Make a sediment column:
            #=
            13-element Array{Sediment{Float64},1}:
            sand fraction: 0.4 , time deposited: 5000.0 , frozen since: [1.0]
            sand fraction: 0.4 , time deposited: 5000.0 , frozen since: [1.0]
            sand fraction: 0.4 , time deposited: 5000.0 , frozen since: [1.0]
            sand fraction: 0.4 , time deposited: 25000.0 , frozen since: [2.0]
            sand fraction: 0.4 , time deposited: 25000.0 , frozen since: [2.0]
            sand fraction: 0.4 , time deposited: 25000.0 , frozen since: [2.0]
            sand fraction: 0.4 , time deposited: 25000.0 , frozen since: [2.0]
            sand fraction: 0.3 , time deposited: 30000.0 , frozen since: [3.0]
            sand fraction: 0.3 , time deposited: 30000.0 , frozen since: [3.0]
            sand fraction: 0.3 , time deposited: 30000.0 , frozen since: [NaN]
            sand fraction: 0.3 , time deposited: 30000.0 , frozen since: [NaN]
            sand fraction: 0.3 , time deposited: 35000.0 , frozen since: [NaN]
            sand fraction: 0.3 , time deposited: 35000.0 , frozen since: [NaN]
            =#
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 5000, [1]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 5000, [1]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 5000, [1]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 25000, [2]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 25000, [2]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 25000, [2]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.4, 25000, [2]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 30000, [3]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 30000, [3]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 30000, [NaN]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 30000, [NaN]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 35000, [NaN]))
            push!(seabed[3].strata, Sediment{𝔽𝕤}(0.3, 35000, [NaN]))
            seabed[3].z[2] = -5.0 + 13 * settings1.δz

            # Make a copy for testing.
            test_sedcol = copy(seabed[3])

            # 💡: seabed[cellindex][index] indexes seabed[cellindex].strata directly.

            @test ArcDelRCM.active_layer!(13, test_sedcol, 8) ≡ 8
            @test isequal(test_sedcol[9], Sediment{𝔽𝕤}(0.3, 30000.0, [NaN]))
            @test ArcDelRCM.frosty_layer!(8, test_sedcol, 4) ≡ 7
            @test ArcDelRCM.frosty_layer!(11, test_sedcol, 3) ≡ 3
            @test all(
                      isequal.(
                               test_sedcol[10:11],
                               [Sediment{𝔽𝕤}(0.3, 30000.0, [3.0]); Sediment{𝔽𝕤}(0.3, 30000.0, [3.0])]
                               )
                      )

            # Re-make the same sediment column for testing
            copy!(test_sedcol, seabed[3])

            test_sedcol.permaz[1] = 0.0               # Pretend that there are no permafrost before now
            test_sedcol.permaz[2] = 5 * settings1.δz  # Pretend that the top 5 cells are in the active layer

            ArcDelRCM.freeze_or_thaw!(test_sedcol, settings1.δz, 8, 4)
            @test test_sedcol.permaz[1] ≈ 7 * settings1.δz
            @test isnan(test_sedcol[9].frostysince[1])

            test_sedcol.permaz[2] = 2 * settings1.δz  # Pretend that the top 2 cells are in the active layer

            ArcDelRCM.freeze_or_thaw!(test_sedcol, settings1.δz, 11, 4)
            @test test_sedcol[10].frostysince[1] ≡ test_sedcol[11].frostysince[1] ≡ 𝔽𝕤(4.0)

        end # @testset "freeze_or_thaw!, active_layer!, and frosty_layer!"

        # Tests ensuring that deposition and erosion are performed correctly, and
        # that the correct information is returned.
        @testset "deposit & erode" begin

            # 👉 isempty(seabed[4].strata) ≡ true

            @test ArcDelRCM.deposit_stratum!(seabed[4].strata, Inf, -Inf, 0) ≡ 0
            @test isempty(seabed[4].strata)
            @test ArcDelRCM.erode_stratum!(seabed[4].strata, 0) ≡ 0

            @test ArcDelRCM.deposit_stratum!(seabed[4].strata, 0.2112, 84531, 16) ≡ 16
            @test length(seabed[4].strata) ≡ 16
            @test seabed[4][rand(1:16)].sandfrac ≡ 𝔽𝕤(0.2112)
            @test seabed[4][rand(1:16)].timeofdep ≡ 𝔽𝕤(84531)
            @test seabed[4][rand(1:16)].frostysince[1] ≡ 𝔽𝕤(NaN)

            @test ArcDelRCM.erode_stratum!(seabed[4].strata, 11) ≡ 11
            @test length(seabed[4].strata) ≡ 5
            @test seabed[4][rand(1:5)].sandfrac ≡ 𝔽𝕤(0.2112)
            @test seabed[4][rand(1:5)].timeofdep ≡ 𝔽𝕤(84531)
            @test seabed[4][rand(1:5)].frostysince[1] ≡ 𝔽𝕤(NaN)

            # Set/re-cast some variables for tests below
            η = a.waters.bed_elev
            h = a.waters.water_depth
            px = settings1.Nx ÷ 2
            py = settings1.Ny½

            # Store the original values
            ηpxpy₀ = η[px, py]
            hpxpy₀ = h[px, py]

            ArcDelRCM.topo_depo_ero!(px, py, η, h, 4551.1451)
            @test η[px, py] - ηpxpy₀ ≡ hpxpy₀ - h[px, py] ≡ 4551.1451

            ArcDelRCM.topo_depo_ero!(px, py, η, h, -4551.1451)
            @test η[px, py] - ηpxpy₀ ≡ hpxpy₀ - h[px, py] ≡ 0.0

        end # @testset "deposit & erode"

    end # @testset "sediment mass exchanges"

    # Tests for all the arithmetic and random-number helpers
    @testset "arithmetic & random" begin

        @testset "nothing means zero" begin

            @test ArcDelRCM.nothingmeanszero(5) ≡ 5
            @test ArcDelRCM.nothingmeanszero(Inf) ≡ Inf
            @test ArcDelRCM.nothingmeanszero(nothing) ≡ 0.0

            @test ArcDelRCM.nothingmeanszero(Float32, nothing) ≡ 0.0f0
            @test ArcDelRCM.nothingmeanszero(Int, nothing) ≡ 0

        end # @testset "nothing means zero"

        @testset "roulette" begin

            @test typeof(ArcDelRCM.roulette(rand(𝔽, 9))) <: Int
            @test 1 ≤ ArcDelRCM.roulette(rand(𝔽, 9)) ≤ 9

        end # @testset "roulette"

        @testset "under-relaxation" begin

            @test ArcDelRCM.underrelax(3.0, 5.0, 0.4) ≡ 4.2
            @test ArcDelRCM.underrelax(3, 5, 0) ≡ 5
            @test all(ArcDelRCM.underrelax([3 3; 3 3; 3 3], [5 5; 5 5; 5 5], 0.43925) .≡ 4.1215)
            @test all(ArcDelRCM.underrelax([3 3; 3 3; 3 3], [5 5; 5 5; 5 5], 1) .≡ 3)

        end # @testset "under-relaxation"

        @testset "weighted random walk" begin

            # Tests both variants of `wheretonext`
            @testset "wheretonext" begin

                flowx = 0.0
                flowy = 0.0
                surfe = zeros(5, 6) .+ Inf
                wdepth = zeros(5, 6) .+ Inf

                # Every direction is equal.
                mask = falses(5, 6)
                exclusion = falses(5, 6)

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  surfe[2:4, 3:5],
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.γ,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test 1 ≤ direction ≤ 9
                @test typeof(direction) <: Int

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test 1 ≤ direction ≤ 9
                @test typeof(direction) <: Int

                # Only one direction is allowed by `mask`, `exclusion` is permissive.
                mask = trues(5, 6)
                mask[4, 5] = false

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  surfe[2:4, 3:5],
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.γ,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test direction ≡ 9

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test direction ≡ 9

                # Only one direction is allowed by `exclusion`, `mask` is permissive.
                mask = falses(5, 6)
                exclusion = trues(5, 6)
                exclusion[3, 3] = false

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  surfe[2:4, 3:5],
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.γ,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test direction ≡ 2

                direction = ArcDelRCM.wheretonext(
                                                  (x...) -> nothing,
                                                  wdepth[2:4, 3:5],
                                                  flowx,
                                                  flowy,
                                                  settings0.θ.water,
                                                  mask[2:4, 3:5],
                                                  exclusion[2:4, 3:5]
                                                  )
                @test direction ≡ 2

                # TODO: Add tests that invoke the `rand_helper_` function.

            end # @testset "wheretonext"

            # Tests both variants of `nextstep_icefree`
            @testset "nextstep_icefree" begin

                flowx = 1.0
                flowy = 0.0
                mask = falses(5, 6)
                exclusion = falses(5, 6)
                surfe = zeros(5, 6)
                wdepth = zeros(5, 6)

                # Water depth matters.
                wdepth[3, 1] = 1.0
                @test @views ArcDelRCM.nextstep_icefree(
                                                        surfe[1:3, 1:3],
                                                        wdepth[1:3, 1:3],
                                                        nothing,
                                                        flowx,
                                                        flowy,
                                                        settings0.γ,
                                                        settings0.θ.water,
                                                        mask[1:3, 1:3],
                                                        exclusion[1:3, 1:3]
                                                        ) ≡ 3
                @test @views ArcDelRCM.nextstep_icefree(
                                                        wdepth[1:3, 1:3],
                                                        nothing,
                                                        flowx,
                                                        flowy,
                                                        settings0.θ.water,
                                                        mask[1:3, 1:3],
                                                        exclusion[1:3, 1:3]
                                                        ) ≡ 3

                # Flow direction matters.
                # NB: wdepth[3, 1] ≡ 1.0 still!
                wdepth[1, 2] = 1.0
                @test @views ArcDelRCM.nextstep_icefree(
                                                        surfe[1:3, 1:3],
                                                        wdepth[1:3, 1:3],
                                                        nothing,
                                                        flowx,
                                                        flowy,
                                                        settings0.γ,
                                                        settings0.θ.water,
                                                        mask[1:3, 1:3],
                                                        exclusion[1:3, 1:3]
                                                        ) ≡ 3
                @test @views ArcDelRCM.nextstep_icefree(
                                                        wdepth[1:3, 1:3],
                                                        nothing,
                                                        flowx,
                                                        flowy,
                                                        settings0.θ.water,
                                                        mask[1:3, 1:3],
                                                        exclusion[1:3, 1:3]
                                                        ) ≡ 3

                # Partitioning coefficient, γ, and surface elevation both matter.
                # NB: wdepth[3, 1] ≡ wdepth[1, 2] ≡ 1.0 still!
                surfe[1, 3] = -0.0000001
                @test @views ArcDelRCM.nextstep_icefree(
                                                        surfe[1:3, 1:3],
                                                        wdepth[1:3, 1:3],
                                                        nothing,
                                                        flowx,
                                                        flowy,
                                                        1.0,
                                                        settings0.θ.water,
                                                        mask[1:3, 1:3],
                                                        exclusion[1:3, 1:3]
                                                        ) ≡ 7

            end # @testset "nextstep_icefree"

            # Tests both variants of `nextstep_withice`
            @testset "nextstep_withice" begin

                flowx = 0.0
                flowy = 1.0
                mask = falses(5, 6)
                exclusion = falses(5, 6)
                surfe = zeros(5, 6)
                wdepth = zeros(5, 6) .+ 0.0000001
                icethk = zeros(5, 6)

                # Water depth matters.
                wdepth[4, 6] = 0.5
                @test @views ArcDelRCM.nextstep_withice(
                                                        surfe[3:5, 4:6],
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        settings1.γ,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 8
                @test @views ArcDelRCM.nextstep_withice(
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 8

                # Flow direction matters.
                # NB: wdepth[4, 6] ≡ 0.5 still!
                wdepth[4, 4] = 0.5
                @test @views ArcDelRCM.nextstep_withice(
                                                        surfe[3:5, 4:6],
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        settings1.γ,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 8
                @test @views ArcDelRCM.nextstep_withice(
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 8

                # Partitioning coefficient, γ, and surface elevation both matter.
                # NB: wdepth[4, 6] ≡ wdepth[4, 4] ≡ 0.5 still!
                surfe[5, 4] = -0.0000001
                @test @views ArcDelRCM.nextstep_withice(
                                                        surfe[3:5, 4:6],
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        1.0,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 3

                # Ice thickness (and still partitioning coefficient, γ) matters.
                # NB: wdepth[4, 6] ≡ wdepth[4, 4] ≡ 0.5 and surfe[5, 4] ≡ -0.0000001 still!
                wdepth[3, 6] = 0.5
                icethk[4, 6] = 0.5
                icethk[3, 6] = 0.4
                @test @views ArcDelRCM.nextstep_withice(
                                                        surfe[3:5, 4:6],
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        0.0,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 7
                @test @views ArcDelRCM.nextstep_withice(
                                                        wdepth[3:5, 4:6],
                                                        icethk[3:5, 4:6],
                                                        flowx,
                                                        flowy,
                                                        settings1.θ.water,
                                                        mask[3:5, 4:6],
                                                        exclusion[3:5, 4:6]
                                                        ) ≡ 7

            end # @testset "nextstep_withice"

            @testset "weights helpers" begin

                flowx = 0.0
                flowy = 1.0
                wdepth = zeros(5, 6) .+ 0.0000001
                icethk = zeros(5, 6)

                w_inertia = zeros(9)

                wdepth[3, 5] = 0.5
                wdepth[3, 3] = 0.5
                ArcDelRCM.w_inertia_H2O_(2, w_inertia, view(wdepth, 2:4, 3:5), 1, flowx, flowy)
                ArcDelRCM.w_inertia_H2O_(8, w_inertia, view(wdepth, 2:4, 3:5), 1, flowx, flowy)
                @test w_inertia[2] ≡ 0.0
                @test w_inertia[8] ≡ 0.5

                w_inertia = zeros(9)

                ArcDelRCM.w_inertia_ICE_(2, w_inertia, view(wdepth, 2:4, 3:5), view(icethk, 2:4, 3:5), 1, flowx, flowy)
                ArcDelRCM.w_inertia_ICE_(8, w_inertia, view(wdepth, 2:4, 3:5), view(icethk, 2:4, 3:5), 1, flowx, flowy)
                @test w_inertia[2] ≡ 0.0
                @test w_inertia[8] ≡ 0.5

                icethk[3, 5] = 0.5
                ArcDelRCM.w_inertia_ICE_(8, w_inertia, view(wdepth, 2:4, 3:5), view(icethk, 2:4, 3:5), 1, flowx, flowy)
                @test w_inertia[8] ≡ 0.0

                surfe = zeros(5, 6) .+ 0.1
                w_surface = zeros(9)

                surfe[1, 3] = 1.0
                surfe[3, 3] = 0.0
                surfe[3, 2] = -√2 + 0.1
                ArcDelRCM.w_surface_(4, w_surface, view(surfe, 1:3, 2:4))
                ArcDelRCM.w_surface_(6, w_surface, view(surfe, 1:3, 2:4))
                ArcDelRCM.w_surface_(3, w_surface, view(surfe, 1:3, 2:4))
                @test w_surface[4] ≡ 0.0
                @test w_surface[6] ≡ 0.1
                @test w_surface[3] ≡ 1.0

                @test all(ArcDelRCM.quadrature_weights(10) .≈ 0.1)
                @test sum(ArcDelRCM.quadrature_weights(sin, 0, pi/2,  0,  0,  1, 20000000)) ≈ 1
                @test sum(ArcDelRCM.quadrature_weights(cos, 0,  pi, pi/4, 1, 500)) ≈ 1
                @test sum(ArcDelRCM.quadrature_weights(tanh, 3)) ≡ 1.0
                @test all(ArcDelRCM.quadrature_weights(cosh, 20; forepadzeros = 9)[1:9] .≡ 0.0)
                @test all(ArcDelRCM.quadrature_weights(sinh, 20; aftpadzeros = 9)[12:end] .≡ 0.0)

            end # @testset "weights helpers"

        end # @testset "weighted random walk"

    end # @testset "arithmetic & random"

end # @testset "support.jl"

## Test functions in "procedures.jl"
@testset "procedures.jl" begin

    # NB: All functions that depend solely on those tested in "support.jl" are tested here only with `@inferred`.

    @testset "impose and melt ice" begin

        agv, ssh = ArcDelRCM.preallocate(settings1)
        ArcDelRCM.populate_initial_state!(settings1, agv, ssh)

        @inferred ArcDelRCM.impose_winter_ice!(settings0, agv.waters)

        # Melting only due to atmospheric heat.
        agv.waters.ice_thickness .= 1.0
        agv.waters.flow_speed .= 0.0
        @inferred ArcDelRCM.melt_ice!(settings1, agv.waters, 1.0)
        @test all(agv.waters.ice_thickness .≡ 0.0)

        # Melting only due to discharge.
        agv.waters.ice_thickness .= 1.0
        agv.waters.flow_speed[settings1.Nwall + 1, :] .= 10.0
        @inferred ArcDelRCM.melt_ice!(settings1, agv.waters, 0.0)
        @test all(agv.waters.ice_thickness[ settings1.Nwall + 1, :] .< 1.0) &&
              all(agv.waters.ice_thickness[(settings1.Nwall + 2):end, :] .≡ 1.0)

        # Melting exceeds existing ice thickness.
        agv.waters.ice_thickness .= 0.01
        @inferred ArcDelRCM.melt_ice!(settings1, agv.waters, 200.0)
        @test all(agv.waters.ice_thickness .>= 0.0)

        # No melting.
        agv.waters.flow_speed .= 0.0
        agv.waters.ice_thickness .= 0.5
        @inferred ArcDelRCM.melt_ice!(settings1, agv.waters, 0.0)
        @test all(agv.waters.ice_thickness .≡ 0.5)

    end # @testset "impose and melt ice"

    @testset "water packets" begin

        @testset "startxy" begin

            px, py = @inferred ArcDelRCM.startxy(settings0)
            @test px ≡ 1
            @test typeof(py) <: Int
            @test settings0.inletsidewalls[1] < py < settings0.inletsidewalls[2]

        end # @testset "startxy"

        @testset "spawn_water!" begin

            agv0, ssh0 = ArcDelRCM.preallocate(settings0)
            agv0.waters.unit_dischg_x_nxt[1, :] .= 0.
            agv0.waters.unit_dischg_nxt[1, :] .= 0.

            px, py = @inferred ArcDelRCM.spawn_water!(1, settings0, agv0.waters, agv0.water_tracking)
            @test px ≡ 1
            @test typeof(py) <: Int
            @test settings0.inletsidewalls[1] < py < settings0.inletsidewalls[2]
            @test agv0.waters.unit_dischg_x_nxt[px, py] ≡ 1.
            @test agv0.waters.unit_dischg_nxt[px, py] ≈ settings0.Qwp[1] / settings0.δc / 2

        end # @testset "spawn_water!"

        @testset "stepn_water!" begin

            agv0, ssh0 = ArcDelRCM.preallocate(settings0)
            ArcDelRCM.populate_initial_state!(settings0, agv0, ssh0)
            agv0.waters.unit_dischg_x_nxt.= 0.
            agv0.waters.unit_dischg_nxt .= 0.
            px, py = ArcDelRCM.spawn_water!(1, settings0, agv0.waters, agv0.water_tracking)

            px1, py1, laststep, looped =
                @inferred ArcDelRCM.stepn_water!(
                                                 ArcDelRCM.nextstep_icefree,
                                                 1,
                                                 1,
                                                 px, py,
                                                 settings0,
                                                 agv0.waters,
                                                 agv0.water_tracking,
                                                 agv0.boundaries
                                                 )

            @test (px ≢ px1 || py ≢ py1)

            @test agv0.waters.unit_dischg_x_nxt[agv0.water_tracking.path_history[2]] ≡
                ArcDelRCM.onestepx[agv0.water_tracking.step_history[1]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[1]] +
                ArcDelRCM.onestepx[agv0.water_tracking.step_history[2]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[2]]

            @test agv0.waters.unit_dischg_y_nxt[agv0.water_tracking.path_history[2]] ≡
                ArcDelRCM.onestepy[agv0.water_tracking.step_history[1]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[1]] +
                ArcDelRCM.onestepy[agv0.water_tracking.step_history[2]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[2]]

            @test agv0.waters.unit_dischg_nxt[agv0.water_tracking.path_history[2]] ≈ settings0.Qwp[1] / settings0.δc

            @test agv0.waters.unit_dischg_x_nxt[agv0.water_tracking.path_history[laststep]] ≡
                ArcDelRCM.onestepx[agv0.water_tracking.step_history[laststep - 1]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[laststep - 1]]

            @test agv0.waters.unit_dischg_y_nxt[agv0.water_tracking.path_history[laststep]] ≡
                ArcDelRCM.onestepy[agv0.water_tracking.step_history[laststep - 1]] /
                      ArcDelRCM.Δᵢ[agv0.water_tracking.step_history[laststep - 1]]

            @test agv0.waters.unit_dischg_nxt[agv0.water_tracking.path_history[laststep]] ≈
                settings0.Qwp[1] / settings0.δc / 2

        end # @testset "stepn_water!"

    end # @testset "water packets"
    ### TODO: add testsets for procedure functions

end # @testset "procedures.jl"

## Test functions in "tasks.jl"
@testset "tasks.jl" begin

    ### TODO: add testsets for task functions

end # @testset "tasks.jl"
